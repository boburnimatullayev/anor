FROM node:14.15.0-alpine as builder

RUN mkdir app
WORKDIR app

COPY package*.json ./
RUN npm install


COPY . ./
RUN npm run-script build
FROM nginx:alpine
COPY --from='builder' /app/dist /dist
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
