CURRENT_DIR=$(shell pwd)

APP=$(shell basename ${CURRENT_DIR})

APP_CMD_DIR=${CURRENT_DIR}/cmd

IMG_NAME=${APP}
REGISTRY=gitlab.udevs.io:5050
PROJECT_NAME=anor
UPDATE_TAG=${UPDATE_TAG}
TAG=latest
ENV_TAG=latest
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
# Including
include .build_info

deploy-to-prod:
	kubectl ctx anor-prod
	kubectl apply -f .kube/prod
	kubectl set image -n microservices deployment/${DEPLOYMENT} ${DEPLOYMENT}=${REGISTRY}/${PROJECT_NAME}/${APP}:${UPDATE_TAG}

deploy-to-test:
	kubectl ctx anor-test
	kubectl apply -f .kube/test
	kubectl set image -n microservices deployment/${DEPLOYMENT} ${DEPLOYMENT}=${REGISTRY}/${PROJECT_NAME}/${APP}:${UPDATE_TAG}

mark-as-production-image:
	docker tag ${REGISTRY}/${IMG_NAME}:${TAG} ${REGISTRY}/${IMG_NAME}:production
	docker push ${REGISTRY}/${IMG_NAME}:production

build-image:
	docker build --rm -t ${REGISTRY}/${PROJECT_NAME}/${IMG_NAME}:${TAG} .
	docker tag ${REGISTRY}/${PROJECT_NAME}/${IMG_NAME}:${TAG} ${REGISTRY}/${PROJECT_NAME}/${IMG_NAME}:${ENV_TAG}

push-image:
	docker push ${REGISTRY}/${PROJECT_NAME}/${IMG_NAME}:${TAG}
	docker push ${REGISTRY}/${PROJECT_NAME}/${IMG_NAME}:${ENV_TAG}
