import { request } from '../api'
import axios_init from "../../utils/axios_init";
const auth = {
  login: (data) => axios_init.post_login(`/auth/standard/login`, data),
  branchLogin: (data) => axios_init.post_login("/auth/one2many/login", data),
  signup: (data) => request.post(`/account/signup/`, data),
  verifyCode: (data) => request.post(`/account/verify-code/`, data),
  recover: (data) => request.post(`/account/recover/`, data),
  verifyCodePassword: (data) =>
    request.post(`/account/verify-code-password/`, data),
  resetPassword: (data) => request.post(`/account/reset-password/`, data),
}

export default auth
