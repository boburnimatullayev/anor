import { message } from "antd"

export function validatePhone(text) {
    // console.log(text)
    if(!isFinite(text)) {
    //   message.error("Телефонный номер должен состоять только из цифр!")
      return Promise.reject(new Error("Телефонный номер должен состоять только из цифр!"))
    } else if(text.length !== 9) {
    //   message.error("Номер телефона должен состоять из 9 цифр!")
      return Promise.reject(new Error("Номер телефона должен состоять из 9 цифр!"))
    } else {
      return Promise.resolve()
    }
}


export function validateLogin(text){
  if(text.length < 8){
    return Promise.reject(new Error("Логин должен быть не менее 8 символов!"))
  } else {
    return Promise.resolve()
  }
}

export function validatePassword(text){
  if(text.length < 8){
    return Promise.reject(new Error("Пароль должен быть не менее 8 символов!"))
  } else {
    return Promise.resolve()
  }
}