try {
  var {type} = JSON.parse(localStorage.getItem("user-data"))
} catch(e) {
  // console.log(e)
}

export default function isInSuperAdmin(meta) {
  if(type === "BRANCH-ADMIN") {
      return !(meta.type === "SUPER-ADMIN")
  }
  return true
}