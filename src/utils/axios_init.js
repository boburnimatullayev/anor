import axios from "axios";
import { notification } from "antd";


const token = localStorage.getItem("token");
const user_data = localStorage.getItem("user-data");
const branch_id = user_data ? JSON.parse(user_data).branch_id : "";

function alert(title, des) {
  notification.error({
    message: title,
    description: des,
  });
}
function Redirect(msg, hooks) {
  alert(msg);
  localStorage.removeItem("token");
  // hooks.history.push('/login')
  window.location.href = "/login";
}

function ErrorHandler(error, hooks) {
  // console.log(hooks)
  if (error.message.startsWith("timeout")) {
    alert("Тайм-аут", "Пожалуйста, проверьте свой интернет!");
  }
  if (error.response) {
    // debugger
    let _error = error.response;
    switch (_error.status) {
      case 400:
        alert("Плохой запрос");
        break;
      case 401:
        Redirect("Неавторизованный", hooks);
        break;
      case 403:
        Redirect("Запрещенный");
        break;
      case 404:
        alert("Не обнаружена");
        break;
      case 500:
        alert("Внутренняя Ошибка Сервера");
        break;
      default:
        break;
    }
  }
}

const init = {
  request(
    method,
    url,
    params,
    data,
    hooks,
    formdata = false,
    auth = false,
    isBranchUser = false
  ) {
    let config = {
      baseURL: auth
        ? process.env.REACT_APP_BASE_URL_AUTH
        : process.env.REACT_APP_BASE_URL,
      timeout: 900000,
      url: url,
      method: method,
      onUploadProgress: function (e) {
        Math.round((e.loaded * 100) / e.total);
      },
    };
    if (!token) {
      config.headers = {
        "platform-id": "7d4a4c38-dd84-4902-b744-0488b80a4c01",
      };
    }
    if (token) {
      config.headers = {
        Authorization: "Bearer " + token,
        "platform-id": "7d4a4c38-dd84-4902-b744-0488b80a4c01",
      };
    }
    if (data) config.data = data;

    if (params) config.params = params;

    let result = axios(config);

    return new Promise((resolve, reject) => {
      result
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          if (!isBranchUser) ErrorHandler(error, hooks);
          reject(error);
        });
    });

  },

  get(url, params, hooks) {
    const superadmin = JSON.parse(user_data)?.type === `SUPER-ADMIN`;

    return this.request(
      "GET",
      url,
      { ...(superadmin ? {} : { branch_id }), ...params },
      undefined,
      hooks
    );
  },
  getC(url, params, hooks) {
    return this.request("GET", url, { ...params }, undefined, hooks);
  },

  post(url, data, params, hooks) {
    return this.request("POST", url, params, data, hooks);


  },
  put(url, data, params, hooks) {
    return this.request("PUT", url, params, data, hooks);
  },
  remove(url, data, params, hooks) {
    return this.request("DELETE", url, params, data, hooks);
  },
  post_login(url, data, params, hooks) {
    return this.request("POST", url, params, data, hooks, false, true);
  },
  post_branch_login(url, data, params, hooks) {
    return this.request("POST", url, params, data, hooks, false, true, true);
  },
};

export default init;
