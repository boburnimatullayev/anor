import { useSelector } from 'react-redux';
export default function usePermissions(value, trueForSimpleAdmin = false) {
  let userData = JSON.parse(window.localStorage.getItem('user-data'));
  if(trueForSimpleAdmin) return true 
  else if (userData.type === 'SIMPLE-ADMIN') return false;
  if (userData.type !== 'SUPER-ADMIN') {
    const permissions = useSelector((state) => state.auth.permissions);
    const onePermission = permissions.find((item) => item.name === value);
    if (onePermission?.name?.length > 0) return true;
    return false;
  }
  return true;
}
