export default function (str) {
  return !!str
    ? str
        ?.toLowerCase()
        .trim()
        // .replace(/ /g, '')
        .split(' ')
        .map((i) => { 
          if(i !== "")
          return i[0]?.toUpperCase() + i?.slice(1)}
          )
        .join(' ')
    : ''
}


