import { store } from '../redux/store';

function guard(route) {
  let userData = JSON.parse(window.localStorage.getItem('user-data')) || {};
  let permissions = [];
  if (userData.type === 'SUPER-ADMIN') {
    permissions = [
      'dashboard',
      'orders',
      'orders/update',
      'orders/create',
      'couriers',
      'couriers/update',
      'couriers/create',
      'reports',
      'reports/by-representatives',
      'reports/by-branches',
      'reports/by-products',
      'reports/by-excel',
      'reports/by-call',
      'reports/by-nfc',
      'reports/by-status',
      'reports/by-courier-status',
      'reports/by-canceled',
      'reports/by-canceled-reason',
      'reports/total',
      'reports/by-daily-volume',
      'reports/by-distribution',
      'reports/by-bugs',
      'reports/create',
      'spravochnik',
      'catalog',
      'category',
      'category/create',
      'category/update',
      'product',
      'product/create',
      'product/update',
      'docs',
      'docs/create',
      'docs/update',
      'geozona',
      'geozona/create',
      'geozona/update',
      'filial',
      'filial/create',
      'filial/update',
      'notification',
      'notification/create',
      'user',
      'user/create',
      'user/update',
      'superviser',
      'superviser/create',
      'superviser/update',
      'zarplata',
      'configs',
      'settings',
      'catalog',
      'permission',
    ];
  }
  if (userData.type === 'SIMPLE-ADMIN') {
    permissions = [
      'dashboard',
      'orders',
      'couriers',
      'couriers/update',
      'couriers/create',
      'reports',
      'reports/by-representatives',
      'reports/by-branches',
      'reports/by-products',
      'reports/by-excel',
      'reports/by-call',
      'reports/by-nfc',
      'reports/by-status',
      'reports/by-courier-status',
      'reports/by-canceled',
      'reports/by-canceled-reason',
      'reports/total',
      'reports/by-daily-volume',
      'reports/by-distribution',
      'reports/by-bugs',
      'spravochnik',
      'catalog',
      'category',
      'product',
      'docs',
      'geozona',
      'filial',
      'notification',
      'user',
      'superviser',
      'zarplata',
      // 'configs',
      'settings',
      'catalog',
    ];
  }
  if (userData.type === 'BUG_TRACKER') {
    permissions = ['reports', 'reports/by-bugs'];
  }
  if (userData.type === 'BRANCH-ADMIN') {
    const state = store.getState()
    // console.log('state', state)
    
    permissions = state.auth.permissions?.map((item) => item.name)
  }
  let _isHave = false;
  if (permissions.includes(route)) _isHave = !_isHave;

  return _isHave;
}

export default guard;
