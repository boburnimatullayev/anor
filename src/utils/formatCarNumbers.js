export default function (str) {
  let result = []
  let strStick = str.replace(/\s+/g, '')
  if (isNaN(strStick[2])) {
    result = [
      strStick.slice(0, 2),
      ' ',
      strStick.slice(2, 3),
      ' ',
      strStick.slice(3, 6),
      ' ',
      strStick.slice(6),
    ]
  } else {
    result = [
      strStick.slice(0, 2),
      ' ',
      strStick.slice(2, 5),
      ' ',
      strStick.slice(5),
    ]
  }
  return result.join(' ')
}
