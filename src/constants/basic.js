import logo from '@/assets/images/anor_logo.svg'
const basic = {
    PRIMARY_COLOR: '#a7d129',
    SECONDARY_COLOR: "#f1f2f6",
    ACTIVE_COLOR: "#f1f2f6",
    DEFAULT_COLOR: "#8c2737",
    DEFAULT_BACKGRUONDCOLOR: "#f3e5eb",
    UNACTIVE_COLOR: "#f94340",
    UNACTIVE_BACKGROUNDCOLOR: "#ffece5",
    TITLE: 'MUNO',
    MENU_THEME: 'light',
    COMPANY_NAME: 'Udevs',
    LOGO: logo,
    LOGO2: 'https://www.shareicon.net/data/2017/01/06/868320_people_512x512.png',
    USER_LOGO: 'https://www.shareicon.net/data/2017/01/06/868320_people_512x512.png',
    BASE_URL: process.env.REACT_APP_BASE_URL,
    COURIER_MAP_URL: process.env.COURIER_MAP_URL
}
export default basic

export const sortOptions = [
    "id",
    "customer_name",
    "customer_phone",
    "from_time",
    "to_time",
    "branch_id",
    "status_id",
    "comment",
    "updated_at",
    "created_at"
] 

export const defaultMapState = { 
    center: [41.311151, 69.279737], 
    zoom: 12,
    controls: [
      "zoomControl", 
      "fullscreenControl",
      "geolocationControl",
      "rulerControl",
      "trafficControl",
      "typeSelector"
    ]
  }
