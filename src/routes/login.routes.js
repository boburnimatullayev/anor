import React from "react"
import Login from "../pages/Login/Login"
import { Route } from "react-router-dom"

export default function AuthRoutes () {
  return (
    <>
      <Route path='/login' exact component={Login} />
    </>
  )
}