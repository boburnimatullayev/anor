import React, { useEffect, useState } from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import routes from "../constants/router";
import guard from "../utils/permissions";
import nprogress from "nprogress";
import isInSuperAdmin from "@/utils/isInSuperAdmin";
import { useSelector } from "react-redux";

const Routes = () => {

  const [publicRouteList, setPublicRouteList] = useState([]);
  const [isGenerated, setIsGenerated] = useState(false);
  const permissions = useSelector((state) => state.auth.permissions);

  React.useState(nprogress.start());

  

  useEffect(() => {
      generatedRoutes(routes);
  }, [permissions]);

  // console.log('rout',permissions)


  useEffect(() => {
    nprogress.done();
    return () => nprogress.start();
  });

  const token = localStorage.getItem("token");
  function generatedRoutes(rout) {
    rout
      .filter((e) => guard(e.meta.permission))
      .forEach((e, i) => {
        if (e.children && e.children.length) {
          setPublicRouteList((old) => [
            ...old,
            <AppRoute key={e} exact path={e.path} component={e.component} />,
          ]);
          generatedRoutes(e.children);
        } else {
          setPublicRouteList((old) => [
            ...old,
            <AppRoute key={e} exact path={e.path} component={e.component} />,
          ]);
          setIsGenerated(true);
        }
      });
  }

  const AppRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => <Component {...props} />} />
  );

  return (
    <div>
      {isGenerated ? (
        <Switch>
          {publicRouteList}
          {token ? (
            <>
        
              <Redirect from="*" to="/404" />
            </>
          ) : (
            <Redirect from="*" to="/login" />
          )}
        </Switch>
      ) : undefined}
    </div>
  );
};

export default withRouter(Routes);
