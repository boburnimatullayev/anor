import React from 'react'
import { CalendarOutlined } from '@ant-design/icons'
import { DatePicker, Popover } from 'antd'
import { useTranslation } from 'react-i18next'
const { RangePicker } = DatePicker
import moment from 'moment'

const RangeFilter = (title, isVisible, setIsVisible, stateRange, setStateRange) => {
  const { t } = useTranslation()

  const content = (
    <RangePicker
      allowClear={false}
      showTime={{ format: 'HH:mm' }}
      format='HH:mm DD-MM-YYYY'
      value={stateRange}
      style={{ width: 300 }}
      defaultValue={[moment().startOf('isoWeek'), moment().endOf('isoWeek')]}
      onOk={() => setIsVisible(false)}
      ranges={{
        Сегодня: [moment().startOf('day').add(5, 'hours'), moment().endOf('day').add(5, 'hours')],
        'Эта неделя': [moment().startOf('isoWeek').add(5, 'hours'), moment().endOf('isoWeek').add(5, 'hours')],
        'Этот месяц': [moment().startOf('month').add(5, 'hours'), moment().endOf('month').add(5, 'hours')],
      }}
      onChange={(value) => setStateRange(value)}
    />
  )

  return (
    <div>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '100%',
        }}
      >
        <p style={{ margin: '0' }}>{t(title)}</p>
        <Popover
          placement='bottomRight'
          content={content}
          trigger='click'
          visible={isVisible}
          onVisibleChange={() => setIsVisible(!isVisible)}
        >
          <CalendarOutlined
            style={{ opacity: `${stateRange ? '1' : '.6'}` }}
            content='div'
            onClick={() => setIsVisible(!isVisible)}
          />
        </Popover>
      </div>
    </div>
  )
}

export default RangeFilter
