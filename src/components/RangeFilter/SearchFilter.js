import { Popover, Input } from "antd"
import React from "react"
import { useTranslation } from "react-i18next"
import { SearchOutlined } from "@ant-design/icons"
const { Search } = Input

const SearchFilter = ({ title, popoverVisible, setPopoverVisible, value, setValue }) => {
  const { t } = useTranslation()

  const onSearchChange = (val) => setValue(val?.trim())

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
        }}
      >
        <p style={{ margin: "0", color: "#000" }}>{t(title)}</p>
        <Popover
          placement="bottomRight"
          content={
            <Search style={{ width: 350, marginRight: 10 }} placeholder={t("type.text")} onSearch={onSearchChange} />
          }
          trigger="click"
          visible={popoverVisible}
          onVisibleChange={() => setPopoverVisible(!popoverVisible)}
        >
          <SearchOutlined
            style={{ opacity: `${value?.trim() ? "1" : ".5"}` }}
            content="div"
            onClick={() => setPopoverVisible(!popoverVisible)}
          />
        </Popover>
      </div>
    </div>
  )
}

export default SearchFilter
