import React from 'react'
import { Avatar } from 'antd'
import { UserOutlined, LogoutOutlined, BellOutlined } from '@ant-design/icons'
import { Menu, Dropdown, Badge } from 'antd'
import { useHistory } from 'react-router-dom'
import './styles/style.less'
import basic from '../constants/basic'
import { logout } from '../redux/actions'
import { useDispatch } from 'react-redux'
import { useTranslation } from "react-i18next"

const user_data = localStorage.getItem("user-data")
const userData = user_data ? JSON.parse(user_data) : {}

function RightContent() {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const history = useHistory()
  const Logout = function () {
    localStorage.removeItem('token')
    localStorage.removeItem("user-data")
    dispatch(logout())
    localStorage.clear()
    history.push('/login')
    document.location.reload()
  }
  const menu = (
    <Menu>
      <Menu.Item
        onClick={() => Logout()}
      >
        <LogoutOutlined /> {t("log.out")}
      </Menu.Item>
    </Menu>
  )
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {/* <Badge style={{ marginRight: '10px', cursor: 'pointer' }} count={5}>
        <BellOutlined style={{ fontSize: '20px', marginRight: '10px' }} />
      </Badge> */}
      <Dropdown overlay={menu} className='dropdown'>
        <div className='right_content'>
          <Avatar
            size={36}
            className='avatar'
            src={userData.image ?? basic.USER_LOGO}
            icon={<UserOutlined />}
          />
          <h1 className='title'>{userData.name}</h1>
        </div>
      </Dropdown>
    </div>
  )
}

export default RightContent
