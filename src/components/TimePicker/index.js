import { Button, Popover, Select, DatePicker, TimePicker } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { DateRangePicker } from "react-date-range";
import "react-date-range/dist/theme/default.css";
import "react-date-range/dist/styles.css";
import moment from "moment";
import "./style.css";

const { Option } = Select;
const { RangePicker } = DatePicker;

function CustomTimePicker({ finalDate, setFinalDate }) {
  const [fromHour, setFromHour] = useState("00");
  const [fromMinute, setFromMinute] = useState("00");
  const [toHour, setToHour] = useState("23");
  const [toMinute, setToMinute] = useState("59");
  const [hours, setHours] = useState([]);
  const [minutes, setMinutes] = useState([]);
  const [show, setShow] = useState(false);

  const [selectedDate, setSelectedDate] = useState([
    {
      startDate: moment().toDate(),
      endDate: moment().add(1, 'days').toDate(),
      key: 'selection',
    },
  ])

  const handleSelectDate = (item) => {
    setSelectedDate([item.selection])
    localStorage.setItem(
      'dateFilter',
      JSON.stringify({
        start: moment(item.selection.startDate).format('YYYY-MM-DD'),
        end: moment(item.selection.endDate).format('YYYY-MM-DD'),
      })
    )
  }

  useEffect(() => {
    for (let i = 0; i < 24; i++) {
      setHours((prev) => [...prev, i.toString().length === 1 ? `0${i}` : i]);
    }
    for (let i = 0; i < 60; i++) {
      setMinutes((prev) => [...prev, i.toString().length === 1 ? `0${i}` : i]);
    }
  }, []);

  const onFromHourChange = (value) => {
    console.log(`selected ${value}`);
    if (!isNaN(+value) && +value < 24) {
      setFromHour(value);
    }
  };
  const onFromMinuteChange = (value) => {
    console.log(`selected ${value}`);
    if (!isNaN(+value) && +value < 60) {
      setFromMinute(value);
    }
  };
  const onToHourChange = (value) => {
    console.log(`selected ${value}`);
    if (!isNaN(+value) && +value < 24) {
      setToHour(value);
    }
  };
  const onToMinuteChange = (value) => {
    if (!isNaN(+value) && +value < 60) {
      setToMinute(value);
    }
  };

  const onSearch = (val) => {
    console.log('search:', val)
  }

  const makeDate = (val) => {
    const fromDate = new Date(val[0].startDate)
    const toDate = new Date(val[0].endDate)
    // setShow(false)
    setFinalDate([new Date(fromDate.setHours(fromHour, fromMinute)), new Date(toDate.setHours(toHour, toMinute))])
  }


  return (
    <div>
      {/* <Popover
        content={content}
        visible={show}
        trigger="click"
        placement="bottom"
      >
        <div onClick={() => setShow(true)}>
          <TimePicker
            showTime={{ format: "HH:mm" }}
            format="HH:mm"
            // value={[
            //   moment(finalDate[0]?.toString()),
            //   moment(finalDate[1]?.toString()),
            // ]}
            // style={{ width: 350 }}
            defaultValue = {moment('10:30', 'HH:mm')}
            open={false}
          />
        </div>
      </Popover> */}
      <div
        style={{
          display: "flex",
          marginLeft: '20px'
        }}
      >
        <div>
          <span style={{ marginRight: 10 }}>От:</span>
          <Select
            showSearch
            optionFilterProp="children"
            style={{ width: 45 }}
            onChange={onFromHourChange}
            notFoundContent={""}
            showArrow={false}
            defaultValue="00"
            onSearch={onSearch}
            filterOption={(input, option) =>
              option.children.toString().indexOf(input) >= 0
            }
          >
            {hours?.map((hour) => (
              <Option key={hour} value={hour}>
                {hour}
              </Option>
            ))}
          </Select>
          <span>&nbsp;:&nbsp;</span>
          <Select
            showSearch
            optionFilterProp="children"
            style={{ width: 45 }}
            onChange={onFromMinuteChange}
            notFoundContent={""}
            showArrow={false}
            onSearch={onSearch}
            defaultValue="00"
            filterOption={(input, option) =>
              option.children.toString().indexOf(input) >= 0
            }
          >
            {minutes?.map((hour) => (
              <Option key={hour} value={hour}>
                {hour}
              </Option>
            ))}
          </Select>
        </div>
        <div>
          <span style={{ margin: '0 10px' }}>До:</span>
          <Select
            showSearch
            optionFilterProp="children"
            style={{ width: 45 }}
            onChange={onToHourChange}
            defaultValue="23"
            notFoundContent={""}
            showArrow={false}
            onSearch={onSearch}
            filterOption={(input, option) =>
              option.children.toString().indexOf(input) >= 0
            }
          >
            {hours?.map((hour) => (
              <Option key={hour} value={hour}>
                {hour}
              </Option>
            ))}
          </Select>
          <span>&nbsp;:&nbsp;</span>
          <Select
            notFoundContent={""}
            showSearch
            optionFilterProp="children"
            style={{ width: 45 }}
            defaultValue="59"
            onChange={onToMinuteChange}
            showArrow={false}
            onSearch={onSearch}
            filterOption={(input, option) =>
              option.children.toString().indexOf(input) >= 0
            }
          >
            {minutes?.map((hour) => (
              <Option key={hour} value={hour}>
                {hour}
              </Option>
            ))}
          </Select>
         
        </div>
        <Button
          type='primary'
          onClick={() => makeDate(selectedDate)}
          style={{ backgroundColor: '#991848', borderColor: '#991848', width: 120, marginLeft: 15 }}
        >
          Принять
        </Button>
      </div>
    </div>
  );
}

export default CustomTimePicker;
