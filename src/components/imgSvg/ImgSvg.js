import "./style.css"
import React from "react"
import basic from "../../constants/basic"

export default function ImgSvg(props) {
  const { type = "withTitle", width, height, color = basic.DEFAULT_COLOR, ...args } = props

  const styles = {
    background: color,
    width: width,
    height: height,
  }

  return (
    <div style={styles} className={type === "withTitle" ? "brand-title-logo" : "brand-logo"}>

      {/* <span style={{...styles, maskImage: img}} {...props}></span> */}
    </div>
  )
}