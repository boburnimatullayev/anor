import React from 'react'
import { Input, Checkbox, Popover, Button } from 'antd'
import { useTranslation } from 'react-i18next'

import { FilterFilled } from '@ant-design/icons'

const { Search } = Input

const PopoverTitle = ({
  title,
  popoverVisible,
  setPopoverVisible,
  setChosenState,
  dataList,
  getValue,
  setGetValue,
  setSearchedWord,
  topfilter,
}) => {
  const { t } = useTranslation()

  const onChangeFilterRadio = (checkedValues) => {
    setGetValue(checkedValues)
  }

  const onSearch = (e) => {
    setSearchedWord(e.target.value.toLocaleLowerCase())
  }

  const handleFilterer = () => {
    setChosenState(getValue)
    setPopoverVisible(false)
  }

  const content = (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        border: '1px solid #ccc',
        padding: '10px',
        borderRadius: '5px',
        maxWidth: '350px',
      }}
    >
      <Search
        placeholder={t('type.text')}
        onChange={onSearch}
        style={{ width: '100%', marginBottom: '20px' }}
        allowClear
      />
      <Checkbox.Group
        onChange={onChangeFilterRadio}
        value={getValue}
        style={{
          borderBottom: '1px solid #ccc',
          marginBottom: '10px',
          paddingBottom: '10px',
          maxHeight: '190px',
          overflow: 'hidden',
          overflowY: 'scroll',
        }}
      >
        {dataList.map((radio, i) => {
          return (
            <div key={i} style={{ padding: '0 15px', display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
              <Checkbox key={radio.value} value={radio.value}>
                {radio.text.slice(0, 1).toUpperCase() + radio.text.slice(1)}
              </Checkbox>
            </div>
          )
        })}
      </Checkbox.Group>
      <Button
        disabled={getValue ? false : true}
        onClick={handleFilterer}
        style={{ width: '50px', alignSelf: 'flex-end' }}
        size='small'
        type='primary'
      >
        OK
      </Button>
    </div>
  )

  return (
    <div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <p style={{ margin: '0', color: topfilter && '#999' }}>{t(title)}</p>
        <Popover
          placement='bottomRight'
          content={content}
          trigger='click'
          visible={popoverVisible}
          onVisibleChange={() => setPopoverVisible(!popoverVisible)}
        >
          <FilterFilled
            style={{ opacity: `${getValue ? '1' : '.5'}` }}
            content='div'
            onClick={() => setPopoverVisible(!popoverVisible)}
          />
        </Popover>
      </div>
    </div>
  )
}

export default PopoverTitle
