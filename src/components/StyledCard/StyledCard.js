import React from "react"
import moment from 'moment'
import { Spin } from 'antd';
import { EnvironmentOutlined, RightOutlined, ProfileOutlined, UserOutlined, PhoneOutlined, ClockCircleOutlined } from '@ant-design/icons'
import './style.css'

export default function StyledCard({ type, style, orders, loading, onViewClick}) {
    return (
        <div style={style} className={`styled-card ${type === "delivered" ? "delivered" : type === "canceled" ? "canceled" : "active"}`}>
            <div className="card-content">
                {orders.length ? orders.map(({customer_name, customer_phone, delivery_time, customer_address, products, id}, i) => (
                    <div key={id} style={{padding: "10px 0", borderTop: i !== 0 ? "1px solid #8B0037" : "none", width: "100%"}}>
                        <div style={{display: "flex", alignItems: "center"}}>
                            <UserOutlined style={{ color: "#8B0037", marginRight: "14px" }} />
                            <span>{customer_name}</span>
                        </div>
                        <div style={{display: "flex", alignItems: "center"}}>
                            <PhoneOutlined style={{ color: "#8B0037", marginRight: "14px" }} />
                            <span>{customer_phone}</span>
                        </div>
                        <div style={{display: "flex", alignItems: "center"}}>
                            <ClockCircleOutlined style={{ color: "#8B0037", marginRight: "14px" }} />
                            <span>{moment(delivery_time).subtract(5, 'h').format('YYYY-MM-DD HH:mm')}</span>
                        </div>
                        <div style={{display: "flex", alignItems: "center"}}>
                            <EnvironmentOutlined style={{ color: "#8B0037", marginRight: "14px" }} />
                            <span>{customer_address}</span>
                        </div>
                        
                    </div>
                )) : <></>}
                {/* <h1>{client}</h1>
                {location && <EnvironmentFilled style={{ color: "#8B0037", marginRight: "8px" }} />}
                <span>{location}</span><br />
                {cart && <ProfileOutlined style={{ color: "#8B0037", marginRight: "8px" }} />}
                <span>{cart}</span> */}
            </div>
            <RightOutlined onClick={onViewClick} className="view-order" />
            {loading && <div className="loading">
                <Spin />
            </div>}
        </div>
    )
}