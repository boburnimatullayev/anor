import { Button, Popover, Select, DatePicker } from 'antd'
import React, { useEffect, useState, useRef } from 'react'
import { DateRangePicker } from 'react-date-range'
import 'react-date-range/dist/theme/default.css'
import { CalendarOutlined } from '@ant-design/icons'
import 'react-date-range/dist/styles.css'
import moment from 'moment'
import * as rdrLocales from 'react-date-range/dist/locale'
import { defaultStaticRanges } from './customRanges'
import './style.css'
import { useTranslation } from 'react-i18next'

const { Option } = Select
const { RangePicker } = DatePicker

function CustomDatePicker({ title = '', finalDate, setFinalDate, asFilter = false }) {
  const wrapperRef = useRef()
  const { t } = useTranslation()

  const [isVisible, setIsVisible] = useState(false)

  const [fromHour, setFromHour] = useState('00')
  const [fromMinute, setFromMinute] = useState('00')
  const [toHour, setToHour] = useState('23')
  const [toMinute, setToMinute] = useState('59')
  const [hours, setHours] = useState([])
  const [minutes, setMinutes] = useState([])
  const [selectedDate, setSelectedDate] = useState([
    {
      startDate: moment().subtract(1, 'days').toDate(),
      endDate: moment().toDate(),
      key: 'selection',
    },
  ])

  const handleSelectDate = (item) => {
    setSelectedDate([item.selection])
    localStorage.setItem(
      'dateFilter',
      JSON.stringify({
        start: moment(item.selection.startDate).format('YYYY-MM-DD'),
        end: moment(item.selection.endDate).format('YYYY-MM-DD'),
      })
    )
  }

  useEffect(() => {
    for (let i = 0; i < 24; i++) {
      setHours((prev) => [...prev, i.toString().length === 1 ? `0${i}` : i])
    }
    for (let i = 0; i < 60; i++) {
      setMinutes((prev) => [...prev, i.toString().length === 1 ? `0${i}` : i])
    }
  }, [])

  const onFromHourChange = (value) => {
    if (!isNaN(+value) && +value < 24) {
      setFromHour(value)
    }
  }
  const onFromMinuteChange = (value) => {
    if (!isNaN(+value) && +value < 60) {
      setFromMinute(value)
    }
  }
  const onToHourChange = (value) => {
    if (!isNaN(+value) && +value < 24) {
      setToHour(value)
    }
  }
  const onToMinuteChange = (value) => {
    if (!isNaN(+value) && +value < 60) {
      setToMinute(value)
    }
  }

  const onSearch = (val) => {
    console.log('search:', val)
  }

  const makeDate = (val) => {
    const fromDate = new Date(val[0].startDate)
    const toDate = new Date(val[0].endDate)
    setIsVisible(false)
    setFinalDate([new Date(fromDate.setHours(fromHour, fromMinute)), new Date(toDate.setHours(toHour, toMinute))])
  }

  function disabledDate(current) {
    return current && current > moment().endOf('day');
  }
  
  const content = (
    <div ref={wrapperRef}>
      <div style={{ display: 'block', flexDirection: 'column' }}>
        <DateRangePicker
          disabledDay={disabledDate}
          locale={rdrLocales.ru}
          rangeColors={['#991848', '#ff00ff', '#00ff00']}
          ranges={selectedDate}
          staticRanges={defaultStaticRanges}
          onChange={handleSelectDate}
          inputRanges={[]}
        />
        <div
          style={{
            borderTop: '1px solid #e3e3e3',
            alignSelf: 'end',
            width: '100%',
            paddingTop: 10,
            textAlign: 'end',
          }}
        >
          <div
            style={{
              display: 'block',
              width: '24%',
              position: 'absolute',
              top: '60%',
              left: '15%',
            }}
          >
            <div>
              <span style={{ marginRight: 10 }}>От:</span>
              <Select
                getPopupContainer={(trigger) => trigger.parentElement}
                showSearch
                optionFilterProp='children'
                style={{ width: 45, marginBottom: '10px' }}
                onChange={onFromHourChange}
                notFoundContent={''}
                showArrow={false}
                defaultValue='00'
                onSearch={onSearch}
                filterOption={(input, option) => option.children.toString().indexOf(input) >= 0}
              >
                {hours?.map((hour) => (
                  <Option key={hour} value={hour}>
                    {hour}
                  </Option>
                ))}
              </Select>
              <span>&nbsp;:&nbsp;</span>
              <Select
                getPopupContainer={(trigger) => trigger.parentElement}
                showSearch
                optionFilterProp='children'
                style={{ width: 45 }}
                onChange={onFromMinuteChange}
                notFoundContent={''}
                showArrow={false}
                onSearch={onSearch}
                defaultValue='00'
                filterOption={(input, option) => option.children.toString().indexOf(input) >= 0}
              >
                {minutes?.map((hour) => (
                  <Option key={hour} value={hour}>
                    {hour}
                  </Option>
                ))}
              </Select>
            </div>
            <div>
              <span style={{ marginRight: 10 }}>До:</span>
              <Select
                getPopupContainer={(trigger) => trigger.parentElement}
                showSearch
                optionFilterProp='children'
                style={{ width: 45 }}
                onChange={onToHourChange}
                defaultValue='23'
                notFoundContent={''}
                showArrow={false}
                onSearch={onSearch}
                filterOption={(input, option) => option.children.toString().indexOf(input) >= 0}
              >
                {hours?.map((hour) => (
                  <Option key={hour} value={hour}>
                    {hour}
                  </Option>
                ))}
              </Select>
              <span>&nbsp;:&nbsp;</span>
              <Select
                notFoundContent={''}
                getPopupContainer={(trigger) => trigger.parentElement}
                showSearch
                optionFilterProp='children'
                style={{ width: 45 }}
                defaultValue='59'
                onChange={onToMinuteChange}
                showArrow={false}
                onSearch={onSearch}
                filterOption={(input, option) => option.children.toString().indexOf(input) >= 0}
              >
                {minutes?.map((hour) => (
                  <Option key={hour} value={hour}>
                    {hour}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
          <Button
            type='primary'
            onClick={() => makeDate(selectedDate)}
            style={{
              backgroundColor: '#991848',
              borderColor: '#991848',
              width: 120,
              marginTop: 15,
            }}
          >
            Принять
          </Button>
        </div>
      </div>
    </div>
  )

  return (
    <div
      ref={wrapperRef}
      className='App'
      style={{
        width: 'fit-content',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: title ? '100%' : 'initial',
      }}
    >
      {title && <div>{t(title)}</div>}
      <div>
        <Popover
          onVisibleChange={() => setIsVisible(!isVisible)}
          content={content}
          visible={isVisible}
          trigger='click'
          placement='bottom'
        >
          <div>
            {asFilter ? (
              <CalendarOutlined content='div' onClick={() => setIsVisible(!isVisible)} />
            ) : (
              <div>
                <RangePicker
                  showTime={{ format: 'HH:mm' }}
                  format='DD-MM-YYYY'
                  value={[moment(finalDate[0]?.toString()), moment(finalDate[1]?.toString())]}
                  style={{ width: 240 }}
                  defaultValue={[moment().startOf('isoWeek'), moment().endOf('isoWeek')]}
                  open={false}
                />
              </div>
            )}
          </div>
        </Popover>
      </div>
    </div>
  )
}

export default CustomDatePicker
