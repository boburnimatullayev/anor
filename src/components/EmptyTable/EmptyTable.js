import React from "react"
import "./style.less"
import EmptyIcon from "../../assets/icons/empty-table.svg"
import { useTranslation } from "react-i18next"

function EmptyTable({ message = "no.data" }) {
  const { t } = useTranslation()

  return (
    <div className="empty">
      <img src={EmptyIcon} alt="empty table" width="100" height="100" />
      <h6 className="message">{t(message)}</h6>
    </div>
  )
}

export default EmptyTable
