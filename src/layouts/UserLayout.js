import React from "react"

function UserLayout ({ children }) {
    return (
        <div style={{ width: '100%', height: '100%' }}>
            {children}
        </div>
    )
}


export default UserLayout
