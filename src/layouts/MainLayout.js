import React, { useState, useEffect } from 'react'
import { Layout, ConfigProvider } from 'antd'
import { useHistory } from 'react-router-dom'
import RightContent from '@/components/RightContent'
import MenuHeader from '@/components/MenuHeader'
import MainMenu from '@/components/menu/Menu'
import basic from '@/constants/basic'
import ruRU from 'antd/lib/locale/ru_RU'
import '@/assets/styles/layout.less'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'
import axios_init from '@/utils/axios_init'
import { setUserPermissions } from '../redux/actions/authActions'
const { Header, Content, Sider } = Layout

// ***********************WEBSOCKET****************************************

const token = localStorage.getItem('token')

// ***********************WEBSOCKET****************************************

export default function MainLayout({ children }) {

  const [collapsed, setCollapsed] = useState(false)
  // const isAuthorited = useSelector((state) => state.auth.accessToken)
  const roleIdFromStorage = localStorage.getItem('role_id')
  const roleID =
    roleIdFromStorage !== 'undefined'
      ? JSON.parse(localStorage.getItem('role_id'))
      : null
  const dispatch = useDispatch()
  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    if (!token) {
      history.push('/login')
    }
  }, [])

  const getPermissions = () => {
  // console.log('MainLayout')
    if (roleID)
      axios_init
        .get(`auth/role/${roleID}`)
        .then(({ data }) => {
  
          dispatch(setUserPermissions(data.permissions))
        })
        .catch((err) => {
          console.log(err)
        })
  }

  useEffect(() => {
    if (token) {
      getPermissions()
    }
  }, [location])

  return (
    <div className={`App ${token ? '' : 'd-none'}`}>

      <Layout style={{ minHeight: '100vh' }}>
        <Header
          className='site-layout-background header'
          style={{ padding: 0, backgroundColor: '#fff' }}
        >

          {/* sdfsd */}
          {/* {collapsed ? (
            <MenuUnfoldOutlined
              onClick={() => {
                setCollapsed(!collapsed)
              }}
              className='menuIcon'
            />
          ) : (
            <MenuFoldOutlined
              onClick={() => {
                setCollapsed(!collapsed)
              }}
              className='menuIcon'
            />
          )} */}
          <MenuHeader />
         
          <RightContent />
        </Header>
        <Layout className='site-layout'>
          <Sider
            className='sider'
            trigger={null}
            collapsible
            theme={basic.MENU_THEME}
            width={250}
            collapsed={collapsed}
            onCollapse={() => setCollapsed(!collapsed)}
          >
            <MainMenu />
          </Sider>
          <Content style={{ margin: '0 16px',}} className='mainBox'>
            <ConfigProvider locale={ruRU}>{children}</ConfigProvider>
          </Content>
          {/*<Footer style={{ textAlign: 'center' }}> ©2021 Created by Udevs</Footer>*/}
        </Layout>
      </Layout>
    </div>
  )
}
