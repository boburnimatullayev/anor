import './style.css'
import React, { useState, useEffect, useRef } from 'react'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { useParams } from 'react-router'
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  message,
  Checkbox,
  Badge,
  DatePicker,
} from 'antd'
import { Table } from 'antd'
import PopoverTitle from '../../components/MultiSelect/PopoverTitle'
const { Search } = Input

const returnedDocsListStatuses = ['completed', 'finished']
const returnedCardsListStatuses = [
  'cancelled',
  'rejected',
  'cancelled_no_return',
  'cancelled_change_date',
  'cancelled_not_identified',
]

export default function CreateBranch() {
  const { t } = useTranslation()
  const history = useHistory()
  const state = useLocation().state
  const params = useParams()

  const [loading, setLoading] = useState(false)
  const [isFetching, setIsFetching] = useState(true)
  const [searchText, setSearchText] = useState('')
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })
  const [popoverCouriersFilter, setPopoverCouriersFilter] = useState(false)
  const [choosenCourierFilter, setChoosenCourierFilter] = useState('')
  const [returnedDocsList, setReturnedDocsList] = useState([])
  const [returnedCardsList, setReturnedCardsList] = useState([])
  const [checkedDocs, setCheckedDocs] = useState([])
  const [checkedCards, setCheckedCards] = useState([])
  const [newCheckedIds, setNewCheckedIds] = useState([])
  const [courierList, setCourierList] = useState([])
  const [rejectedDocs, setRejectedDocs] = useState([])
  const [date, setDate] = useState(moment())
  const [getValueCourierFilter, setGetValueCourierFilter] = useState(undefined)
  const [searchedWordCourierFilter, setSearchedWordCourierFilter] = useState('')

  useEffect(() => {
    getSingleSupervisorEmployees()
  }, [date, choosenCourierFilter, searchText])

  useEffect(() => getRejectedDocs(), [date])

  const error = () => {
    message.error(t('wrong.password'))
  }

  const getSingleSupervisorEmployees = () => {
    if (params.id) {
      setLoading(true)

      axios_init
        .get(`/supervisor/employees/${params.id}`, {
          password: state.password,
        })
        .then((data) => {
          if (!data?.data?.couriers_id) return
          axios_init
            .get(`/order`, {
              courier_id: choosenCourierFilter.length
                ? getValueCourierFilter.join()
                : data?.data?.couriers_id.join(),
              delivery_time_from: moment(date)
                .set({ hour: 0, minute: 0 })
                .add(5, 'h'),
              delivery_time_to: moment(date)
                .set({ hour: 23, minute: 59 })
                .add(5, 'h'),
              search: searchText,
              limit: 1000,
            })
            .then(({ data }) => {
              setReturnedDocsList(
                data.orders.filter((item) =>
                  returnedDocsListStatuses.includes(item.status_id)
                )
              )
              setReturnedCardsList(
                data.orders.filter((item) =>
                  returnedCardsListStatuses.includes(item.status_id)
                )
              )
              setCheckedDocs(
                data.orders
                  .filter(
                    (item) =>
                      returnedDocsListStatuses.includes(item.status_id) &&
                      item.is_confirmed
                  )
                  .map((item) => item.id)
              )
              setCheckedCards(
                data.orders
                  .filter(
                    (item) =>
                      returnedCardsListStatuses.includes(item.status_id) &&
                      item.is_confirmed
                  )
                  .map((item) => item.id)
              )
            })
            .catch((err) => {
              console.log(err)
            })
            .finally(() => {
              setLoading(false)
              setIsFetching(false)
            })

          getCouriers(data.data.couriers_id)
        })
        .catch((err) => {
          error()
          setTimeout(() => {
            history.push('/supervisor')
          }, 500)
        })
        .finally(() => {
          setLoading(false)
          setIsFetching(false)
        })
    } else {
      setIsFetching(false)
    }
  }

  const onFinish = (values) => {
    setLoading(true)

    if (params.id) {
      axios_init
        .put(`/order-confirm`, {
          to_false: '',
          to_true: newCheckedIds.length ? newCheckedIds.join() : null,
        })
        .then((res) => {
          message.success(t('updated.successfully'))
          window.location.reload()
        })
        .catch((err) => {
          console.log(err)
          message.error(t('updating.failed'))
        })
        .finally(() => setLoading(false))
    } else {
      setLoading(false)
      history.push('/supervisor')
    }
  }

  const getCouriers = (courierId) => {
    axios_init
      .get('/order/courier-orders', {
        courier_id: courierId.length ? courierId.join() : null,
        delivery_time_from: moment(date)
          .set({ hour: 0, minute: 0 })
          .add(5, 'h'),
        delivery_time_to: moment(date)
          .set({ hour: 23, minute: 59 })
          .add(5, 'h'),
        limit: 1000,
      })
      .then(({ data }) => {
        let couriers = data.map((item) => ({
          value: item.courier_id,
          text: item.courier_name,
          order_count: item.count,
        }))
        setCourierList(couriers)
      })
      .catch((err) => console.log(err))
      .finally(() => setIsFetching(false))
  }

  const onSearchChange = (text) => {
    setSearchText(text)
    setReqQuery((old) => ({ ...old, offset: 0 }))
  }

  const getRejectedDocs = () => {
    axios_init
      .get('/report/rejected-documents', {
        id: params.id,
        password: history.location.state.password,
        from_time: moment(date).set({ hour: 0, minute: 0 }).add(5, 'h'),
        to_time: moment(date).set({ hour: 23, minute: 59 }).add(5, 'h'),
      })
      .then((res) => {
        console.log('response ==', res)
        setRejectedDocs(res.data)
      })
      .catch((err) => console.log(err))
      .finally(() => setIsFetching(false))
  }

  const routes = [
    {
      name: 'supervisor',
      route: '/supervisor',
      link: true,
    },

    {
      name: t('employees'),
      route: '/supervisor/employees',
      link: false,
    },
  ]

  const titleOne = (title) => {
    let num
    let numDocs = returnedDocsList?.length
    let numCards = returnedCardsList?.length
    if (title.toLowerCase() === 'returned.documents') num = numDocs
    if (title.toLowerCase() === 'returned.cards') num = numCards
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <p style={{ margin: 0 }}>{t(`${title}`)}</p>
        <Badge count={num} style={{ background: '#8B0037' }}></Badge>
      </div>
    )
  }

  const columnsDocs = [
    {
      title: titleOne('returned.documents'),
      dataIndex: 'customer_name',
      width: '85%',
    },
  ]
  const columnsCards = [
    {
      title: titleOne('returned.cards'),
      dataIndex: 'customer_name',
      width: '85%',
    },
  ]

  const columnsCouriers = [
    {
      title: '№',
      width: '7%',
      render: (record, _, i) => {
        return i + 1
      },
    },
    {
      title: t('couriers'),
      dataIndex: 'text',
      width: '55%',
    },
    {
      title: t('count'),
      dataIndex: 'order_count',
      width: '30%',
    },
  ]

  const rejectedDocsColumn = [
    {
      title: '№',
      width: '7%',
      render: (record, _, i) => {
        return i + 1
      },
    },
    {
      title: t('couriers'),
      dataIndex: 'courier_name',
      width: '30%',
    },
    {
      title: t('clients'),
      dataIndex: 'customer_name',
      width: '30%',
    },
    {
      title: t('files'),
      dataIndex: 'file_url',
      width: '30%',
      render: (url, record) => <a href={`${url}`}>{record.file_name}</a>,
    },
  ]

  const onChangeDate = (value) => {
    setDate(value)
  }

  const title = (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Search
        allowClear
        style={{ width: 350, marginRight: 10 }}
        placeholder={`${t('search')}...`}
        onSearch={onSearchChange}
      />
      <div
        style={{
          minWidth: 200,
          border: '1px solid #bbb',
          fontSize: 14,
          padding: '4px 8px',
          borderRadius: 5,
          marginRight: 10,
        }}
      >
        {!isFetching ? (
          <PopoverTitle
            title='couriers'
            popoverVisible={popoverCouriersFilter}
            setPopoverVisible={setPopoverCouriersFilter}
            setChosenState={setChoosenCourierFilter}
            dataList={courierList}
            getValue={getValueCourierFilter}
            setGetValue={setGetValueCourierFilter}
            searchedWord={searchedWordCourierFilter}
            setSearchedWord={setSearchedWordCourierFilter}
            topfilter='topfilter'
          />
        ) : (
          <></>
        )}
      </div>
      <DatePicker
        allowClear={false}
        style={{ width: 150 }}
        placeholder={t('select.date')}
        value={date}
        onChange={onChangeDate}
      />
    </div>
  )

  const onCheckItem = (element, status) => {
    if (status) {
      setNewCheckedIds((old) => [...old, element.id])
    } else {
      setNewCheckedIds((old) => old.filter((el) => el !== element.id))
    }
  }

  useEffect(() => {}, [newCheckedIds])

  return (
    <div className='branch-user'>
      <BreadCrumbTemplete routes={routes} />

      <div className='create-edit'>
        {!isFetching ? (
          <Form layout='vertical' onFinish={onFinish}>
            <Card title={title}>
              <Row style={{ margin: '0 -10px' }}>
                <Col span='7' style={{ padding: '0 10px' }}>
                  <Table
                    rowSelection={{
                      selectedRowKeys: [...checkedDocs, ...newCheckedIds],
                      onSelect: onCheckItem,
                      getCheckboxProps: (record) => ({
                        disabled: record.is_confirmed,
                      }),
                      type: Checkbox,
                      hideSelectAll: true,
                    }}
                    bordered
                    scroll={{ y: 440 }}
                    dataSource={
                      returnedDocsList.length > 0 ? returnedDocsList : null
                    }
                    columns={columnsDocs}
                    rowKey={(record) => record.id}
                    pagination={false}
                  />
                </Col>
                <Col span='7' style={{ padding: '0 10px' }}>
                  <Table
                    rowSelection={{
                      selectedRowKeys: [...checkedCards, ...newCheckedIds],
                      onSelect: onCheckItem,
                      getCheckboxProps: (record) => ({
                        disabled: record.is_confirmed,
                      }),
                      hideSelectAll: true,
                    }}
                    bordered
                    scroll={{ y: 440 }}
                    dataSource={returnedCardsList}
                    columns={columnsCards}
                    rowKey={(record) => record.id}
                    pagination={false}
                  />
                </Col>
                <Col span='10' style={{ padding: '0 10px' }}>
                  <Table
                    bordered
                    scroll={{ y: 440 }}
                    dataSource={courierList}
                    columns={columnsCouriers}
                    pagination={false}
                  />
                </Col>
                <Col span='10' style={{ padding: '0 10px', marginTop: 15 }}>
                  <Table
                    bordered
                    scroll={{ y: 400 }}
                    dataSource={rejectedDocs}
                    columns={rejectedDocsColumn}
                    pagination={false}
                  />
                </Col>
              </Row>
            </Card>
            <Row
              justify='end'
              style={{ paddingTop: '24px', backgroundColor: '#F9F9F9' }}
            >
              <Button
                type='primary'
                disabled={newCheckedIds.length ? false : true}
                htmlType='submit'
                loading={loading}
              >
                {t('accept')}
              </Button>
            </Row>
          </Form>
        ) : (
          'loading'
        )}
      </div>
    </div>
  )
}
