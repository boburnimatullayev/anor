import "./style.css";
import React, { useEffect, useState } from "react";
import StyledTag from "@/components/StyledTag/StyledTag";
import axios_init from "@/utils/axios_init";
import StyledButton from "@/components/StyledButton/StyledButton";
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "../../components/breadcrumb/BreadCrumbTemplete";
import { useHistory, useLocation } from "react-router-dom";
import { EditFilled, DeleteFilled, PlusOutlined } from "@ant-design/icons";
import {
  Row,
  Col,
  Card,
  Button,
  Form,
  Table,
  Popconfirm,
  Input,
  message,
  Modal,
} from "antd";
import { useSelector } from "react-redux";
import usePermission from "@/utils/usePermission";
const { Search } = Input;

export default function BranchUser() {
  const { t } = useTranslation();
  const history = useHistory();

  const [inputModal, setInputModal] = useState("");
  const [modalType, setModalType] = useState("");

  const [visible, setVisible] = useState(null);
  const [loading, setLoading] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 });
  const [sort, setSort] = useState({ order: "", arrangement: "" });
  const [searchText, setSearchText] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [recordId, setRecordId] = useState("");
  const [branches, setBranches] = useState([]);
  const userData = localStorage.getItem("user-data");

  useEffect(() => {
    getItems();
    getBranches();
  }, [reqQuery, sort, searchText]);

  const getItems = () => {
    setTableLoading(true);

    if (JSON.parse(userData)?.type === 'SUPER-ADMIN') {
      axios_init
        .getC("/supervisor", {
          limit: reqQuery.limit,
          offset: reqQuery.offset,
          search: searchText,
        })
        .then(({ data }) => {
          setItems(data);
        })
        .catch((err) => console.log(err))
        .finally(() => setTableLoading(false));
    }
    else{
      axios_init
        .get("/supervisor", {
          limit: reqQuery.limit,
          offset: reqQuery.offset,
          search: searchText,
        })
        .then(({ data }) => {
          setItems(data);
        })
        .catch((err) => console.log(err))
        .finally(() => setTableLoading(false));
    }
  };
  const getBranches = () => {
    setTableLoading(true);
    axios_init
      .get("/branch", {
        limit: 0,
        offset: 0,
      })
      .then(({ data }) => {
        setBranches(data.branches);
      })
      .catch((err) => console.log(err))
      .finally(() => setTableLoading(false));
  };

  const handleDelete = (val) => {
    setLoading(true);
    axios_init
      .remove(`/supervisor/${val.id}`)
      .then((res) => {
        getItems();
        message.success(t("deleted.successfully"));
      })
      .catch((err) => {
        console.log(err);
        message.error(t("deleting.failed"));
      })
      .finally(() => {
        setLoading(false);
        setVisible(null);
      });
  };

  const ExtraButton = function () {
    return (
      <Button
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => {
          history.push("/supervisor/create");
        }}
      >
        {t("add")}
      </Button>
    );
  };

  const onSortChange = (pagination, filters, sorter) => {
    setSort({ order: sorter.columnKey, arrangement: sorter.order });
  };

  const onSearchChange = (text) => {
    setSearchText(text);
    setReqQuery((old) => ({ ...old, offset: 0 }));
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const modalOnClick = (record, type) => {
    setModalVisible(true);
    setModalType(type);
    setRecordId(record.id);
  };

  const modalHideClick = () => {
    setModalVisible(false);
  };

  const warning = () => {
    message.warning(t("please.input.password"));
  };

  const submitModal = () => {
    if (inputModal.length > 0) {
      history.push({
        pathname: `/supervisor/employees/${recordId}`,
        state: { password: inputModal, supervisorId: recordId },
      });
    } else {
      warning();
    }
  };

  const editModal = () => {
    if (inputModal.length > 0) {
      history.push({
        pathname: `/supervisor/edit/${recordId}`,
        state: { password: inputModal, supervisorId: recordId },
      });
    } else {
      warning();
    }
  };

  const columns = [
    {
      title: t("FIO"),
      dataIndex: "name",
      key: "name",
      render: (name, record) => {
        return (
          <a
            className="employee-link"
            onClick={() => modalOnClick(record, "name")}
          >
            {name}
          </a>
        );
      },
    },
    {
      title: t("phone.number"),
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: t("branches"),
      dataIndex: "branch_id",
      key: "branch_id",
      render: (record) => {
        let userBranch = branches?.find((item) => item.id === record);
        return userBranch?.name;
      },
    },
    {
      title: t("actions"),
      key: "actions",
      align: "center",
      render: (text, record, index) => (
        <div>
          {usePermission("superviser/update") && (
            <StyledButton
              color="link"
              icon={EditFilled}
              onClick={() => modalOnClick(record, "edit")}
              tooltip={t("edit.order")}
            />
          )}
          <Popconfirm
            title={t("do.you.really.want.to.delete")}
            visible={visible === index}
            onConfirm={() => handleDelete(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t("cancel")}
            okText={t("yes")}
          >
            {usePermission("superviser/delete") && (
              <StyledButton
                onClick={() => setVisible(index)}
                color="danger"
                icon={DeleteFilled}
                tooltip={t("delete")}
              />
            )}
          </Popconfirm>
        </div>
      ),
    },
  ];

  const routes = [
    {
      name: "supervisor",
      link: false,
      route: "/supervisor",
    },
  ];

  const configs = {
    password: {
      rules: [
        {
          required: true,
          message: t("please.input.password"),
        },
      ],
    },
  };

  return (
    <div className="branch-user">
      <Modal
        title={t("please.confirm")}
        visible={modalVisible}
        onOk={modalType === "name" ? submitModal : editModal}
        onCancel={modalHideClick}
        okText={t("submit")}
        cancelText={t("cancel")}
      >
        <Form>
          <Form.Item
            name="password"
            label={t("password")}
            {...configs.password}
          >
            <Input.Password
              onChange={(e) => setInputModal(e.target.value)}
              placeholder={t("enter.password")}
            />
          </Form.Item>
        </Form>
      </Modal>
      <BreadCrumbTemplete routes={routes} />

      <div className="content">
        <Card
          title={
            <Search
              allowClear
              style={{ width: 350, paddingLeft: "4px" }}
              placeholder={`${t("search")}...`}
              onSearch={onSearchChange}
            />
          }
          extra={
            usePermission("superviser/create") ? <ExtraButton /> : <div></div>
          }
        >
          <Table
            bordered
            columns={columns}
            loading={tableLoading}
            dataSource={items.couriers}
            pagination={pagination}
            rowKey={(record) => record.id}
            onChange={onSortChange}
          />
        </Card>
      </div>
    </div>
  );
}

const arrng = {
  descend: "desc",
  ascend: "asc",
};
