import './style.css'
import React, { useEffect, useState } from 'react'
import StyledTag from '@/components/StyledTag/StyledTag'
import axios_init from '@/utils/axios_init'
import StyledButton from '@/components/StyledButton/StyledButton'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { useHistory } from 'react-router-dom'
import { EditFilled, DeleteFilled, PlusOutlined, LockOutlined } from '@ant-design/icons'
import { Card, Button, Table, Popconfirm, Input, message } from 'antd'
import PasswordModal from './PasswordModal'
import usePermission from '@/utils/usePermission'
const { Search } = Input

export default function BranchUser() {
  const { t } = useTranslation()
  const history = useHistory()

  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [tableLoading, setTableLoading] = useState(false)
  const [items, setItems] = useState([])
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })
  const [sort, setSort] = useState({ order: '', arrangement: '' })
  const [searchText, setSearchText] = useState('')
  //modal
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalData, setModalData] = useState({});


  useEffect(() => {
    getItems()
  }, [reqQuery, sort, searchText])

  const getItems = () => {
    setTableLoading(true)
    axios_init
      .get('/branch-user', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        arrangement: arrng[sort.arrangement],
        order: sort.order,
        search: searchText,
      })
      .then(({ data }) => {
        setItems(data)
      })
      .catch((err) => console.log(err))
      .finally(() => setTableLoading(false))
  }

  const handleDelete = (val) => {
    // console.log(val)
    setLoading(true)
    axios_init
      .remove(`/branch-user/${val.id}`)
      .then((res) => {
        getItems()
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const handleEdit = (id) => {
    // const selectedUserId = items.branches.
    history.push({
      pathname: `/branch-user/edit/${id}`,
    })
    console.log(id)
  }

  const changePassword = (id) => {
    console.log(id)
    setIsModalVisible(true)
    setModalData(id)
  }
  const ExtraButton = function () {
    return (
      <Button
        type='primary'
        icon={<PlusOutlined />}
        onClick={() => {
          history.push('/branch-user/create')
          // console.log('salom')
        }}
      >
        {t('add')}
      </Button>
    )
  }

  const onSortChange = (pagination, filters, sorter) => {
    console.log(sorter)
    setSort({ order: sorter.columnKey, arrangement: sorter.order })
  }

  const onSearchChange = (text) => {
    setSearchText(text)
    setReqQuery((old) => ({ ...old, offset: 0 }))
    // getItems(undefined, 0, undefined, e.target.value)
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    // showSizeChanger: true,
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    // pageSizeOptions: [10, 20, 50, 100],
    // defaultPageSize: 2,
    // pageSizeOptions: [2, 4, 8, 16],
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const columns = [
    {
      title: t('first.name'),
      dataIndex: 'name',
      key: 'name',
      sorter: {},
      defaultSortOrder: sort.order === 'name' ? sort.arrangement : undefined,
    },
    {
      title: t('phone.number'),
      dataIndex: 'phone',
      key: 'phone',
      sorter: {},
      defaultSortOrder: sort.order === 'phone' ? sort.arrangement : undefined,
    },
    {
      title: t('address'),
      dataIndex: 'address',
      key: 'address',
      sorter: {},
      defaultSortOrder: sort.order === 'address' ? sort.arrangement : undefined,
    },
    {
      title: t('status'),
      key: 'active',
      dataIndex: 'active',
      sorter: {},
      defaultSortOrder: sort.order === 'active' ? sort.arrangement : undefined,
      render: (active) => (
        <StyledTag color={active ? 'success' : 'danger'}>{t(active ? 'active' : 'inactive')}</StyledTag>
      ),
    },
    {
      title: t('actions'),
      key: 'actions',
      align: 'center',
      render: (text, record, index) => (
        <div>
          {
            usePermission('user/update') &&
            <StyledButton
              color='link'
              icon={EditFilled}
              onClick={() => handleEdit(record.id)}
              tooltip={t('edit.order')}
            />
          }          
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => handleDelete(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
            {
              usePermission('user/delete') &&
              <StyledButton onClick={() => setVisible(index)} color='danger' icon={DeleteFilled} tooltip={t('delete')} />
            }
          </Popconfirm>
          {usePermission('user/update') &&           
          <StyledButton
          color='link'
          icon={LockOutlined}
          onClick={() => changePassword(record.id)}
          tooltip={t('edit.password')}
          />
          }
        </div>
      ),
    },
  ]

  const routes = [
    {
      name: 'branch.user',
      link: false,
      route: '/branch-user',
    },
  ]

  return (
    <div className='branch-user'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
    
        <Card
          title={
            <Search allowClear style={{ width: 350 }} placeholder={`${t('search')}...`} onSearch={onSearchChange} />
          }
          extra={usePermission('user/create') ? <ExtraButton /> : <div></div>}
        >
          <Table
            bordered
            columns={columns}
            loading={tableLoading}
            dataSource={items.branches}
            pagination={pagination}
            rowKey={(record) => record.id}
            onChange={onSortChange}
          />
        </Card>
      </div>
      <PasswordModal 
       isVisible={isModalVisible}
       setIsVisible={setIsModalVisible}
       data={modalData}
      />
    </div>
  )
}

const arrng = {
  descend: 'desc',
  ascend: 'asc',
}
