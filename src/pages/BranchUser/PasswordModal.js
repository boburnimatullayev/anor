import React, { useEffect, useState } from 'react';
import { validateLogin, validatePassword } from '@/utils/validatePhone';
import { Modal, Form, Col, Input, Button, Row, notification } from 'antd';
import { useTranslation } from 'react-i18next';
import axios_init from '@/utils/axios_init';

export default function PasswordModal({
  isVisible = false,
  setIsVisible,
  data,
}) {
  const { t } = useTranslation();
  const { Password } = Input;
  const [loading, setLoading] = useState(false);
  const [userData, setBranchUserData] = useState({});

  const handleCancel = () => {
    setIsVisible(false);
  };

  const getBranchUser = () => {
    axios_init
      .get(`/branch-user/${data}`)
      .then((res) => {
        setBranchUserData(res.data);
        console.log(res.data, 'BRANCH');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onFinish = (values) => {
    axios_init
      .put(`/branch-user/${data}`, {
        ...userData,
        password: values.password,
        login: values.login,
      })
      .then((res) => {
        if (res.code == 200) {
          notification.success({ message: 'Успешно' });
        }
        if (res.code != 200) {
          notification.warning({ message: 'Ошибка' });
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        handleCancel();
      });
  };

  useEffect(() => {
    if (Object.keys(data).length > 0) {
      getBranchUser();
    }
  }, [isVisible]);

  const configs = {
    login: {
      rules: [
        {
          required: true,
          message: t('please.input.login'),
        },
        {
          validator: (_, value) => validateLogin(value),
        },
      ],
    },
    password: {
      rules: [
        {
          required: true,
          message: t('please.input.password'),
        },
        {
          validator: (_, value) => validatePassword(value),
        },
      ],
    },
  };

  return (
    <Modal
      title={<span style={{ fontSize: '21px' }}>{t('edit.password')}</span>}
      visible={isVisible}
      footer={null}
      onCancel={handleCancel}
    >
      <Form onFinish={onFinish}>
        <Row>
          <Col
            span='24'
            style={{
              padding: '10px',
            }}
          >
            <Form.Item name='login' label={t('login')} {...configs.login}>
              <Input placeholder={t('enter.login')} />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col
            span='24'
            style={{
              padding: '10px',
            }}
          >
            <Form.Item
              name='password'
              label={t('password')}
              {...configs.password}
            >
              <Password placeholder={t('enter.password')} />
            </Form.Item>
          </Col>
        </Row>
        <Row justify='end'>
          <Button type='primary' htmlType='submit' loading={loading}>
            {t('save')}
          </Button>
        </Row>
      </Form>
    </Modal>
  );
}
