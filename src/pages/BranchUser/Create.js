import "./style.css";
import React, { useState, useEffect, useRef } from "react";
import basic from "@/constants/basic";
// import moment from 'moment'
import ImgCrop from "antd-img-crop";
import axios_init from "@/utils/axios_init";
import { useHistory } from "react-router-dom";
import {
  validatePhone,
  validateLogin,
  validatePassword,
} from "@/utils/validatePhone";
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete";
import { defaultMapState } from "@/constants/basic";
import { YMaps, Map, Placemark } from "react-yandex-maps";
import { useParams } from "react-router";
import {
  PlusSquareOutlined,
  LoadingOutlined,
  EyeOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Upload,
  Button,
  Select,
  Switch,
  message,
} from "antd";

const { Option } = Select;
const { Password } = Input;

export default function CreateBranch() {
  const { t } = useTranslation();
  const history = useHistory();
  const mapRef = useRef(null);
  const params = useParams();
  const [selectedRole, setSelectedRole] = useState("");

  const [imgUrl, setImgUrl] = useState("");
  const [loading, setLoading] = useState(false);
  // const [tableLoading, setTableLoading] = useState(false)
  const [isActive, setIsActive] = useState(true);
  const [uploading, setUploading] = useState(false);
  const [branchList, setBranchList] = useState([]);
  const [openFileDialog, setOpenFileDialog] = useState(true);
  const [placemarkGeometry, setPlacemarkGeometry] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [roleData, setRoleData] = useState([]);
  const [initialValues, setInitialValues] = useState({
    name: "",
    address: "",
    phone: "",
    // login: '',
    // password: '',
    branch_id: "",
    active: isActive,
    role_id: "",
  });

  useEffect(() => {
    getBranches();
    getSingleBranchUser();
    getList();
  }, []);

  const getList = () => {
    setLoading(true);
    axios_init
      .get("/auth/role")
      .then((res) => setRoleData(res.data?.roles?.reverse()))
      .finally(() => setLoading(false));
  };

  const getSingleBranchUser = () => {
    if (params.id) {
      setLoading(true);

      axios_init
        .get(`/branch-user/${params.id}`)
        .then(({ data }) => {
          setInitialValues({
            name: data.name,
            address: data.address,
            phone: data.phone.slice(4),
            branch_id: data.branch_id,
            active: data.active === 1 ? setIsActive(true) : setIsActive(false),
            role_id: data?.role_id,
          });
          console.log(data);
        })
        .catch((err) => console.log(err))
        .finally(() => {
          setLoading(false);
          setIsFetching(false);
        });
    } else {
      setIsFetching(false);
    }
  };

  const onFinish = (values) => {
    console.log(values);
    setLoading(true);

    let data = {
      ...values,
      phone: "+998" + values.phone,
      active: +isActive,
      image: imgUrl,
      location: {
        lat: placemarkGeometry[0],
        long: placemarkGeometry[1],
      },
    };

    if (params.id) {
      // update yoziladi
      axios_init
        .put(`/branch-user/${params.id}`, {
          ...data,
          password: "",
          login: "",
        })
        .then((res) => {
          message.success(t("updated.successfully"));
          history.push("/branch-user");
        })
        .catch((err) => {
          console.log(err);
          message.error(t("updating.failed"));
        })
        .finally(() => setLoading(false));

        
        axios_init.put(`auth/user/active`,{
          user_id:params.id,
          active:data?.active
        }).then(res =>{
          console.log('resdata',res);
        })

    } else {
      axios_init
        .post("/branch-user", data)
        .then((res) => {
          message.success(t("saved.successfully"));
          history.push("/branch-user");
        })
        .catch((err) => {
          console.log(err);
          message.error(t("saving.failed"));
        })
        .finally(() => setLoading(false));
    }
  };

  const getBranches = () => {
    axios_init
      .get("/branch")
      .then(({ data }) => setBranchList(data.branches))
      .catch((err) => console.log(err));
  };

  const onUploadChange = (info) => {
    // setFileList(newFileList);
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(t("image.uploaded.successfully"));
      setImgUrl(info.file.response.url);
      setUploading(false);
    } else if (info.file.status === "error") {
      message.error(t("image.uploading.failed"));
      setUploading(false);
    }
  };

  const setMapRef = (ref) => {
    if (ref) {
      mapRef.current = ref;

      ref.events.add("click", (e) => {
        setPlacemarkGeometry(e.get("coords"));
        // console.log(e.get('coords'))
      });
    }
  };

  const token = localStorage.getItem("token")
  const props = {
    headers: {
      'platform-id': '7d4a4c38-dd84-4902-b744-0488b80a4c01',
         Authorization: "Bearer " + token,
    }  

  }

  const routes = [
    {
      name: "branch.user",
      route: "/branch-user",
      link: true,
    },

    {
      name: `${params.id ? "edit" : "create"}`,
      route: "/branch-user/create",
      link: false,
    },
  ];

  const configs = {
    name: {
      rules: [
        {
          required: true,
          message: t("please.input.name"),
        },
      ],
    },
    address: {
      rules: [
        {
          required: false,
          message: t("please.input.address"),
        },
      ],
    },
    phone: {
      rules: [
        {
          required: false,
          message: t("please.input.phone"),
        },
        // {
        //   validator: (_, value) => validatePhone(value),
        // },
      ],
    },
    login: {
      rules: [
        {
          required: true,
          message: t("please.input.login"),
        },
        {
          validator: (_, value) => validateLogin(value),
        },
      ],
    },
    password: {
      rules: [
        {
          required: true,
          message: t("please.input.password"),
        },
        {
          validator: (_, value) => validatePassword(value),
        },
      ],
    },
    branches: {
      rules: [
        {
          required: true,
          message: t("please.select.branch"),
        },
      ],
    },
    role: {
      rules: [
        {
          required: true,
          message: t("please.select.role"),
        },
      ],
    },
  };

  return (
    <div className="branch-user">
      <BreadCrumbTemplete routes={routes} />

      <div className="create-edit">
        {!isFetching ? (
          <Form
            layout="vertical"
            onFinish={onFinish}
            initialValues={initialValues}
          >
            <Card title={t("branch.user")}>
              <Row style={{ margin: "0 -10px" }}>
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item
                    name="name"
                    label={t("first.name")}
                    {...configs.name}
                  >
                    <Input placeholder={t("enter.name")} />
                  </Form.Item>
                </Col>
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item
                    name="address"
                    label={t("address")}
                    {...configs.address}
                  >
                    <Input placeholder={t("enter.address")} />
                  </Form.Item>
                </Col>
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item
                    name="phone"
                    label={t("phone.number")}
                    {...configs.phone}
                  >
                    <Input
                      placeholder={t("enter.phone.number")}
                      prefix="(+998)"
                    />
                  </Form.Item>
                </Col>
                {!params.id && (
                  <>
                    <Col span="8" style={{ padding: "0 10px" }}>
                      <Form.Item
                        name="login"
                        label={t("login")}
                        {...configs.login}
                      >
                        <Input placeholder={t("enter.login")} />
                      </Form.Item>
                    </Col>
                    <Col span="8" style={{ padding: "0 10px" }}>
                      <Form.Item
                        name="password"
                        label={t("password")}
                        {...configs.password}
                      >
                        <Password placeholder={t("enter.password")} />
                      </Form.Item>
                    </Col>
                  </>
                )}
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item
                    name="branch_id"
                    label={t("branches")}
                    {...configs.branches}
                  >
                    <Select
                      loading={!branchList.length}
                      placeholder={t("select.branch")}
                      // onChange={onSelectChange}
                    >
                      {branchList.length ? (
                        branchList.map(({ name, id }) => (
                          <Option key={id} value={id}>
                            {name}
                          </Option>
                        ))
                      ) : (
                        <></>
                      )}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item name="role_id" label={"Role"} {...configs.role}>
                    <Select
                      loading={!roleData.length}
                      value={initialValues.role_id}
                      // placeholder={t("select.branch")}
                      onChange={(value) =>
                        setInitialValues((prev) => ({ ...prev, role_id: value }))
                      }
                    >
                      {roleData.length ? (
                        roleData.map(({ name, id }) => (
                          <Option key={id} value={id}>
                            {name}
                          </Option>
                        ))
                      ) : (
                        <></>
                      )}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span="8" style={{ padding: "0 10px" }}>
                  <Form.Item
                    name="active"
                    label={t(isActive ? "active" : "inactive")}
                    {...configs.active}
                  >
                    <Switch
                      checked={isActive}
                      defaultChecked
                      onChange={(val) => {
                        setIsActive(val);
                        console.log(val);
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row style={{ margin: "0 -10px" }}>
                <Col span="12" style={{ padding: "0 10px" }}>
                  <div
                    style={{
                      backgroundColor: "grey",
                      width: "100%",
                      height: "100%",
                    }}
                  >
                    <YMaps query={{ lang: "ru_RU", load: "package.full" }}>
                      <Map
                        instanceRef={(ref) => setMapRef(ref)}
                        width="100%"
                        height="100%"
                        defaultState={defaultMapState}
                      >
                        <Placemark geometry={placemarkGeometry} />
                      </Map>
                    </YMaps>
                  </div>
                </Col>
                <Col
                  span="12"
                  className="img-upload"
                  style={{ padding: "0 10px" }}
                >
                  <ImgCrop
                    rotate
                    aspect={1}
                    quality={1}
                    modalOk={t("ok")}
                    modalCancel={t("cancel")}
                    modalTitle={t("edit.image")}
                  >
                    <Upload
                      action={`${basic.BASE_URL}/upload/file`}
                      {...props}
                      listType="picture-card"
                      onChange={onUploadChange}
                      showUploadList={false}
                      openFileDialogOnClick={openFileDialog}
                      beforeUpload={(file) => {
                        if (
                          file.type === "image/png" ||
                          file.type === "image/jpeg" ||
                          file.type === "image/jpg" ||
                          file.type === "image/webp"
                        ) {
                          setUploading(true);
                          return true;
                        }
                        message.error(t("invalid.image.format"));
                        return false;
                      }}
                    >
                      {uploading ? (
                        <LoadingOutlined style={{ fontSize: 30 }} />
                      ) : imgUrl.length ? (
                        <div
                          className="img-content"
                          onMouseEnter={() => setOpenFileDialog(false)}
                          onMouseLeave={() => setOpenFileDialog(true)}
                        >
                          <img alt="uploded img" src={imgUrl} />
                          <div className="img-buttons">
                            <EyeOutlined
                              style={{ color: "#fff", fontSize: 20 }}
                              onClick={(e) => {
                                window.open(imgUrl, "_blank");
                              }}
                            />
                            <DeleteOutlined
                              style={{
                                color: "#fff",
                                fontSize: 20,
                                marginLeft: 10,
                              }}
                              onClick={() => {
                                setImgUrl("");
                                setOpenFileDialog(true);
                              }}
                            />
                          </div>
                        </div>
                      ) : (
                        <div style={{ color: "rgba(0, 0, 0, 0.25)" }}>
                          <PlusSquareOutlined style={{ fontSize: "22px" }} />
                          <p style={{ fontSize: "15px" }}>
                            {t("image.upload")}
                          </p>
                        </div>
                      )}
                    </Upload>
                  </ImgCrop>
                </Col>
              </Row>
            </Card>
            <Row
              justify="end"
              style={{ paddingTop: "24px", backgroundColor: "#F9F9F9" }}
            >
              <Button type="primary" htmlType="submit" loading={loading}>
                {t("save")}
              </Button>
            </Row>
          </Form>
        ) : (
          "loading"
        )}
      </div>
    </div>
  );
}
