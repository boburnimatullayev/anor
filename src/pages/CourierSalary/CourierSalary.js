import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete';
import React, { useState, useEffect } from 'react';
import { Tabs, Card, Table, Button, Tag } from 'antd';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import axios_init from '@/utils/axios_init';
import moment from 'moment';
import DownloadOutlined from '@ant-design/icons/lib/icons/DownloadOutlined';
import CustomDatePicker from '../../components/DatePicker';
import './style.css';
import { da } from 'react-date-range/dist/locale';
import PopoverTitle from '../../components/MultiSelect/PopoverTitle';
export default function CourierSalary() {
  const { t } = useTranslation();
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])
  const [tableLoading, setTableLoading] = useState(false);
  const [dataCount, setDataCount] = useState(0);
  const [btnLoading, setBtnLoading] = useState(false);
  const [data, setTableData] = useState([]);
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 });
  const [paginations, setPagination] = useState({ current: 1, pageSize: 10 });

  //couriers
  const [popoverCouriersFilter, setPopoverCouriersFilter] = useState(false);
  const [choosenCourierFilter, setChoosenCourierFilter] = useState('');
  const [courierList, setCourierList] = useState([]);
  const [searchedWordCourierFilter, setSearchedWordCourierFilter] =
    useState('');
  const [getValueCourierFilter, setGetValueCourierFilter] = useState(undefined);

  //GET TABLE DATA
  const getSalaryList = () => {
    setTableLoading(true);
    axios_init
      .get('/salary', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: choosenCourierFilter.length
          ? choosenCourierFilter.join()
          : null,
      })
      .then((res) => {
        axios_init
          .get('/branch')
          .then(({ data }) => {
            const allBranches = data.branches.map((branches) => {
              return { name: branches.name, value: branches.id };
            });
            setTableData(() =>
              res.data.salaries?.map((el) => ({
                ...el,
                branch_name: getBranchName(allBranches, el.branch_id),
              }))
            );
            setDataCount(res.data.Count);
            setTableLoading(false);
          })
          .catch((err) => {});
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // GET BRANCH NAME BY ID
  const getBranchName = (arr, value) => {
    return arr.filter((el) => el.value === value)[0]?.name;
  };


  //GET COURIERS LIST
  const getCouriers = (searchValue = '') => {
    axios_init
      .get('/courier', { search: searchValue })
      .then(({ data }) => {
        let couriers = data.couriers?.map((item, i) => ({
          value: item.id,
          text: item.name,
          key: i,
        }));
        setCourierList(couriers);
      })
      .finally(() => setIsFetching(false));
  };

  const downloadApi = () => {
    setBtnLoading(true);
    axios_init
      .get('/salary-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: choosenCourierFilter.length
          ? choosenCourierFilter.join()
          : null,
      })
      .then((res) => {
        window.location.href = res.data.url;
        console.log();
      })
      .finally(() => setBtnLoading(false));
  };

  useEffect(() => {
    getSalaryList();
  }, [dateRange, reqQuery, choosenCourierFilter]);

  
  useEffect(() => {
    getCouriers(searchedWordCourierFilter);
  }, [searchedWordCourierFilter]);


  const pagination = {
    total: dataCount,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const routes = [
    {
      name: 'couriers_salary',
      link: false,
      route: '/couriers-salary',
    },
  ];

  const columns = [
    {
      title: t('first.name'),
      key: 'name',
      dataIndex: 'name',
      width: 300,
    },
    {
      title: t('branch'),
      dataIndex: 'branch_name',
      key: 'branch_name',
      width: 200,
    },
    {
      title: t('summ'),
      dataIndex: 'amount',
      key: 'amount',
      width: 200,
    },
    {
      title: t('total.count'),
      dataIndex: 'orders',
      key: 'orders',
      width: 200,
    },
  ];

  return (
    <div className='salaries'>
    
      <BreadCrumbTemplete routes={routes} />
      <div className='content'>
        <Card>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: '20px',
              marginBottom: '20px',
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                minWidth: '470px',
              }}
            >
              <CustomDatePicker
                finalDate={dateRange}
                setFinalDate={setDateRange}
              />
              <div
                style={{
                  minWidth: 200,
                  border: '1px solid #bbb',
                  fontSize: 14,
                  padding: '4px 8px',
                  borderRadius: 5,
                  marginRight: 10,
                }}
              >
                <PopoverTitle
                  title='couriers'
                  popoverVisible={popoverCouriersFilter}
                  setPopoverVisible={setPopoverCouriersFilter}
                  setChosenState={setChoosenCourierFilter}
                  dataList={courierList}
                  getValue={getValueCourierFilter}
                  setGetValue={setGetValueCourierFilter}
                  setSearchedWord={setSearchedWordCourierFilter}
                  topfilter='topfilter'
                />
              </div>
            </div>
            <Button
              type='primary'
              loading={btnLoading}
              icon={<DownloadOutlined />}
              onClick={downloadApi}
            ></Button>
          </div>
          <Table
            bordered
            columns={columns}
            loading={tableLoading}
            dataSource={data}
            pagination={pagination}
            rowKey={(record) => record.id}
          />
        </Card>
      </div>
    </div>
  );
}
