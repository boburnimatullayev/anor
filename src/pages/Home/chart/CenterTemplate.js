import React from "react"

export default function TooltipTemplate(pieChart) {
  return (
    <svg>
      <text textAnchor="middle" x="300" y="250" style={{ fontSize: 18, fill: "#494949" }}>
        <tspan x="100">country</tspan>
        <tspan x="100" dy="20px" style={{ fontWeight: 600 }}>
          620,921
        </tspan>
      </text>
    </svg>
  )
}
