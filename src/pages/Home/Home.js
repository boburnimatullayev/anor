import "./style.css";
import React, { useState, useEffect } from "react";
import moment from "moment";
import axios_init from "@/utils/axios_init";
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete";
import { Card, Col, Row, message } from "antd";
import EmptyTable from "../../components/EmptyTable/EmptyTable";
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Legend,
  Tooltip,
} from "devextreme-react/chart";
import PieChart, {
  Series as PieSeries,
  Tooltip as PieTooltip,
  Label as PieLabel,
} from "devextreme-react/pie-chart";
import CustomDatePicker from "../../components/DatePicker";
import { useSelector } from "react-redux";

export default function Home() {
  const { t } = useTranslation();
  const permissions = useSelector((state) => state.auth.permissions);
  const permissionOrder = permissions?.find((item) => item?.name === "is_my_branch");
 
  const [couriers, setCouriers] = useState([]);
  const [donutChartData, setDonutChartData] = useState([]);
  const [topBranches, setTopBranches] = useState([]);
  const [barChartData, setbarChartData] = useState([]);
  const [showLegend, setShowLegend] = useState(false);
  const [productsGetting, setProductsGetting] = useState(false);
  const [totalProducts, setTotalProducts] = useState(0);
  const [dateRange, setDateRange] = useState([
    moment().startOf("day"),
    moment().endOf("day"),
  ]);
  const userData = localStorage.getItem("user-data");
  useEffect(() => {
    getBarChartData();
    getCourierReports();
    getProductReports();
    getTopBranches();
  }, [dateRange]);

  const routes = [
    {
      name: "dashboard",
      route: "/",
      link: false,
    },
  ];

  

  const getBarChartData = () => {
    console.log('permisonsOrder');
    const params = {
      from_time: dateRange[0],
      to_time: dateRange[1],
    };
    if (permissionOrder?.is_my_branch) {
      params.branch_id=localStorage.getItem("branch_id").length ? JSON.parse(localStorage.getItem("branch_id")) :""
    }
   
    if(JSON.parse(userData)?.type === 'SUPER-ADMIN' || permissionOrder?.id === "dd41320a-4155-4333-a3ea-f4608c6d6877"){
      axios_init
      .getC("daily-order-count", params)
      .then((res) => {
        setbarChartData(res.data);  
      })
      .catch((err) => message.error(t(err.response.data.error)))
      .finally(() => setShowLegend(true));
  }else{
    axios_init
    .get("daily-order-count", params)
    .then((res) => {
      setbarChartData(res.data);
    })
    .catch((err) => message.error(t(err.response.data.error)))
    .finally(() => setShowLegend(true));
  }
    }

  const getCourierReports = () => {
    axios_init
      .get(`/top-couriers`, {
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then((res) =>
        setCouriers(
          res.data.couriers
            .sort((a, b) => b.orders - a.orders)
            .filter((item) => item.orders > 0)
        )
      )
      .catch((err) => console.log(err));
  };

  const getTopBranches = () => {
    axios_init
      .get("orders-count", {
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then((res) => {
        if (res.data) {
          setTopBranches(res.data?.sort((a, b) => b.count - a.count));
        }
      });
    // .catch((err) => message.error(t(err.response.data.error)))
  };

  const getProductReports = () => {
    setProductsGetting(true);
    const params = {
      from_time: dateRange[0],
      to_time: dateRange[1],
      status_id: "finished",
    };
    if (permissionOrder?.is_my_branch) {
      params.branch_id=localStorage.getItem("branch_id").length ? JSON.parse( localStorage.getItem("branch_id")) :""
    }
    axios_init
      .get("report/products", params)
      .then((res) => {
        if (res.data) {
          setTotalProducts(
            res.data?.reduce((acc, cur) => acc + cur.total_count, 0) || 0
          );
          setDonutChartData(
            res.data.map(({ product_name, total_count }) => ({
              type: product_name,
              value: total_count,
            }))
          );
        }
      })
      .catch((err) => message.error(t(err.response.data.error)))
      .finally(() => setProductsGetting(false));
  };
  // console.log("res", donutChartData);

  const customizeTooltip = (arg) => {
    return {
      text: `${arg.seriesName}: ${arg.valueText}`,
    };
  };

  const customizePieTooltip = (arg) => {
    return { text: `${arg.argumentText}: ${arg.valueText}` };
  };

  const pieLegendClickHandler = (e) => {
    const arg = e.target;
    const item = e.component.getAllSeries()[0].getPointsByArg(arg)[0];

    item.isVisible() ? item.hide() : item.show();
    setTotalProducts(
      e.component
        .getAllSeries()[0]
        .getVisiblePoints()
        .reduce((s, p) => s + p.originalValue, 0)
    );
  };

  const grep = function (items, callback) {
    var filtered = [],
      len = items.length,
      i = 0;
    for (i; i < len; i++) {
      var item = items[i];
      var cond = callback(item);
      if (cond) {
        filtered.push(item);
      }
    }

    return filtered;
  };

  return (
    <div>
      <BreadCrumbTemplete routes={routes} />
      <div className="date-picker">
        <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
      </div>
      <div className="home">
        <Row>
          <Col span={24}>
            <div className="wrapper">
              <Card
                className="gutter-row"
                title={t("order.amount")}
                bodyStyle={{ paddingTop: 15 }}
                headStyle={{ borderBottom: "1px solid #e5e9eb" }}
              >
                <Chart
                  palette="Soft Pastel"
                  loadingIndicator={{ enabled: true }}
                  onDone={(e) => {
                    var series = e.component.getAllSeries();
                    for (var i = 0; i < series.length; i++) {
                      var points = series[i].getAllPoints();
                      var zeroPoints = grep(points, function (point) {
                        return point.originalValue == 0;
                      });
                      if (zeroPoints.length == points.length) {
                        series[i].hide();
                        var seriesInConfig = e.component.option("series");
                        for (var j = 0; j < seriesInConfig.length; j++) {
                          if (seriesInConfig[j].name == series[i].name) {
                            e.component.option(
                              "series[" + j.toString() + "].showInLegend",
                              false
                            );
                          }
                        }
                      }
                    }
                  }}
                  onLegendClick={(e) =>
                    e.target.isVisible() ? e.target.hide() : e.target.show()
                  }
                  id="chart"
                  dataSource={barChartData}
                  barGroupWidth={72}
                >
                  <CommonSeriesSettings
                    barPadding={0.1}
                    argumentField="date"
                    type="stackedBar"
                    argumentText={(val) => val + "**"}
                    // label={(val) => val + '**'}
                  />
                  <Series valueField="arrived" name={t("arrived")} />
                  <Series valueField="assigned" name={t("assigned")} />
                  <Series
                    valueField="cancelled_in_progress"
                    name={t("cancelled_in_progress")}
                  />
                  <Series
                    valueField="cancelled_after_call"
                    name={t("cancelled_after_call")}
                  />
                  <Series valueField="new" name={t("new")} />
                  <Series valueField="in_progress" name={t("in_progres")} />
                  <Series valueField="pending" name={t("pending")} />
                  <Series valueField="on_the_way" name={t("on_the_way")} />
                  <Series
                    valueField="cancelled_after_call2"
                    name={t("cancelled_after_call2")}
                  />
                  <Series
                    valueField="cancelled_not_identified"
                    name={t("cancelled_not_identified")}
                  />
                  <Series valueField="delivered" name={t("delivered")} />
                  <Series valueField="finished" name={t("finished")} />
                  <Series valueField="cancelled" name={t("cancelled")} />
                  <Series valueField="not_assigned" name={t("not_assigned")} />
                  <Series valueField="completed" name={t("completed")} />
                  <Series
                    valueField="cancelled_no_return"
                    name={t("cancelled_no_return")}
                  />
                  <Legend
                    customizeText={(name) =>
                      name.seriesName.length > 13
                        ? name.seriesName.slice(0, 13) + "..."
                        : name.seriesName
                    }
                    verticalAlignment="bottom"
                    horizontalAlignment="center"
                    itemTextPosition="top"
                    visible={showLegend}
                  />
                  <Tooltip
                    enabled={true}
                    location="edge"
                    customizeTooltip={customizeTooltip}
                  />
                </Chart>
              </Card>
            </div>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col className="gutter-row" span={12} style={{ marginTop: 10 }}>
            <div className="wrapper">
              <Card
                title={t("products")}
                bodyStyle={{ paddingTop: 24 }}
                headStyle={{ borderBottom: "1px solid #e5e9eb" }}
              >
                <div className="pie">
                  {!productsGetting && (
                    <div className="total">
                      <p className="sup">Общее</p>
                      <p>{totalProducts}</p>
                    </div>
                  )}
                  <PieChart
                    loadingIndicator={{ enabled: true }}
                    id="pie-chart"
                    type="doughnut"
                    sizeGroup="piesGroup"
                    onLegendClick={pieLegendClickHandler}
                    dataSource={donutChartData}
                    palette="Dark Violet"
                    resolveLabelOverlapping="shift"
                    innerRadius={0.65}
                  >
                    <PieSeries argumentField="type" valueField="value" />
                    <PieTooltip
                      enabled={true}
                      customizeTooltip={customizePieTooltip}
                    />
                  </PieChart>
                </div>
              </Card>
            </div>
          </Col>
          <Col className="gutter-row" span={12} style={{ marginTop: 10 }}>
            <div className="wrapper">
              <Card
                title={t("top.curiers")}
                bodyStyle={{ padding: 0 }}
                headStyle={{ borderBottom: "1px solid #e5e9eb" }}
              >
                <div className="branches2">
                  {!!couriers.length ? (
                    couriers.map(({ id, name, orders }, i) => (
                      <div key={id} className="branches_wrapper">
                        <div className="branches_left">
                          <div className="branches_count">{i + 1}</div>
                          <div>
                            <div className="branches_count_name">{name}</div>
                          </div>
                        </div>
                        <div className="branches_right">
                          <div className="branches_right_total">{orders}</div>
                        </div>
                      </div>
                    ))
                  ) : (
                    <EmptyTable />
                  )}
                </div>
              </Card>
            </div>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col className="gutter-row" span={12} style={{ marginTop: 10 }}>
            <div className="wrapper">
              <Card
                title={t("top.branches")}
                headStyle={{ borderBottom: "1px solid #e5e9eb" }}
                bodyStyle={{ padding: 0 }}
              >
                <div className="curier">
                  {!!topBranches.length ? (
                    <>
                      <div className="curier_header">
                        <div className="curier_header_num">№</div>
                        <div className="curier_header_fio">{t("FIO")}</div>
                        <div className="curier_header_count">
                          {t("delivery.count")}
                        </div>
                      </div>
                      {topBranches.map((branch, index) => (
                        <div
                          className="curier_row"
                          key={branch.branch_id}
                          style={{
                            backgroundColor:
                              index % 2 !== 0 ? "#fff" : "#F4F6FA",
                            borderBottom:
                              topBranches.length - 1 !== index
                                ? "1px solid #e5e9eb"
                                : "",
                          }}
                        >
                          <div className="curier_row_num">{index + 1}</div>
                          <div className="curier_row_fio">
                            {branch.branch_name}
                          </div>
                          <div className="curier_row_count">{branch.count}</div>
                        </div>
                      ))}
                    </>
                  ) : (
                    <EmptyTable />
                  )}
                </div>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
