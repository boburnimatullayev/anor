import './style.css'
import React, { useEffect, useState } from 'react'
import axios_init from '@/utils/axios_init'
import StyledButton from '@/components/StyledButton/StyledButton'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../components/breadcrumb/BreadCrumbTemplete'
import StyledTag from '@/components/StyledTag/StyledTag'
import { useHistory } from 'react-router-dom'
import { EditFilled, DeleteFilled, PlusOutlined, EyeOutlined } from '@ant-design/icons'
import { Card, Button, Table, Popconfirm, Input, Select, message } from 'antd'
import OrderHistories from './OrderHistories'
import makeStrFirstLetterUpper from '../../utils/makeStrFirstLetterUpper'
import formatCarNumbers from '../../utils/formatCarNumbers'
import usePermissions from '../../utils/usePermission';

const { Search } = Input

export default function MobileRepresentatives() {
  const { t } = useTranslation()
  const history = useHistory()

  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [items, setItems] = useState([])
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  const [sort, setSort] = useState({ order: '', arrangement: '' })
  const [searchText, setSearchText] = useState('')
  const [courierId, setCourierId] = useState(null)
  const [branches, setBranches] = useState([])
  const [selectedBranch, setSelectedBranch] = useState()

  useEffect(() => {
    getBranches()
  }, [])

  useEffect(() => {
    getItems()
  }, [reqQuery, sort, searchText, selectedBranch])

  let userData = JSON.parse(window.localStorage.getItem('user-data'))

  const getItems = () => {
    setLoading(true)
    const query = {
      limit: reqQuery.limit,
      offset: reqQuery.offset,
      arrangement: arrng[sort.arrangement],
      order: sort.order,
      search: searchText,
      active: ''
    }
    if (userData.type === 'SUPER-ADMIN') {
      query.branch_id = selectedBranch
    }

    axios_init
      .get('/courier', query)
      .then(({ data }) => setItems(data))
      .catch((err) => console.log(err))
      .finally(() => setLoading(false))
  }

  const getBranches = () => {
    axios_init
      .get('/branch')
      .then((res) => {
        setBranches(res.data.branches.map((elm) => ({ label: elm.name, value: elm.id })))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('failed.to.fetch'))
      })
  }

  const handleDelete = (val) => {
    setLoading(true)
    axios_init
      .remove(`/courier/${val.id}`)
      .then((res) => {
        getItems()
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const handleEdit = (val) => {
    history.push({
      pathname: '/couriers/edit',
      state: val,
    })
  }

  const ExtraButton = function () {
      return (
        <Button
          type='primary'
          icon={<PlusOutlined />}
          onClick={() => {
            history.push('/couriers/create')
          }}
        >
          {t('add')}
        </Button>
      )
  }

  const onSortChange = (pagination, filters, sorter) => {
    setSort({ order: sorter.columnKey, arrangement: sorter.order })
  }

  const onSearchChange = (text) => {
    setSearchText(text)
    setReqQuery((old) => ({ ...old, offset: 0 }))
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const columns = [
    {
      title: t('fio'),
      dataIndex: 'name',
      key: 'name',
      sorter: {},
      defaultSortOrder: sort.order === 'name' ? sort.arrangement : undefined,
      render: (val) => makeStrFirstLetterUpper(val),
    },
    {
      title: t('phone.number'),
      dataIndex: 'phone',
      key: 'phone',
      sorter: {},
      defaultSortOrder: sort.order === 'phone' ? sort.arrangement : undefined,
    },
    {
      title: t('phone.model'),
      dataIndex: 'phone_model',
      key: 'phone_model',
      sorter: {},
      defaultSortOrder: sort.order === 'phone_model' ? sort.arrangement : undefined,
    },
    {
      title: t('car.model'),
      dataIndex: 'vehicle_model',
      key: 'vehicle_model',
      sorter: {},
      defaultSortOrder: sort.order === 'vehicle_model' ? sort.arrangement : undefined,
      render: (val) => val?.toLowerCase() !== 'нет' && makeStrFirstLetterUpper(val),
    },
    {
      title: t('car.number'),
      dataIndex: 'vehicle_number',
      key: 'vehicle_number',
      sorter: {},
      defaultSortOrder: sort.order === 'vehicle_number' ? sort.arrangement : undefined,
      render: (num) => num?.toLowerCase() !== 'нет' && formatCarNumbers(num),
    },
    {
      title: t('status'),
      key: 'active',
      dataIndex: 'active',
      sorter: {},
      defaultSortOrder: sort.order === 'active' ? sort.arrangement : undefined,
      render: (active) => (
        <StyledTag color={active === 1 ? 'success' : 'danger'}>{t(active === 1 ? 'active' : 'inactive')}</StyledTag>
      ),
    },
    {
      title: t('actions'),
      key: 'actions',
      align: 'center',
      width: 180,
      render: (text, record, index) => (
        <div>
          <StyledButton
            color='view'
            icon={EyeOutlined}
            onClick={() => setCourierId(record.id)}
            tooltip={t('view.orders.history')}
          />
          {usePermissions('couriers/update',true) &&
          <StyledButton color='link' icon={EditFilled} onClick={() => handleEdit(text)} tooltip={t('edit')} />
          }
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => handleDelete(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
          {usePermissions('couriers/delete') &&  
            <StyledButton onClick={() => setVisible(index)} color='danger' icon={DeleteFilled} tooltip={t('delete')} />
          }
            </Popconfirm>
        </div>
      ),
    },
  ]

  const cardTitle = (
    <div style={{ display: 'flex', gap: 10, paddingLeft: '4px' }}>
      <Search allowClear style={{ width: 250 }} placeholder={`${t('search')}...`} onSearch={onSearchChange} />
      {userData.type === 'SUPER-ADMIN' && (
        <Select
          allowClear
          style={{ width: 200 }}
          placeholder={t('branches')}
          options={branches}
          value={selectedBranch}
          onChange={setSelectedBranch}
        />
      )}
    </div>
  )

  const routes = [
    {
      name: 'mobile.representatives',
      link: false,
      route: '/couriers',
    },
  ]

  return (
    <div className='mobile-representatives'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <Card title={cardTitle} extra={usePermissions('couriers/create',true) ? <ExtraButton /> : <div></div>}>
          <Table
            bordered
            loading={loading}
            columns={columns}
            dataSource={items.couriers}
            pagination={pagination}
            rowKey={(record) => record.id}
            onChange={onSortChange}
            locale={{
              filterConfirm: t('ok'),
              filterReset: t('reset'),
              emptyText: t('no.data'),
            }}
          />
        </Card>
      </div>

      <OrderHistories courierId={courierId} setCourierId={setCourierId} />
    </div>
  )
}

const arrng = {
  descend: 'desc',
  ascend: 'asc',
}
