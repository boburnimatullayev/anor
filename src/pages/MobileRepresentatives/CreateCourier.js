import './style.css';
import React, { useState, useEffect, useRef } from 'react';
import basic from '@/constants/basic';
import ImgCrop from 'antd-img-crop';
import axios_init from '@/utils/axios_init';
import { useHistory } from 'react-router-dom';
import { validatePhone } from '@/utils/validatePhone';
import { useTranslation } from 'react-i18next';
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete';
import { defaultMapState } from '@/constants/basic';
import {
  PlusSquareOutlined,
  LoadingOutlined,
  EyeOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import { YMaps, Map, Polygon, Placemark } from 'react-yandex-maps';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  TimePicker,
  Upload,
  Button,
  Select,
  Table,
  message,
} from 'antd';

const { Option } = Select;

export default function CreateBranch() {
  const { t } = useTranslation();
  const history = useHistory();
  const mapRef = useRef(null);

  const [imgUrl, setImgUrl] = useState('');
  const [loading, setLoading] = useState(false);
  const [uploading, setUploading] = useState(false);
  const [mapList, setMapList] = useState([]);
  const [selectedZones, setSelectedZones] = useState('');
  const [selectedSupervisors, setSelectedSupervisors] = useState([]);
  const [supervisorList, setSupervisorList] = useState([]);

  const [branchList, setBranchList] = useState([]);
  const [shifts, setShifts] = useState([]);
  const [openFileDialog, setOpenFileDialog] = useState(true);
  const [placemarkGeometry, setPlacemarkGeometry] = useState([]);
  const [cells, setCells] = useState([[], [], [], [], [], [], []]);
  const userData = localStorage.getItem("user-data");


  useEffect(() => {
    getBranches();
    getSupervisors();
    getCourierShifts();
    getMaps();
  }, []);

  const onFinish = (values) => {
    setLoading(true);
    let data = {
      ...values,
      phone: '+998' + values.phone,
      image: imgUrl,
      supervisor_id: values?.supervisor_id?.length
        ? values?.supervisor_id.join()
        : null,
      location: {
        lat: placemarkGeometry[0],
        long: placemarkGeometry[1],
      },
      timetable: cells.map(([index], i) => ({
        day: (i + 1) % 7,
        from_time: shifts[index]?.from_time,
        to_time: shifts[index]?.to_time,
      })),
    };

    axios_init
      .post('/courier', data)
      .then((res, err) => {
        message.success(t('saved.successfully'));
        history.push('/couriers');
      })
      .catch((err) => {
        message.error(t(err.response.data.error));
      })
      .finally(() => setLoading(false));
  };

  const getMaps = () => {
    axios_init
      .get('/geozone')
      .then(({ data }) => setMapList(data.geozones))
      .catch((err) => console.log(err));
  };

  const getBranches = () => {
    axios_init
      .get('/branch')
      .then(({ data }) => setBranchList(data.branches))
      .catch((err) => console.log(err));
  };

  const getSupervisors = () => {
    console.log('userdata1',userData);
   if(JSON.parse(userData)?.type === 'SUPER-ADMIN'){
    axios_init  
    .getC('/supervisor')
    .then(({ data }) => setSupervisorList(data.couriers))
    .catch((err) => console.log(err));
   }
   else{
    axios_init
    .get('/supervisor')
    .then(({ data }) => setSupervisorList(data.couriers))
    .catch((err) => console.log(err));
   }
  };

  const getCourierShifts = () => {
    axios_init
      .get('/courier-shift', { arrangement: 'asc' })
      .then(({ data }) => {
        setShifts(data.shifts);
      })
      .catch((err) => console.log(err));
  };

  const onSelectChange = (id) => {
    let geozone_id = branchList.filter((val) => val.id === id)[0].geozone_id;
  };

  const onUploadChange = (info) => {
    if (info.file.status !== 'uploading') {
    }
    if (info.file.status === 'done') {
      message.success(t('image.uploaded.successfully'));
      setImgUrl(info.file.response.url);
      setUploading(false);
    } else if (info.file.status === 'error') {
      message.error(t('image.uploading.failed'));
      setUploading(false);
    }
  };

  const token = localStorage.getItem("token")
  const props = {
    headers: {
      'platform-id': '7d4a4c38-dd84-4902-b744-0488b80a4c01',
         Authorization: "Bearer " + token,
    }  

  }


  const getMapData = (id) => {
    // axios_init
    //   .get(`/geozones?geozone_ids=${id.length ? id.join() : null}`)
    //   .then(({ data }) => {
    //     if (!data.geozones.length) {
    //       setMultiZone(prev => ({ ...prev, geometries: [] }))
    //       return
    //     }
    //     const firstData = JSON.parse(data.geozones[0].shape.data)
    //     setMultiZone({
    //       geometries: data.geozones.map(geozone => JSON.parse(geozone.shape.data).geometry[0]),
    //       options: firstData.options,
    //       defaultState: firstData.defaultState
    //     })
    //   })
    //   .catch((err) => console.log(err))
  };

  const setMapRef = (ref) => {
    if (ref) {
      mapRef.current = ref;

      ref.events.add('click', (e) => {
        setPlacemarkGeometry(e.get('coords'));
      });
    }
  };

  const handleSelectCell = (i, index) => {
    if (cells[i].includes(index)) {
      setCells((old) => old.map((val, indx) => (indx === i ? [] : val)));
    } else {
      setCells((old) => old.map((val, indx) => (indx === i ? [index] : val)));
    }
  };

  const routes = [
    {
      name: 'mobile.representatives',
      route: '/couriers',
      link: true,
    },

    {
      name: 'create',
      route: '/couriers/create',
      link: false,
    },
  ];

  const columns = [
    {
      title: '',
      dataIndex: 'range',
      key: 'range',
      render: (val, data) => (
        <div className='time-range'>
          <span>{data.from_time.slice(11, 16)}</span>
          <div className='vertical-divider'></div>
          <span>{data.to_time.slice(11, 16)}</span>
        </div>
      ),
    },
    ...weaks.map((val, i) => ({
      title: t(val),
      align: 'center',
      key: i,
      render: (val, data, index) => (
        <div
          className='table-cell'
          style={
            cells[i === 6 ? 0 : i + 1]?.includes(index)
              ? { backgroundColor: 'rgba(255, 59, 48, 0.1)' }
              : { border: 'none' }
          }
          onClick={() => handleSelectCell(i === 6 ? 0 : i + 1, index)}
        />
      ),
    })),
  ];

  const configs = {
    name: {
      rules: [
        {
          required: true,
          message: t('please.input.name'),
        },
      ],
    },
    address: {
      rules: [
        {
          required: true,
          message: t('please.input.address'),
        },
      ],
    },
    phone: {
      rules: [
        {
          required: true,
          message: t('please.input.phone'),
        },
        {
          validator: (_, value) => validatePhone(value),
        },
      ],
    },
    geozone: {
      rules: [
        {
          required: true,
          message: t('please.select.geozone'),
        },
      ],
    },
    vehicle_model: {
      rules: [
        {
          required: true,
          message: t('please.input.car.model'),
        },
      ],
    },
    vehicle_number: {
      rules: [
        {
          required: true,
          message: t('please.input.car.number'),
        },
      ],
    },
    vehicle_brand: {
      rules: [
        {
          required: true,
          message: t('please.input.car.brand'),
        },
      ],
    },
    vehicle_type: {
      rules: [
        {
          required: true,
          message: t('please.input.car.type'),
        },
      ],
    },
    vehicle_colour: {
      rules: [
        {
          required: true,
          message: t('please.input.car.colour'),
        },
      ],
    },
    supervisor: {
      rules: [
        {
          required: true,
          message: t('please.select.supervisor'),
        },
      ],
    },
  };

  return (
    <div className='mobile-representatives'>
      <BreadCrumbTemplete routes={routes} />
      <div className='create-edit'>
        <Form layout='vertical' onFinish={onFinish}>
          <Card title={t('mobile.representatives')}>
            <Row style={{ margin: '0 -10px' }}>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item name='name' label={t('fio')} {...configs.name}>
                  <Input placeholder={t('enter.name')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='address'
                  label={t('address')}
                  {...configs.address}
                >
                  <Input placeholder={t('enter.address')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='phone'
                  label={t('phone.number')}
                  {...configs.phone}
                >
                  <Input
                    placeholder={t('enter.phone.number')}
                    prefix='(+998)'
                  />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item name='branch_id' label={t('branches')}>
                  <Select
                    loading={!branchList.length}
                    placeholder={t('select.branch')}
                    onChange={onSelectChange}
                  >
                    {branchList.length ? (
                      branchList.map(({ name, id }) => (
                        <Option key={id} value={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item name='district_id' label={t('geozones')}>
                  <Select
                    loading={!mapList.length}
                    placeholder={t('select.geofence')}
                    allowClear
                    onChange={(val) => {
                      setSelectedZones(val);
                      getMapData(val);
                    }}
                  >
                    {mapList.length ? (
                      mapList.map(({ name, id }) => (
                        <Option key={id} value={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_model'
                  label={t('car.model')}
                  {...configs.vehicle_model}
                >
                  <Input placeholder={t('enter.car.model')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_number'
                  label={t('car.number')}
                  {...configs.vehicle_number}
                >
                  <Input placeholder={t('enter.car.number')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_brand'
                  label={t('car.brand')}
                  {...configs.vehicle_brand}
                >
                  <Input placeholder={t('enter.car.brand')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_type'
                  label={t('car.type')}
                  {...configs.vehicle_type}
                >
                  <Input placeholder={t('enter.car.type')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_color'
                  label={t('car.colour')}
                  {...configs.vehicle_colour}
                >
                  <Input placeholder={t('enter.car.colour')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='supervisor_id'
                  label={t('supervisor')}
                  {...configs.supervisor}
                >
                  <Select
                    loading={!supervisorList.length}
                    placeholder={t('select.branch')}
                    mode='multiple'
                    allowClear
                    onChange={(val) => setSelectedSupervisors(val)}
                  >
                    {supervisorList.length ? (
                      supervisorList.map(({ name, id }) => (
                        <Option key={id} value={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ margin: '0 -10px' }}>
              <Col span='12' style={{ padding: '0 10px' }}>
                <div
                  style={{
                    backgroundColor: 'grey',
                    width: '100%',
                    height: '100%',
                  }}
                >
                  <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
                    <Map
                      instanceRef={(ref) => setMapRef(ref)}
                      width='100%'
                      height='100%'
                      defaultState={defaultMapState}
                    >
                      <Placemark geometry={placemarkGeometry} />
                    </Map>
                  </YMaps>
                </div>
              </Col>
              <Col
                span='12'
                className='img-upload'
                style={{ padding: '0 10px' }}
              >
                <ImgCrop
                  rotate
                  aspect={16 / 9}
                  quality={1}
                  modalOk={t('ok')}
                  modalCancel={t('cancel')}
                  modalTitle={t('edit.image')}
                >
                  <Upload
                  {...props}
                    action={`${basic.BASE_URL}/upload/file`}
                    listType='picture-card'
                    onChange={onUploadChange}
                    showUploadList={false}
                    openFileDialogOnClick={openFileDialog}
                    beforeUpload={(file) => {
                      console.log("file",file);
                    
                      if (
                        file.type === 'image/png' ||
                        file.type === 'image/jpeg' ||
                        file.type === 'image/jpg' ||
                        file.type === 'image/webp'
                      ) {
                        setUploading(true);
                        return true;
                      }
                      message.error(t('invalid.image.format'));
                      return false;
                    }}
                  >
                    {imgUrl.length ? (
                      <div
                        className='img-content'
                        onMouseEnter={() => setOpenFileDialog(false)}
                        onMouseLeave={() => setOpenFileDialog(true)}
                      >
                        <img alt='uploded img' src={imgUrl} />
                        <div className='img-buttons'>
                          <EyeOutlined
                            style={{ color: '#fff', fontSize: 20 }}
                            onClick={(e) => {
                              window.open(imgUrl, '_blank');
                            }}
                          />
                          <DeleteOutlined
                            style={{
                              color: '#fff',
                              fontSize: 20,
                              marginLeft: 10,
                            }}
                            onClick={() => {
                              setImgUrl('');
                              setOpenFileDialog(true);
                            }}
                          />
                        </div>
                      </div>
                    ) : uploading ? (
                      <LoadingOutlined style={{ fontSize: 30 }} />
                    ) : (
                      <div style={{ color: 'rgba(0, 0, 0, 0.25)' }}>
                        <PlusSquareOutlined style={{ fontSize: '22px' }} />
                        <p style={{ fontSize: '15px' }}>{t('image.upload')}</p>
                      </div>
                    )}
                  </Upload>
                </ImgCrop>
              </Col>
              <Col span='24' style={{ padding: '0 10px', marginTop: 24 }}>
                <Table
                  bordered
                  pagination={false}
                  loading={false}
                  columns={columns}
                  dataSource={shifts}
                  rowKey={(record) => record.id}
                />
              </Col>
            </Row>
          </Card>
          <Row
            justify='end'
            style={{ paddingTop: '24px', backgroundColor: '#F9F9F9' }}
          >
            <Button type='primary' htmlType='submit' loading={loading}>
              {t('save')}
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

const weaks = [
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'sunday',
];
