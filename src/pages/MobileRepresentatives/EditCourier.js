import "./style.css"
import React, { useState, useEffect, useRef } from "react"
import basic from "@/constants/basic"
import moment from 'moment';
import ImgCrop from 'antd-img-crop';
import axios_init from '@/utils/axios_init'
import { validatePhone } from "@/utils/validatePhone"
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete"
import { defaultMapState } from "@/constants/basic"
import { useHistory, useLocation } from "react-router-dom" 
import { YMaps, Map, Polygon, Placemark } from 'react-yandex-maps';
import { PlusSquareOutlined, LoadingOutlined, EyeOutlined, DeleteOutlined } from '@ant-design/icons'
import { Row, Col, Card, Form, Input, Upload, Button, Select, Table, message, Switch, Modal } from 'antd';
const { Option } = Select

export default function EditCourier() {
  const { t } = useTranslation()
  const history = useHistory()
  const mapRef = useRef(null)
  const state = useLocation()?.state

  const [imgUrl, setImgUrl] = useState('')
  const [loading, setLoading] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [uploading, setUploading] = useState(false)
  const [branchList, setBranchList] = useState([])
  const [mapList, setMapList] = useState([])
  const [selectedZones, setSelectedZones] = useState("")
  const [selectedSupervisors, setSelectedSupervisors] = useState([])
  const [supervisorList, setSupervisorList] = useState([])
  const [status, setStatus] = useState(false)
  const [isModalVisible, setIsModalVisible] = useState(false)

  // const [phonenumber, setPhonenumber] = useState("")
  const [openFileDialog, setOpenFileDialog] = useState(true)
  const [shifts, setShifts] = useState([])
  // const [mapData, setMapData] = useState(_mapData)
  const [placemarkGeometry, setPlacemarkGeometry] = useState([])
  const [cells, setCells] = useState([[], [], [], [], [], [], []])
  
  const [planTable, setPlanTable] = useState([])
  const [updatedPlan, setUpdatedPlan] = useState(0)

  //get status
  useEffect(() => {
    getStatus()
  } , [])
  useEffect(() => {
    if(state) {
      // console.log('state => ', state)
      // getMaps()
      getBranches()
      getSupervisors()
      getCourierShifts()
      getMaps()
      getPlanTable()
      // getMapData(state.geozone_id)
      setPlacemarkGeometry([state.location.lat, state.location.long])
      setImgUrl(state.image)
      // setCells(state.timetable ? state.timetable.map((val) => {
      //   let index = null
      // setStatus(state.active == 1 ? true : false)
      // }) : [])
    } else {
      history.push("/couriers")
    }
  }, []);
  const onFinish = (values) => {
      // console.log(values);
      // if(!imgUrl.length) {
      //     message.error("Please upload image")
      // } else if(!placemarkGeometry.length) {
      //     message.error("Please mark the branch on the geozone")
      // } else {
      // }
      setLoading(true)
      // let from_time = moment(values.time_range[0]._d).format('HH:mm:ss')
      // let to_time = moment(values.time_range[1]._d).format('HH:mm:ss')

      let data = {
        ...values,
        phone: "+998" + values.phone,
        image: imgUrl,
        supervisor_id: values?.supervisor_id?.length ? values?.supervisor_id.join() : null,
        location: {
          "lat": placemarkGeometry[0],
          "long": placemarkGeometry[1]
        },
        timetable: cells.map(([index], i) => ({
          day: (i + 1)%7,
          from_time: shifts[index]?.from_time,
          to_time: shifts[index]?.to_time,
        })),
      }

      axios_init.put(`/courier/${state.id}`, data)
        .then(res => {
          message.success(t("saved successfully"))
          history.push('/couriers')
        })
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
  }

  const getMaps = () => {
    axios_init.get('/geozone')
        .then(({data}) => setMapList(data.geozones))
        .catch(err => console.log(err))
} 
  const getStatus = () => {
    axios_init.get(`/auth/user/full/${state.id}`)
      .then(({data}) => setStatus(data.active === 1 ? true : false))
        .catch((err) => console.log(err))
  } 

  const statusChecked = (e) => {
    setStatus(e)
    axios_init.put('/auth/user/activity', {
      active: !status ? 1 : 0,
      user_id: state.id
    }).then(res => {
      message.success(t("saved successfully"))
      .catch(err => console.log(err))
      .finally(() => setLoading(false))
    })
  }

  const getBranches = () => {
    axios_init.get('/branch')
      .then(({data}) => setBranchList(data.branches))
      .catch(err => console.log(err))
  }
  const getSupervisors = () => {
    axios_init.get('/supervisor')
      .then(({data}) => setSupervisorList(data.couriers))
      .catch(err => console.log(err))
  }
  
  const getCourierShifts = () => {
    axios_init.get('/courier-shift', {arrangement: "asc"})
      .then(({data}) => {
        setShifts(data.shifts)
        // console.log(data)

        setCells(state.timetable ? state.timetable.map((val) => {
          let index = null
          data.shifts.forEach((value, i) => {
            if(val.from_time === value.from_time) index = i
          })
          return index !== null ? [index] : []
        }) : [])
      })
      .catch(err => console.log(err))
  }

  //get plan table
  const getPlanTable = () => {
    setTableLoading(true)
    axios_init.get('/couriers-plan', {courier_id: state.id})
    .then((res) =>  {
      setPlanTable(res.data)
      setUpdatedPlan(res.data[0].plan)
    })
    .catch((err) => console.log(err))
    .finally(() => setTableLoading(false))
  }

  //onPlanUpdate
  const onPlanUpdate = (data) => {
    const order = {
      courier_id: state.id,
      month: planTable[0].month,
      fact:  planTable[0].fact,
      id: planTable[0].id,
      plan: Number(updatedPlan)
    }
    axios_init.put('/couriers-plan', order)
    .then(res => {
      message.success(t("saved successfully"))
      setIsModalVisible(false)
      getPlanTable()

    })
    .catch(err => console.log(err))
  }
  // const getMaps = () => {
  //     axios_init.get('/geozone')
  //         .then(({data}) => setMapList(data.geozones))
  //         .catch(err => console.log(err))
  // }
  
  const onChange = (info) => {
      // setFileList(newFileList);
      if (info.file.status !== 'uploading') {
          // console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
          message.success(t("image.uploaded.successfully"));
          // console.log(info.file)
          setImgUrl(info.file.response.url)
          setUploading(false)
      } else if (info.file.status === 'error') {
          message.error(t("image.uploading.failed"));
          setUploading(false)
      }
  };

  const getMapData = (id) => {
  //     axios_init.get(`/geozone/${id}`)
  //       .then(({data}) => {
  //         setMapData(JSON.parse(data.shape.data)) 
  //         console.log(JSON.parse(data.shape.data))
  //       }).catch(err => console.log(err))
  }

  const setMapRef = ref => {
      if(ref) {
          mapRef.current = ref;

          ref.events.add('click', (e) => {
              setPlacemarkGeometry(e.get('coords'))
              // console.log(e.get('coords'))
          })
      }
  }
  
  // const onChangePhone = (val) => {
  //   let value = val.split(" ").join("")
  //   let result = "";
    
  //   if(value.length === 1) {
  //     if(isFinite(value)) {
  //       result = `+998 ${value}`
  //       setPhonenumber(result.trim())
  //     }
  //   } else {
  //     if(isFinite(value.slice(4,13))) {
  //       result = `+998 ${value.slice(4,6)} ${value.slice(6,9)} ${value.slice(9,11)} ${value.slice(11,13)}`
  //       setPhonenumber(result.trim())
  //     }
  //   }
  // }

  const handleSelectCell = (i, index) => {
    // if(cells.includes(index)) {
    //   setCells(old => old.filter(v => v !== index))
    // } else {
    //   setCells(old => [...old, index])
    // }

    if(cells[i]?.includes(index)) {
      setCells(old => old.map((val, indx) => indx === i ? [] : val))
    } else {
      setCells(old => old.map((val, indx) => indx === i ? [index] : val))
    }
  }
  
  // const timeDefVal = () => {
  //   return [
  //     moment(state.from_time.slice(11, 19), 'HH:mm:ss'),
  //     moment(state.to_time.slice(11, 19), 'HH:mm:ss'),
  //   ]
  // }

  const columns = [
    {
      title: "",
      dataIndex: "range",
      key: "range",
      render: (val, data) => ( //moment(data).format('HH:mm')
        <div className="time-range">
          <span>{data.from_time.slice(11, 16)}</span>
          <div className="vertical-divider"></div>
          <span>{data.to_time.slice(11, 16)}</span>
        </div>
      ) 
    },
    ...weaks.map((val, i) => ({
      title: t(val),
      // dataIndex: val,
      align: 'center',
      key: i,
      render: (val, data, index) => (
        <div 
          className="table-cell" 
          // style={cells.includes(i) ? {backgroundColor: 'rgba(255, 59, 48, 0.1)'} : {border: 'none'}}
          style={cells[i === 6 ? 0 : i+1]?.includes(index) ? {backgroundColor: 'rgba(255, 59, 48, 0.1)'} : {border: 'none'}}
          onClick={() => handleSelectCell(i === 6 ? 0 : i+1, index)}
        />
      )
    }))
  ]

  //plan table columns
  const planColumns = [
    {
      title: t('month'),
      dataIndex: "month",
      key: "month",
      render: (date) => (
        <div>{t(moment(date).utcOffset(0).format('MMMM'))}</div>
      ),
    },
    {
      title: t('plan'),
      dataIndex: "plan",
      key: "plan",
    },
     {
      title: t('fact'),
      dataIndex: "fact",
      key: "fact",
    }
  ]
  //update modal columns
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const configs = { 
    name: {
      rules: [
        {
          required: true,
          message: t("please.input.name"),
        },
      ],
    },
    address: {
      rules: [
        {
          required: true,
          message: t("please.input.address"),
        },
      ],
    },
    phone: {
      rules: [
        {
          required: true,
          message: t("please.input.phone"),
        },
        {
          validator: (_, value) => validatePhone(value)
        }
      ],
    },
    phone_brand: {
      rules: [
        {
          required: false,
          message: t("please.input.phone.brand"),
        },
      ],
    },
    phone_version: {
      rules: [
        {
          required: false,
          message: t("please.input.phone.version"),
        },
      ],
    },
    phone_model: {
      rules: [
        {
          required: false,
          message: t("please.input.phone.model"),
        },
      ],
    },
    geozone: {
      rules: [
        {
          required: true,
          message: t("please.select.geozone"),
        },
      ],
    },
    vehicle_model: {
      rules: [
        {
          required: true,
          message: t("please.input.car.model"),
        },
      ],
    },
    vehicle_number: {
      rules: [
        {
          required: true,
          message: t("please.input.car.number"),
        },
      ],
    },
    vehicle_brand: {
      rules: [
        {
          required: true,
          message: t('please.input.car.brand'),
        },
      ],
    },
    vehicle_type: {
      rules: [
        {
          required: true,
          message: t('please.input.car.type'),
        },
      ],
    },
    vehicle_colour: {
      rules: [
        {
          required: true,
          message: t('please.input.car.colour'),
        },
      ],
    },
    supervisor: {
      rules: [
        {
          required: true,
          message: t("please.select.supervisor"),
        },
      ],
    }
  };
  

  const routes = [
    {
      name: 'mobile.representatives',
      route: '/couriers',
      link: true
    },
    {
      name: 'edit',
      route: '/couriers/edit',
      link: false
    }
  ]

  return (
    <div className="mobile-representatives">
      <BreadCrumbTemplete routes={routes} />

      <div className="create-edit">
          <Form 
            layout="vertical" 
            onFinish={onFinish}
            initialValues={state ? {
              ...state,
              phone: state.phone.split("+998")[1],
            } : {}}
          >
            <Card title={t('mobile.representatives')}>
              <Row style={{ margin: '0 -10px' }}>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="name" label={t('fio')} {...configs.name}>
                        <Input placeholder={t('enter.name')} />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="address" label={t('address')} {...configs.address}>
                        <Input placeholder={t('enter.address')} />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="phone" label={t('phone.number')} {...configs.phone}>
                        <Input placeholder={t('enter.phone.number')}  prefix="(+998)" />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="phone_brand" label={t('phone.brand')} {...configs.phone_brand}>
                        <Input disabled placeholder={t('enter.phone.brand')}   />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="phone_model" label={t('phone.model')} {...configs.phone_model}>
                        <Input disabled placeholder={t('enter.phone.model')} />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="phone_version" label={t('phone.version')} {...configs.phone_version}>
                        <Input disabled placeholder={t('enter.phone.version')}  />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="branch_id" label={t('branches')}>
                        <Select 
                            loading={!branchList.length} 
                            placeholder={t('select.branch')} 
                            // onChange={onSelectChange}
                        >
                            {branchList.length ? branchList.map(({name, id}) => (
                                <Option key={id} value={id}>{name}</Option>
                            )) : <></>}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="district_id" label={t('geozones')}>
                        <Select 
                            loading={!mapList.length} 
                            placeholder={t('select.geofence')} 
                            // mode=""
                            allowClear
                            onChange={val => {setSelectedZones(val); getMapData(val)}}
                        >
                            {mapList.length ? mapList.map(({name, id}) => (
                                <Option key={id} value={id}>{name}</Option>
                            )) : <></>}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="vehicle_model" label={t('car.model')} {...configs.vehicle_model}>
                        <Input placeholder={t('enter.car.model')} />
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="vehicle_number" label={t('car.number')} {...configs.vehicle_number}>
                        <Input placeholder={t('enter.car.number')} />
                    </Form.Item>
                </Col>
                <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_brand'
                  label={t('car.brand')}
                  {...configs.vehicle_brand}
                >
                  <Input placeholder={t('enter.car.brand')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_type'
                  label={t('car.type')}
                  {...configs.vehicle_type}
                >
                  <Input placeholder={t('enter.car.type')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='vehicle_color'
                  label={t('car.colour')}
                  {...configs.vehicle_colour}
                >
                  <Input placeholder={t('enter.car.colour')} />
                </Form.Item>
              </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="supervisor_id" label={t('supervisor')} {...configs.supervisor}>
                        <Select 
                            loading={!supervisorList.length} 
                            placeholder={t('select.branch')} 
                            mode="multiple"
                            allowClear
                            onChange={val => setSelectedSupervisors(val) }
                        >
                            {supervisorList.length ? supervisorList.map(({name, id}) => (
                                <Option key={id} value={id}>{name}</Option>
                            )) : <></>}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span="8" style={{ padding: '0 10px' }}>
                    <Form.Item name="active" label={t('status')}>
                        <Switch checked={status}
                         onChange={statusChecked}  />
                    </Form.Item>
                </Col>
              </Row>
              <Row style={{ margin: '0 -10px' }}>
                  <Col span="12" style={{padding: '0 10px'}}>
                      <div style={{backgroundColor: 'grey', width: '100%', height: '100%'}}>
                          <YMaps query={{ lang: "ru_RU", load: "package.full" }}>
                            <Map 
                              instanceRef={ref => setMapRef(ref)}
                              width="100%" 
                              height="100%" 
                              defaultState={{...defaultMapState, center: placemarkGeometry}}
                            >
                              {/* <Polygon
                                  geometry={mapData?.geometry}
                                  options={{...mapData?.options, draggable: false, hasHint: false}}
                                  instanceRef={ref => setPolygonRef(ref)}
                              /> */}
                              <Placemark 
                                  geometry={placemarkGeometry}
                                  // instanceRef={ref => ref && setPlacemarkRef(ref)}
                              />
                            </Map>
                          </YMaps>
                      </div>
                  </Col>
                  <Col span="12" className="img-upload" style={{ padding: '0 10px' }}>
                      <ImgCrop 
                          rotate 
                          aspect={16/9}
                          quality={1}
                          modalOk={t("ok")}
                          modalCancel={t("cancel")}
                          modalTitle={t("edit.image")}
                      >
                          <Upload
                              action={`${basic.BASE_URL}/upload/file`}
                              listType="picture-card"
                              onChange={onChange}
                              showUploadList={false}
                              openFileDialogOnClick={openFileDialog}
                              beforeUpload={(file) => {
                                if(file.type === 'image/png' || 
                                  file.type === 'image/jpeg' || 
                                  file.type === 'image/jpg' || 
                                  file.type === 'image/webp'
                                ) {
                                  setUploading(true); 
                                  return true
                                }
                                message.error(t('invalid.image.format'))
                                return false
                              }}
                          >
                              {uploading ? (
                                  <LoadingOutlined style={{fontSize: 30}}  />
                              ) : imgUrl.length ? (
                                <div
                                  className="img-content"
                                  onMouseEnter={() => setOpenFileDialog(false)} 
                                  onMouseLeave={() => setOpenFileDialog(true)} 
                                >
                                  <img alt="uploded img" src={imgUrl} />
                                  <div className="img-buttons">
                                    <EyeOutlined 
                                      style={{color: "#fff", fontSize: 20}} 
                                      onClick={(e) => {window.open(imgUrl, '_blank')}} 
                                    />
                                    <DeleteOutlined 
                                      style={{color: "#fff", fontSize: 20, marginLeft: 10}}
                                      onClick={() => {setImgUrl(""); setOpenFileDialog(true)}}
                                    />
                                  </div>
                                </div>
                              ) : (
                                  <div style={{color: "rgba(0, 0, 0, 0.25)"}}>
                                      <PlusSquareOutlined style={{fontSize:'22px'}} />
                                      <p style={{fontSize:"15px"}}>{t('image.upload')}</p>
                                  </div>
                              )}
                          </Upload>
                      </ImgCrop>
                  </Col>
                  <Col span="24" style={{ padding: '0 10px', marginTop: 24 }}>
                    <Table 
                      bordered 
                      pagination={false}
                      loading={false}
                      columns={columns} 
                      dataSource={shifts} 
                      rowKey={ (record) => record.id } 
                    />
                  </Col>
              </Row>
            </Card>
            <Row style={{ margin: '0 10px 10px' }}>
            <Col span="24" style={{ padding: '0 10px', marginTop: 24 }}>
                    <Table 
                      bordered 
                      pagination={false}
                      loading={tableLoading}
                      columns={planColumns} 
                      dataSource={planTable} 
                      rowKey={ (record) => record.id } 
                      onRow={(record, index) => {
                        if(index === 0) {
                          return {
                            onClick: e => showModal()
                          };
                        }
                      }}
                    />
                  </Col>
            </Row>
            <Row justify="end" style={{ paddingTop: "24px", backgroundColor: "#F9F9F9" }}>
              <Button
                type="primary"
                htmlType="submit"
                loading={loading}
              >
                {t('save')}
              </Button>
            </Row>
          </Form>
          <Modal keyboard={true} title={t('edit')} visible={isModalVisible} onOk={onPlanUpdate} onCancel={handleCancel} >
            <Form initialValues={planTable ? planTable[0] : {}}>
              <Form.Item name="plan" label={t('plan')} {...configs.plan} >
                <Input type='number' min="0" onChange= { e => setUpdatedPlan(e.target.value)}/>
              </Form.Item>
            </Form>
          </Modal>
      </div>
    </div>
  )
}


const weaks = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday"
]