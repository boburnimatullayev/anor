import React, { useEffect, useState } from 'react'
import moment from 'moment'
import StyledTag from '@/components/StyledTag/StyledTag'
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import { Modal, Input, Table, Tag } from 'antd'

const { Search } = Input

export default function OrderHistories({ courierId, setCourierId }) {
  const { t } = useTranslation()

  const [items, setItems] = useState([])
  const [loading, setLoading] = useState(false)
  const [sort, setSort] = useState({ order: '', arrangement: '' })
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    if (courierId) getItems()
  }, [courierId, reqQuery, sort, searchText])

  const getItems = () => {
    axios_init
      .get(`/courier-order/${courierId}`, {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        arrangement: arrng[sort.arrangement],
        order: sort.order,
        search: searchText,
      })
      .then(({ data }) => setItems(data))
      .catch((err) => console.log(err))
      .finally(() => setLoading(false))
  }

  const columns = [
    {
      title: t('status'),
      key: 'status_id',
      dataIndex: 'status',
      sorter: {},
      defaultSortOrder:
        sort.order === 'status_id' ? sort.arrangement : undefined,
      render: (text, data) => {
        return (
          <StyledTag>
            {data.status_id === '' ? t('new') : t(data.status_id)}
          </StyledTag>
        )
      },
    },
    {
      title: t('client'),
      dataIndex: 'customer_name',
      key: 'customer_name',
      sorter: {},
      defaultSortOrder:
        sort.order === 'customer_name' ? sort.arrangement : undefined,
    },
    {
      title: t('phone.number'),
      dataIndex: 'customer_phone',
      key: 'customer_phone',
      sorter: {},
      defaultSortOrder:
        sort.order === 'customer_phone' ? sort.arrangement : undefined,
    },
    {
      title: t('address'),
      dataIndex: 'customer_address',
      key: 'customer_address',
    },
    {
      title: t('products'),
      dataIndex: 'product',
      key: 'product',
      width: 200,
      render: (product) =>
        product.name.length ? (
          <Tag color='blue'>{product.name}</Tag>
        ) : (
          'нет продукта'
        ),
    },
    {
      title: t('created'),
      dataIndex: 'created_at',
      key: 'created_at',
      sorter: {},
      defaultSortOrder:
        sort.order === 'created_at' ? sort.arrangement : undefined,
      render: (date) => <div>{moment(date).format('YYYY-MM-DD HH:mm')}</div>,
    },
    {
      title: t('delivered'),
      dataIndex: 'from_time',
      key: 'delivery_day',
      render: (date, val) => {
        return `${moment(date).format('YYYY-MM-DD')} | ${moment(
          new Date(new Date(date).getTime() + 1800000)
        )
          .utc()
          .format('HH:mm')}`
      },
    },
  ]

  const onSearchChange = (text) => {
    setSearchText(text)
    setReqQuery((old) => ({ ...old, offset: 0 }))
  }

  const onSortChange = (pagination, filters, sorter) => {
    console.log(sorter)
    setSort({ order: sorter.columnKey, arrangement: sorter.order })
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  return (
    <Modal
      title={t('view.order')}
      visible={courierId}
      width={1200}
      he
      onCancel={() => setCourierId(null)}
      footer={null}
    >
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Search
          allowClear
          style={{ width: 300 }}
          placeholder={`${t('search')}...`}
          onSearch={onSearchChange}
          onChange={(e) => e.target.value === '' && setSearchText('')}
        />
      </div>
      <Table
        style={{ marginTop: 24 }}
        loading={loading}
        scroll={{ y: 400 }}
        columns={columns}
        dataSource={items.orders}
        pagination={pagination}
        rowKey={(record) => record.id}
        onChange={onSortChange}
      />
    </Modal>
  )
}

const arrng = {
  descend: 'desc',
  ascend: 'asc',
}
