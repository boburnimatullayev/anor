import './style.css'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../components/breadcrumb/BreadCrumbTemplete'
import { Button, Card, Table, message, Popconfirm, Tag } from 'antd'
import StyledTag from '@/components/StyledTag/StyledTag'
import StyledButton from '@/components/StyledButton/StyledButton'
import axios_init from '../../utils/axios_init'
import usePermission from '@/utils/usePermission'
import {
  SearchOutlined,
  PlusOutlined,
  EditFilled,
  DeleteFilled,
} from '@ant-design/icons'
import { useHistory, Link } from 'react-router-dom'

export default function Documents() {
  const { t } = useTranslation()
  const [items, setItems] = useState([])
  const history = useHistory()
  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })

  useEffect(() => {
    getData()
  }, [reqQuery])

  const getData = function () {
    axios_init.get('/document', {
      limit: reqQuery.limit,
      offset: reqQuery.offset
    }).then((res) => {
        setItems(res.data)
        // console.log(res)
      }).catch((err) => {
        console.log(err)
      })
  }
  const columns = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
      render: (text) => <Link>{text}</Link>,
    },
    {
      title: t('action'),
      key: 'action',
      align: 'center',
      render: (text, record, index) => (
        <div>
          {
            usePermission('docs/update') &&
            <StyledButton
              color='link'
              icon={EditFilled}
              tooltip={t("edit")}
              onClick={() => editItem(text, record)}
            />
          }
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => deleteItem(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
            {
              usePermission('docs/delete') &&
              <StyledButton
                color='danger'
                icon={DeleteFilled}
                tooltip={t("delete")}
                onClick={() => setVisible(index)}
              />
            }
          </Popconfirm>
        </div>
      ),
    },
  ]

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false,
    },
    {
      name: 'documents',
      // route: '/catalog/documents',
      link: false,
    },
    // {
    //   name: 'documents',
    //   route: '/documents',
    //   link: false,
    // },
  ]
  const ExtraButton = function () {
    return (
      <Button
        type='primary'
        icon={<PlusOutlined />}
        onClick={() => {
          history.push('/catalog/documents/create')
        }}
      >
        {t('add')}
      </Button>
    )
  }
  const editItem = function (text) {
    console.log('Text:', text)
    // console.log('Record:', record)
    history.push({
      pathname: '/catalog/documents/edit',
      state: text,
    })
  }
  const deleteItem = function (text) {
    setLoading(true)
    axios_init
      .remove(`/document/${text.id}`)
      .then((res) => {
        getData(reqQuery.limit, reqQuery.offset)
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }
  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
    // getItems(pageSize, offset)
  }

  const pagination = {
    // showSizeChanger: true,
    total: items.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 10,
    // pageSizeOptions: [10, 20, 50, 100],
    // defaultPageSize: 2,
    // pageSizeOptions: [2, 4, 8, 16],
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }
  return (
    <div className='documents'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <Card title={t('documents')} extra={usePermission('docs/create') ? <ExtraButton /> : <div></div>}>
          <Table
            bordered
            pagination={pagination}
            columns={columns}
            dataSource={items.documents}
            rowKey={(record) => record.id}
            scroll={{ x: 100 }}
            locale={{
              filterConfirm: t('ok'),
              filterReset: t('reset'),
              emptyText: t('no.data'),
            }}
          />
        </Card>
      </div>
    </div>
  )
}
