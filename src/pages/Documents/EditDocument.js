import "./style.css"
import React, { useState, useEffect } from 'react'
import basic from "@/constants/basic"
import ImgCrop from 'antd-img-crop';
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { useHistory, useLocation } from 'react-router-dom'
import { PlusSquareOutlined, LoadingOutlined, EyeOutlined, DeleteOutlined } from '@ant-design/icons'
import {
  Row,
  Col,
  Card,
  Form,
  Radio,
  Input,
  Upload,
  Button,
  Select,
  message,
  Checkbox,
  TimePicker,
} from 'antd'

const { Option } = Select

export default function EditDocument() {
  const { t } = useTranslation()
  const state = useLocation().state
  const history = useHistory()
  
  const [imgUrl, setImgUrl] = useState('')
  const [loading, setLoading] = useState(false)
  const [uploading, setUploading] = useState(false)
  const [openFileDialog, setOpenFileDialog] = useState(true)

  useEffect(() => {
    console.log("state", state)
    if(state) {
      setImgUrl(state.rule.img_url ?? "")
    } else {
      history.push("/catalog/documents")
    }
  }, [])

  const onFinish = ({name, pages, from, to, action, action_radio}) => {
    setLoading(true)

    axios_init
      .put(`/document/${state.id}`, {
        name,
        rule: {
          from,
          to,
          pages: +pages,
          img_url: imgUrl,
          action: {
            photo: action_radio === "photo",
            scan: action_radio === "scan",
            signature: action.includes("signature"),
            copy: action.includes("copy"),
            original: action.includes("original"),
          }
        }
      })
      .then((res) => {
        console.log(res)
        message.success(t('saved.successfully'))
        history.push('/catalog/documents')
      })
      .catch((err) => {
        console.log(err)
        message.error(t('saving.failed'))
      })
      .finally(() => setLoading(false))
  }

  const onUploadChange = (info) => {
    // setFileList(newFileList);
    if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
        message.success(t("image.uploaded.successfully"));
        console.log(info.file)
        setImgUrl(info.file.response.url)
        setUploading(false)
    } else if (info.file.status === 'error') {
        message.error(t("image.uploading.failed"));
        setUploading(false)
    }
  };

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false,
    },
    {
      name: 'documents',
      route: '/catalog/documents',
      link: true,
    },
    {
      name: 'edit',
      route: '/catalog/documents/edit',
      link: false,
    },
  ]
  const configs = {
    name: {
      rules: [
        {
          required: true,
          message: t('please.fill.name'),
        },
      ],
    },
    pages: {
      rules: [
        {
          required: true,
          message: t('please.input.number.of.pages'),
        },
        {
          validator: (_, value) => {
            if(value < 1) {
              return Promise.reject(new Error(t("it.should.be.greater.than.zero")))
            } else {
              return Promise.resolve()
            }
          }
        },
      ],
    },
  }

  const plainOptions = [
    { label: t("signature"), value: 'signature' },
    { label: t("copy"), value: 'copy' },
    { label: t("original"), value: 'original' }
  ]
  const radioOptions = [
    { label: t("photo"), value: 'photo' },
    { label: t("scan"), value: 'scan' },
  ]

  return (
    <div className='documents'>
      <BreadCrumbTemplete routes={routes} />

      <div className='create-edit'>
        <Form
          layout='vertical'
          onFinish={onFinish}
          initialValues={state ? { 
              name: state.name,
              from: state.rule?.from,
              to: state.rule?.to,
              pages: state.rule?.pages,
              action: makeActionArray(state.rule?.action),
              action_radio: selectedRadio(state.rule?.action)
          } : {}}
        >
          <Card title={t('documents')}>
            <Row>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='name'
                  label={t('document.name')}
                  {...configs.name}
                >
                  <Input placeholder={t('enter.product.name')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='from'
                  label={t('from')}
                >
                  <Select allowClear placeholder={t("select.from.where")}>
                    {toWhere.map((value) => (
                      <Option key={value} value={value}>{t(value)}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='to'
                  label={t('to')}
                >
                  <Select allowClear placeholder={t("select.to.where")}>
                    {toWhere.map((value) => (
                      <Option key={value} value={value}>{t(value)}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span='16' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='action_radio'
                  label={t('action')}
                >
                  <Radio.Group options={radioOptions} />
                </Form.Item>
                <Form.Item
                  name='action'
                  // label={t('action')}
                >
                  <Checkbox.Group options={plainOptions} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='pages'
                  label={t('number.of.pages')}
                  {...configs.pages}
                >
                  <Input type="number" placeholder={t('enter.number.of.pages')} />
                </Form.Item>
              </Col>
              <Col span="12" className="img-upload" style={{ padding: '0 10px' }}>
                <ImgCrop 
                  rotate 
                  aspect={16/9}
                  quality={1}
                  modalOk={t("ok")}
                  modalCancel={t("cancel")}
                  modalTitle={t("edit.image")}
                >
                  <Upload
                    action={`${basic.BASE_URL}/upload/file`}
                    listType="picture-card"
                    onChange={onUploadChange}
                    showUploadList={false}
                    openFileDialogOnClick={openFileDialog}
                    beforeUpload={(file) => {
                      if(file.type === 'image/png' || 
                        file.type === 'image/jpeg' || 
                        file.type === 'image/jpg' || 
                        file.type === 'image/webp'
                      ) {
                        setUploading(true); 
                        return true
                      }
                      message.error(t('invalid.image.format'))
                      return false
                    }}
                  >
                      {uploading ? (
                        <LoadingOutlined style={{fontSize: 30}}  />
                      ) : imgUrl.length ? (
                        <div 
                          className="img-content"
                          onMouseEnter={() => setOpenFileDialog(false)} 
                          onMouseLeave={() => setOpenFileDialog(true)} 
                        >
                          <img alt="uploded img" src={imgUrl} />
                          <div className="img-buttons">
                            <EyeOutlined 
                              style={{color: "#fff", fontSize: 20}} 
                              onClick={(e) => {window.open(imgUrl, '_blank')}} 
                            />
                            <DeleteOutlined 
                              style={{color: "#fff", fontSize: 20, marginLeft: 10}}
                              onClick={() => {setImgUrl(""); setOpenFileDialog(true)}}
                            />
                          </div>
                        </div>
                      ) : (
                        <div style={{color: "rgba(0, 0, 0, 0.25)"}}>
                          <PlusSquareOutlined style={{fontSize:'22px'}} />
                          <p style={{fontSize:"15px"}}>{t('image.upload')}</p>
                        </div>
                      )}
                  </Upload>
                </ImgCrop>
              </Col>
            </Row>
          </Card>
          <Row
            justify='end'
            style={{ paddingTop: '24px', backgroundColor: '#F9F9F9' }}
          >
            <Button
              type='primary'
              htmlType='submit'
              disabled={uploading}
              loading={loading}
            >
              {t('save')}
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  )
}

const toWhere = ["bank", "client"]

const makeActionArray = (actions) => {
  let res = []
  if(!actions) return res; 

  for (let key in actions) {
    if(actions[key] && key !== "photo" && key !== "scan") res.push(key)
  }

  return res
}

const selectedRadio = (actions) => {
  if(!actions) return undefined;
  if(actions.photo) return "photo";
  if(actions.scan) return "scan";
  return undefined;
}