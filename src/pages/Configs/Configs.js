import './style.css'
import React, { useState, useEffect } from 'react'
import axios_init from '../../utils/axios_init'
import StyledButton from '@/components/StyledButton/StyledButton'
import {EditFilled } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { Card, Button, Table, Modal, Form, Input } from 'antd'

export default function Configs() {
  const { t } = useTranslation()

  const [form] = Form.useForm()
  const [items, setItems] = useState([])
  const [defaultValue, setDefaultValue] = useState({})
  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(null)

  useEffect(() => {
    getConfigs()
  }, [])

  const getConfigs = () => {
    axios_init.get('/configs').then((res) => setItems(res.data))
  }

  const onFinish = (values) => {
    if (visible === 'edit') {
      setLoading(true)
      axios_init
        .put(`/configs/${values.name}`, {
          name: values.name,
          value: {
            [values.name]: values.value,
          },
        })
        .then((res) => console.log(res))
        .finally(() => {
          getConfigs()
          setLoading(false)
          setVisible(null)
        })
    }
  }

  const editItem = (text, record) => {
    setDefaultValue({
      name: record.name,
      value: record.value[Object.keys(record.value)[0]],
    })
    setVisible('edit')
  }

  useEffect(() => {
    form.resetFields()
  }, [visible])


  const columns = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
      render: (text) => t(text),
    },
    {
      title: t('value'),
      dataIndex: 'value',
      key: 'value',
      render: (text) => text[Object.keys(text)[0]],
    },
    {
      title: t('action'),
      key: 'action',
      align: 'center',
      render: (text, record, index) => (
        <div>
          <StyledButton color='link' icon={EditFilled} onClick={() => editItem(text, record)} tooltip={t('edit')} />
        </div>
      ),
    },
  ]

  const routes = [
    {
      name: 'configs',
      route: '/configs',
      link: false,
    },
  ]
  return (
    <div className='configs'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <Card title={t('configs')} >
          <Table bordered columns={columns} dataSource={items.configs} rowKey={(record) => record.created_at} />
        </Card>
        <Modal visible={visible} footer={null} onCancel={() => setVisible(null)}>
          {visible && (
            <Form form={form} layout='vertical' onFinish={onFinish} initialValues={defaultValue}>
              <Form.Item name='name' label={t('name')} required>
                <Input disabled placeholder={t('enter.name')} />
              </Form.Item>
              <Form.Item name='value' label={t('value')} required>
                <Input type={defaultValue?.name==="Sms message" ? 'text':"number"} placeholder={t('enter.name')} />
              </Form.Item>
              <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
                <Button type='primary' htmlType='submit' loading={loading}>
                  {t('save')}
                </Button>
              </div>
            </Form>
          )}
        </Modal>
      </div>
    </div>
  )
}
