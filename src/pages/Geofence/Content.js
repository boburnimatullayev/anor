import "./style.css"
import React, { useEffect, useState } from "react"
import usePermissions from '../../utils/usePermission'
import map_img from "../../assets/images/map_img.png"
import axios_init from "@/utils/axios_init";
import { ViewMap } from "./Map/ViewMap"
import StyledButton from "@/components/StyledButton/StyledButton"
import { useHistory } from "react-router-dom"
import { useTranslation } from "react-i18next";
import { EditOutlined, DeleteOutlined, EditFilled, DeleteFilled, EyeOutlined } from '@ant-design/icons';
import { Row, Col, Card, Typography, Pagination, Popconfirm, Button, message } from 'antd';
// import { LoaderTargetPlugin } from "webpack";

const { Text } = Typography

export function Content(props) {
    const { t } = useTranslation()
    const history = useHistory()

    const [mapData, setMapData] = useState([])
    const [visible, setVisible] = useState(null)
    const [loading, setLoading] = useState(false)
    const [selectedMap, setSelectedMap] = useState({})
    const [visibleModal, setVisibleModal] = useState(false)
    const [reqQuery, setReqQuery] = useState({limit: 12, offset: 0})

    useEffect(() => {
        getItems()
    }, []);

    const getItems = (limit = reqQuery.limit, offset = reqQuery.offset) => {
        axios_init.get(`/geozone?limit=${limit}&offset=${offset}`)
            .then(res => {setMapData(res.data)})
            .catch(err => console.log(err))
    }

    const handleEdit = id => {
        history.push({
            pathname: "/geofence/edit",
            state: {_id: id}
        })
    }

    const handleDelete = id => {
        setLoading(true)
        axios_init.remove(`/geozone/${id}`)
            .then(data => {
                message.success(t("deleted.successfully"));
                getItems(reqQuery.limit, reqQuery.offset)
                // console.log(data)
            }).catch(err => {
                console.log(err)
                message.error(t("deleting.failed"));
            }).finally(() => {
                setLoading(false);
                setVisible(null)
            })
    }

    const handleView = (data) => {
        let _data = JSON.parse(data.shape.data)
        _data.name = data.name
        setSelectedMap(_data)
        // console.log(_data)
        setVisibleModal(true)
    }

    const handleCreate = () => {
        history.push("/geofence/create")
    }

    const onShowSizeChange = (page, pageSize) => {
        let offset = (page - 1)*pageSize
        setReqQuery({limit: pageSize, offset})
        getItems(pageSize, offset)
    }

    return (
        <div>
            <Row  className="geozone-header" justify="space-between">
                <Col>
                    <h2 style={{margin: 0}}>{t("geofence")}</h2>
                </Col>
                <Col>
                {
                    usePermissions('geozona/create') &&
                    <Button type="primary" htmlType="submit" onClick={handleCreate}>
                        {t("create")}
                    </Button>
                }
           </Col>
            </Row>
            <Row style={{margin: "0 -10px"}} className="content">
                {mapData.geozones ? mapData.geozones.map((data, index) => (
                    <Col xl={6} lg={8} md={12} sm={24} style={{padding: 10}} key={index}>
                        <Card>
                            <div className="map-content">
                                <img src={map_img} width="100%" />
                            </div>
                            <div className="card-footer">
                                <div className="card-footer-text">
                                    {/* <p style={{fontSize: 12}} className="truncate"><span>{data.name}</span></p> */}
                                    <Text style={{fontSize: 12}} className="truncate" strong>{data.name}</Text>
                                </div>
                                <div className="toolbar">
                                    <StyledButton 
                                        color="view"
                                        icon={EyeOutlined}
                                        onClick={() => handleView(data)}
                                        tooltip={t("view")}
                                    />
                                    {
                                        props.update &&
                                        <StyledButton 
                                            color="link"
                                            icon={EditFilled}
                                            onClick={() => handleEdit(data.id)}
                                            tooltip={t("edit")}
                                        />                                        
                                    }
                                    <Popconfirm
                                        title={t("do.you.really.want.to.delete")}
                                        visible={visible === index}
                                        onConfirm={() => handleDelete(data.id)}
                                        okButtonProps={{ loading: loading }}
                                        onCancel={() => setVisible(null)}
                                        cancelText={t("cancel")}
                                        okText={t("yes")}
                                    >
                                    {
                                        props.delete && (
                                            <StyledButton 
                                                color="danger"
                                                icon={DeleteFilled}
                                                onClick={() => setVisible(index)}
                                                tooltip={t("delete")}
                                                />
                                            
                                        )
                                    }
                                    </Popconfirm>
                                </div>
                            </div>
                        </Card>
                    </Col>
                )) : (
                    <h1>Loading...</h1>
                )}
            </Row>
            <Row justify="end">
                <Col style={{padding: 10}}>
                    <Pagination
                        // showSizeChanger
                        total={mapData.count}
                        defaultCurrent={1}
                        // defaultPageSize={2}
                        // pageSizeOptions={[2, 4, 8, 16]}
                        defaultPageSize={12}
                        // pageSizeOptions={[12, 24, 48, 96]}
                        onShowSizeChange={onShowSizeChange}
                        onChange={onShowSizeChange}
                    />
                </Col>
            </Row>
            <ViewMap
                title={selectedMap.name}
                visible={visibleModal}
                onCancel={() => setVisibleModal(false)}
                footer={null}
                data={selectedMap}
            />
        </div>
    )
}

const arr = ["", "", "", "", "", "", "", "", "", "", "", ""]