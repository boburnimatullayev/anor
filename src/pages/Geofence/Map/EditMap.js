import './style.css'
import React, { useRef, useState, useEffect } from 'react'
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { YMaps, Map, Polygon } from 'react-yandex-maps'
import { useLocation, useHistory } from 'react-router-dom'
import { Row, Col, Form, Input, Button, Modal } from 'antd'

export default function EditMap() {
  const mapRef = useRef(null)
  const polygonRef = useRef(null)

  const [mapState, setMapState] = useState({
    center: [41.311151, 69.279737],
    zoom: 12,
  })
  const [geometry, setGeometry] = useState([])
  const [visible, setVisible] = useState(false)
  const [loading, setLoading] = useState(false)
  const [id, setId] = useState(null)
  const [name, setName] = useState('')

  const { t } = useTranslation()
  const history = useHistory()
  const state = useLocation()?.state

  // ******** GET MAP DATA *********
  useEffect(() => {
    try {
      const { _id } = state
      //   console.log(_id)
      setId(_id)
      axios_init
        .get(`/geozone/${_id}`)
        .then(({ data }) => {
          // console.log(data)
          let _data = JSON.parse(data.shape.data)
          //   console.log(_data)
          setName(data.name)
          setGeometry(_data.geometry)
          setMapState(_data.defaultState)
        })
        .catch((err) => console.log(err))
    } catch (e) {
      history.push('/geofence')
    }
  }, [])

  // *********** START DARWING POLYGON ***********
  const draw = (ref) => {
    ref.editor.startDrawing()
    polygonRef.current = ref
    // console.log(ref.geometry.getCoordinates())

    // ref.editor.events.add("statechange", event => {
    //     console.log(event);
    // });
  }

  // ********** SAVE ALL CHANGES TO THE DATABASE ***********
  const handleSave = () => {
    const { geometry, data } = makeData()

    if (name.length && geometry) {
      setLoading(true)
      const polygon = data.geometry[0].map((val) => val.join(' ')).join()
      axios_init
        .put(`/geozone/${id}`, {
          name,
          polygon,
          shape: {
            data: JSON.stringify(data),
          },
        })
        .then((data) => {
          history.push('/geofence')
          if (data.code === 200) {
            // console.log(data);
          }
        })
        .catch((err) => console.log(err))
        .finally(() => setLoading(false))
    }
  }

  // ************* GET POLYGON DATA AND CHANGE IT POSTIBLE MODE ****************
  const makeData = () => {
    let geometry = polygonRef.current.geometry.getCoordinates()
    let center = findEverage(geometry[0])
    // console.log(geometry)

    const data = {
      defaultState: {
        center,
        zoom: mapRef.current.getZoom(),
        controls: [
          'zoomControl',
          'fullscreenControl',
          'geolocationControl',
          'rulerControl',
          'trafficControl',
          'typeSelector',
        ],
      },
      geometry,
      options: {
        editorDrawingCursor: 'crosshair',
        draggable: true,
        fillColor: '#d61a1a',
        stokeColor: '#255985',
        editorMaxPoints: 1000000,
        strokeWidth: 4,
        opacity: 0.7,
      },
    }

    return { geometry, data }
  }

  // ************ FIND CENTER OF SELECTED POLYGON **************
  const findEverage = (arr) => {
    let sum = arr.reduce((prev, cur) => [prev[0] + cur[0], prev[1] + cur[1]])
    let res = [sum[0] / arr.length, sum[1] / arr.length]

    return res
  }

  const handleClose = () => {
    setVisible(false)
  }

  const handleOpen = () => {
    setVisible(true)
  }

  const handleCancel = () => {
    history.goBack()
  }

  // ************* INITIAL MAP OPTIONS *************
  const options = {
    editorDrawingCursor: 'crosshair',
    draggable: true,
    fillColor: '#d61a1a',
    stokeColor: '#255985',
    editorMaxPoints: 1000,
    strokeWidth: 4,
    opacity: 0.7,
  }

  // ************** BREADCRUMB STATE ************
  const routes = [
    {
      name: 'geofence',
      link: true,
      route: '/geofence',
    },
    {
      name: 'edit',
      link: false,
      // route: '/geofence/create'
    },
  ]

  return (
    <div className='create-map'>
      <BreadCrumbTemplete routes={routes} />

      <Row style={{ backgroundColor: '#fff' }} className='content'>
        <Col span={24}>
          <div className='map-content'>
            <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
              <Map
                instanceRef={(ref) => {
                  if (ref) mapRef.current = ref
                }}
                width='100%'
                height='100%'
                state={mapState}
              >
                <Polygon
                  // onLoad={ymaps => setYmaps(ymaps)}
                  geometry={geometry}
                  options={options}
                  instanceRef={(ref) => ref && draw(ref)}
                />
                {/* <ZoomControl options={{ float: 'right' }} /> */}
              </Map>
            </YMaps>
          </div>
          <Row justify='end' style={{ margin: '17px 0' }}>
            <Col>
              <Button onClick={handleCancel} type='default'>
                {t('cancel')}
              </Button>
              <Button onClick={handleOpen} type='primary' style={{ marginLeft: 8 }}>
                {t('save')}
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={visible}
        title={t('please.enter.name')}
        onCancel={handleClose}
        footer={[
          <Button key='cancel' onClick={handleClose}>
            {t('cancel')}
          </Button>,
          <Button key='submit' type='primary' disabled={!name.length} loading={loading} onClick={handleSave}>
            {t('submit')}
          </Button>,
        ]}
      >
        <Form onFinish={handleSave}>
          <Form.Item>
            <Input placeholder={t('name')} value={name} onChange={(e) => setName(e.target.value)} />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}
