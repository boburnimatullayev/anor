import './style.css'
import React, { useRef, useState, useEffect } from 'react'
import Basic from '@/constants/basic'
import axios from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { useLocation, useHistory } from 'react-router-dom'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { YMaps, Map, ZoomControl, Polygon } from 'react-yandex-maps'
import { Row, Col, Card, Skeleton, Form, Input, Button, Upload, Modal, Typography, Pagination } from 'antd'

export default function CreateMap() {
  const mapRef = useRef(null)
  const polygonRef = useRef(null)

  const [visible, setVisible] = useState(false)
  const [geometry, setGeometry] = useState([[]])
  const [loading, setLoading] = useState(false)
  const [name, setName] = useState('')
  const { t } = useTranslation()

  const history = useHistory()

  // **************** DRAW POLYGON ******************
  const draw = (ref) => {
    ref.editor.startDrawing()
    polygonRef.current = ref
    // console.log(ref.geometry.getCoordinates())

    // ref.editor.events.add("statechange", event => {
    //     console.log(ref.geometry.getCoordinates())
    // });
  }

  // ************** SAVE TO DATABASE *****************
  const handleSave = () => {
    const data = makeData()
    // console.log(geometry)

    if (name.length && geometry) {
      setLoading(true)
      const polygon = data.geometry[0].map((val) => val.join(' ')).join()
      console.log(name, data)
      console.log(polygon)

      axios
        .post('/geozone', {
          name,
          polygon,
          shape: {
            data: JSON.stringify(data),
          },
        })
        .then((data) => {
          history.push('/geofence')
        })
        .catch((err) => console.log(err))
        .finally(() => setLoading(false))
    }
  }

  // ************** GET DATA FROM MAP *************
  const makeData = () => {
    let center = findEverage(geometry[0])
    // let geometry = polygonRef.current.geometry.getCoordinates();

    const data = {
      defaultState: {
        center,
        zoom: mapRef.current.getZoom(),
        controls: [
          'zoomControl',
          'fullscreenControl',
          'geolocationControl',
          'rulerControl',
          'trafficControl',
          'typeSelector',
        ],
      },
      geometry,
      options: {
        editorDrawingCursor: 'crosshair',
        draggable: true,
        fillColor: '#d61a1a',
        stokeColor: '#255985',
        editorMaxPoints: 1000000,
        strokeWidth: 4,
        opacity: 0.7,
      },
    }

    return data
  }

  // ************ FIND CENTER OF SELECTED POLYGON **************
  const findEverage = (arr) => {
    let sum = arr.reduce((prev, cur) => [prev[0] + cur[0], prev[1] + cur[1]])
    let res = [sum[0] / arr.length, sum[1] / arr.length]

    return res
  }

  const handleClose = () => {
    setVisible(false)
  }

  const handleOpen = () => {
    setVisible(true)
  }

  const handleCancel = () => {
    history.goBack()
  }

  // *********** SET SELECTED POLYGON GEOMETRY ***********
  const handleChange = () => {
    setGeometry(polygonRef.current.geometry.getCoordinates())
  }

  // ************** INITIAL MAP STATE **************
  const defaultState = {
    center: [41.311151, 69.279737],
    zoom: 12,
  }

  // ************* INITIAL MAP OPTIONS *************
  const options = {
    editorDrawingCursor: 'crosshair',
    draggable: true,
    fillColor: '#d61a1a',
    stokeColor: '#255985',
    editorMaxPoints: 1000,
    strokeWidth: 4,
    opacity: 0.7,
  }

  // ************** BREADCRUMB STATE ************
  const routes = [
    {
      name: 'geofence',
      link: true,
      route: '/geofence',
    },
    {
      name: 'create',
      link: false,
      // route: '/geofence/create'
    },
  ]

  return (
    <div className='create-map'>
      <BreadCrumbTemplete routes={routes} />

      <Row style={{ backgroundColor: '#fff' }} className='content'>
        <Col span={24}>
          <div className='map-content'>
            <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
              <Map
                instanceRef={(ref) => {
                  if (ref) mapRef.current = ref
                }}
                width='100%'
                height='100%'
                defaultState={defaultState}
              >
                <Polygon
                  // onLoad={ymaps => setYmaps(ymaps)}
                  defaultGeometry={[]}
                  options={options}
                  instanceRef={(ref) => ref && draw(ref)}
                  onGeometryChange={handleChange}
                />
                {/* <ZoomControl options={{ float: 'right' }} /> */}
              </Map>
            </YMaps>
          </div>

          <Row justify='end' style={{ margin: '17px 0' }}>
            <Col>
              <Button onClick={handleCancel} type='default'>
                {t('cancel')}
              </Button>
              <Button onClick={handleOpen} type='primary' style={{ marginLeft: 8 }} disabled={!geometry[0].length}>
                {t('save')}
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>

      <Modal
        visible={visible}
        title={t('please.enter.name')}
        onCancel={handleClose}
        // onOk={this.handleOk}
        footer={[
          <Button key='cancel' onClick={handleClose}>
            {t('cancel')}
          </Button>,
          <Button key='submit' type='primary' disabled={!name.length} loading={loading} onClick={handleSave}>
            {t('submit')}
          </Button>,
        ]}
      >
        <Form onFinish={handleSave}>
          <Form.Item>
            <Input
              placeholder={t('name')}
              ref={(ref) => ref && ref.focus()}
              onChange={(e) => setName(e.target.value)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}
