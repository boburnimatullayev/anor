import "./style.css"
import React from "react"
import { Modal } from "antd"
import { YMaps, Map, Polygon } from 'react-yandex-maps';

export function ViewMap({data, ...props}) {

    return (
        <Modal {...props} className="view-geozone">
            <YMaps query={{ lang: "ru_RU", load: "package.full" }}>
                <Map 
                    // instanceRef={ref => {if(ref) mapRef.current = ref}}
                    width="100%" 
                    height="100%" 
                    state={{...data.defaultState, 
                        // controls: [
                        //     "zoomControl", 
                        //     "fullscreenControl",
                        //     "geolocationControl",
                        //     "rulerControl",
                        //     "trafficControl",
                        //     "typeSelector"
                        // ]
                    }} 
                >
                    <Polygon
                        // onLoad={ymaps => setYmaps(ymaps)}
                        geometry={data.geometry}
                        options={{...data.options, draggable: false}}
                        // instanceRef={ref => ref && draw(ref)}
                    />
                    {/* <ZoomControl options={{ float: 'right' }} /> */}
                </Map>
            </YMaps>
        </Modal>
    )
}