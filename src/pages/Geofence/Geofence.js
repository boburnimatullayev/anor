import "./style.css"
import React, {useState} from "react"
import { Content } from "./Content"
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "../../components/breadcrumb/BreadCrumbTemplete"
import usePermissions from "../../utils/usePermission";
export default function Orders() {
    const { t } = useTranslation() 

    const routes = [
        {
            name: 'geofence',
            link: false,
            // route: '/about'
        }
    ]

    return (
        <div>
            <BreadCrumbTemplete routes={routes} />
            
            <div className="settings-geofence">
                <Content
                update={usePermissions('geozona/update')}
                delete={usePermissions('geozona/delete')}
                />
            </div>
        </div>
    )
}