import './login.less'
import React, { useEffect, useState } from 'react'
import logo from '@/assets/images/logo_white.svg'
import axios_init from '@/utils/axios_init'
import { requests } from '@/redux/requests'
import { useHistory } from 'react-router-dom'
import { useMutation } from 'react-query'
import { useDispatch } from 'react-redux'
import { Form, Input, Button, Alert } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { isLoadingOverlay, setAuthTokens } from '@/redux/actions'
import { setUserPermissions } from '../../redux/actions/authActions'

function Login() {
  const token = localStorage.getItem('token')
  const dispatch = useDispatch()
  const history = useHistory()
  const [notfound, setNotFound] = useState(false)
  const [username, setUsername] = useState('')
  const getPermissions = () => {
    axios_init
      .get(`auth/role/${roleID}`)
      .then(({ data }) => {
        console.log('2222', data)
        dispatch(setUserPermissions(data.permissions))
      })
      .catch((err) => {
        console.log(err)
      })
  }
  useEffect(() => {
    if (token) {
      getPermissions()
    }
  }, [token])

  const [loginSuperAdmin, loginInfo] = useMutation(requests.auth.login, {
    onSuccess: (res) => {
      dispatch(isLoadingOverlay(false))
      dispatch(setAuthTokens(res.data.token.access_token))
      localStorage.setItem('token', res.data.token.access_token)
      localStorage.setItem(
        'user-data',
        JSON.stringify({ name: username, type: 'SUPER-ADMIN' })
      )
      history.push('/')
      // document.location.reload()
    },
    onError: (e) => {
      console.log('error 1', e)
      setNotFound(true)
      dispatch(isLoadingOverlay(false))
    }
  })

  const login = async (values) => {
    try {
      let { data } = await axios_init.post_branch_login(
        '/auth/one2many/login',
        { ...values, data: '' }
      )
      // let {data} = await requests.auth.branchLogin({...values, data: ""})
      let oneMinuteToken = data.token.access_token
      console.log('try-1', oneMinuteToken)

      try {
        let { data } = await axios_init.get(
          `/branch-user-token/${oneMinuteToken}`
        )
        console.log('data =>', data)
        let { branch_id, name, image } = data
        console.log('try-2', data)
        localStorage.setItem('role_id', JSON.stringify(data.role_id))
        localStorage.setItem('branch_id', JSON.stringify(data.branch_id))

        try {
          let { data } = await requests.auth.branchLogin({
            ...values,
            data: branch_id
          })
          console.log('try-3', data, data.token.access_token)
          dispatch(isLoadingOverlay(false))
          dispatch(setAuthTokens(data.token.access_token))
          localStorage.setItem('token', data.token.access_token)
          localStorage.setItem(
            'user-data',
            JSON.stringify({
              branch_id,
              name,
              image,
              type: data.client_type.name
            })
          )
          history.push('/')
          document.location.reload()
        } catch (e) {
          console.log('error 3', e)
          setNotFound(true)
          dispatch(isLoadingOverlay(false))
        }
      } catch (e) {
        console.log('error 2', e)
        setNotFound(true)
        dispatch(isLoadingOverlay(false))
      }
    } catch (e) {
      console.log('superadmin')
      loginSuperAdmin(values)
    }
  }

 
  const onFinish = (values) => {
    setUsername(values?.username)
    dispatch(isLoadingOverlay(true))
    login(values)
    // localStorage.setItem("username", values.username)
  }
  return (
    <div className='login'>
      <div className='logo_content'>
        <img className='logo_image' alt={'Logo'} src={logo} />
      </div>
      <div className='login_content'>
        <h1>Войти в систему</h1>
        {notfound ? (
          <Alert
            style={{ marginBottom: '20px' }}
            message='Логин или пароль неверный !'
            type='error'
          />
        ) : undefined}

        <Form
          name='normal_login'
          className='login-form'
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name='username'
            rules={[
              {
                required: true,
                message: 'Пожалуйста, введите ваше имя пользователя!'
              }
            ]}
          >
            <Input
              size='large'
              prefix={<UserOutlined className='site-form-item-icon' />}
              placeholder='Введите логин'
            />
          </Form.Item>
          <Form.Item
            name='password'
            rules={[
              { required: true, message: 'Пожалуйста, введите свой пароль!' }
            ]}
          >
            <Input.Password
              size='large'
              prefix={<LockOutlined className='site-form-item-icon' />}
              // type='password'
              placeholder='Введите пароль'
            />
          </Form.Item>

          <Form.Item>
            <Button
              type='primary'
              htmlType='submit'
              className='login-form-button'
              size='large'
            >
              Войти в систему
            </Button>
            {/*Or <a href="">register now!</a>*/}
          </Form.Item>
        </Form>
        <p>All Rights Reserved. &copy; Udevs 2021</p>
      </div>
    </div>
  )
}
export default Login

