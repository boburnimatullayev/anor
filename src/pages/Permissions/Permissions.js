import React, { useEffect, useState } from 'react'
import { Card, Switch, Collapse } from 'antd'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { useTranslation } from 'react-i18next'
import './style.css'
import axios_init from '@/utils/axios_init'
import setChecked from './setChecked'
export default function Permissions() {
  const { t } = useTranslation()
  const [data, setData] = useState([])
  const { Panel } = Collapse
  const routes = [
    {
      name: 'permissions',
      link: false,
      route: '/permissions'
    }
  ]

  //GET ALL PERMISSION
  const getAllPermissions = () => {
    axios_init
      .get('auth/nested-permission')
      .then((res) => {
        axios_init
          .get(`auth/role/a1ca1301-4da9-424d-a9e2-578ae6dcde09`)
          .then(({ data }) => {
            setData((els) =>
              res.data?.childs?.map((el, idx) => {
                return setChecked(data, el)
              })
            )
          })
      })
      .catch((err) => console.log(err))
  }

  useEffect(() => {
    getAllPermissions()
  }, [])

  //ON SWICTH CHANGE
  const onSwitchChange = (e, id) => {
    if (e === true) {
      axios_init
        .post('auth/role-permission', {
          permission_id: id,
          role_id: 'a1ca1301-4da9-424d-a9e2-578ae6dcde09'
        })
        .then((res) => {})
        .catch((err) => console.log(err))
    } else {
      axios_init
        .remove('auth/role-permission', {
          permission_id: id,
          role_id: 'a1ca1301-4da9-424d-a9e2-578ae6dcde09'
        })
        .then((res) => {})
        .catch((err) => console.log(err))
    }
  }

  return (
    <div>
      <BreadCrumbTemplete routes={routes} />
      <div>
        <Card>
          <h2>{t('permissions')}</h2>
          <div className='roles-wrapper'>
            <Collapse ghost>
              {data?.map((el, index) => {
                return (
                  <>
                    <Panel
                      collapsible={el.childs ? true : 'disabled'}
                      // showArrow={el.childs ? true : false}
                      header={
                        <div key={index} className='swtich-item'>
                          <h1>{t(el.name)}</h1>
                          <Switch
                            onChange={(e) => onSwitchChange(e, el.id)}
                            defaultChecked={el.checked}
                          />
                        </div>
                      }
                      key={index}
                    >
                      <>
                        <Collapse ghost>
                          {el?.childs?.map((el, index) => {
                            return (
                              <>
                                <Panel
                                  showArrow={el.childs ? true : false}
                                  collapsible={el.childs ? true : 'disabled'}
                                  header={
                                    <div key={index} className='swtich-item'>
                                      <h1>{t(el.name)}</h1>
                                      <Switch
                                        onChange={(e) =>
                                          onSwitchChange(e, el.id)
                                        }
                                        defaultChecked={el.checked}
                                      />
                                    </div>
                                  }
                                  key={index}
                                >
                                  <>
                                    <Collapse ghost>
                                      {el?.childs?.map((el, index) => {
                                        return (
                                          <>
                                            <Panel
                                              showArrow={
                                                el?.childs ? true : false
                                              }
                                              collapsible={
                                                el?.childs ? true : 'disabled'
                                              }
                                              header={
                                                <div
                                                  key={index}
                                                  className='swtich-item'
                                                >
                                                  <h1>{t(el.name)}</h1>
                                                  <Switch
                                                    onChange={(e) =>
                                                      onSwitchChange(e, el.id)
                                                    }
                                                    defaultChecked={el.checked}
                                                  />
                                                </div>
                                              }
                                              key={index}
                                            >
                                              {el.childs ? (
                                                <>
                                                  <Collapse ghost>
                                                    {el?.childs?.map(
                                                      (el, index) => {
                                                        return (
                                                          <>
                                                            <Panel
                                                              showArrow={
                                                                el.childs
                                                                  ? true
                                                                  : false
                                                              }
                                                              collapsible={
                                                                el.childs
                                                                  ? true
                                                                  : 'disabled'
                                                              }
                                                              header={
                                                                <div
                                                                  key={index}
                                                                  className='swtich-item'
                                                                >
                                                                  <h1>
                                                                    {t(el.name)}
                                                                  </h1>
                                                                  <Switch
                                                                    onChange={(
                                                                      e
                                                                    ) =>
                                                                      onSwitchChange(
                                                                        e,
                                                                        el.id
                                                                      )
                                                                    }
                                                                    defaultChecked={
                                                                      el.checked
                                                                    }
                                                                  />
                                                                </div>
                                                              }
                                                              key={index}
                                                            ></Panel>
                                                          </>
                                                        )
                                                      }
                                                    )}
                                                  </Collapse>
                                                </>
                                              ) : (
                                                <>
                                                  <div
                                                    key={index}
                                                    className='swtich-item'
                                                  >
                                                    <h1>{t(el.name)}</h1>
                                                    <Switch
                                                      onChange={(e) =>
                                                        onSwitchChange(e, el.id)
                                                      }
                                                      defaultChecked={
                                                        el.checked
                                                      }
                                                    />
                                                  </div>
                                                </>
                                              )}
                                            </Panel>
                                          </>
                                        )
                                      })}
                                    </Collapse>
                                  </>
                                </Panel>
                              </>
                            )
                          })}
                        </Collapse>
                      </>
                    </Panel>
                  </>
                )
              })}
            </Collapse>
          </div>
        </Card>
      </div>
    </div>
  )
}
