import React from 'react';

export default function setChecked(data, el) {
  // GET IS ACTIVE OR NOT
  const findStatusById = (arr, idToCheck) => {
    const isChecked = arr.filter((el) => el.id === idToCheck);
    return isChecked.length > 0 ? true : false;
  };

  if (!el.childs) {
    return {
      ...el,
      checked: findStatusById(data.permissions, el.id),
      childs: null
    };
  } else if (el.childs) {
    return {
      ...el,
      checked: findStatusById(data.permissions, el.id),
      childs: el?.childs?.map((item) => {
        return setChecked(data, item);
      }),
    };
  }
}
