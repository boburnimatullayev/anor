import './style.css'
import React, { useState, useEffect, useRef } from 'react'
import basic from '@/constants/basic'
import moment from 'moment'
import ImgCrop from 'antd-img-crop'
import axios_init from '@/utils/axios_init'
import { useHistory } from 'react-router-dom'
import { validatePhone } from '@/utils/validatePhone'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { defaultMapState } from '@/constants/basic'
import { YMaps, Map, Polygon, Placemark } from 'react-yandex-maps'
import {
  PlusSquareOutlined,
  LoadingOutlined,
  EyeOutlined,
  DeleteOutlined,
} from '@ant-design/icons'
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  TimePicker,
  Upload,
  Button,
  Select,
  message,
} from 'antd'

const { Option } = Select
const { RangePicker } = TimePicker

export default function CreateBranch() {
  const { t } = useTranslation()
  const history = useHistory()
  const polygonRef = useRef(null)

  const [imgUrl, setImgUrl] = useState('')
  const [loading, setLoading] = useState(false)
  const [uploading, setUploading] = useState(false)
  const [mapList, setMapList] = useState([])
  const [openFileDialog, setOpenFileDialog] = useState(true)
  const [placemarkGeometry, setPlacemarkGeometry] = useState([])
  const [selectedZones, setSelectedZones] = useState('')
  const [multiZone, setMultiZone] = useState({
    defaultState: defaultMapState,
    geometries: [],
    options: {},
  })

  useEffect(() => {
    getMaps()
  }, [])

  const onFinish = (values) => {
    //   console.log(values);
    //   if(!placemarkGeometry.length) {
    //   message.error(t("please.mark.branch.on.geozone"))
    // } else if(!imgUrl.length) {
    //   message.error("Please upload image")
    //   } else {
    setLoading(true)
    // let from_time = moment(values.time_range[0]._d).format('HH:mm:ss')
    // let to_time = moment(values.time_range[1]._d).format('HH:mm:ss')

    let data = {
      ...values,
      phone: '+998' + values.phone,
      geozone_id: selectedZones?.length ? selectedZones.join() : null,
      status: 1,
      image: imgUrl,
      from_time: new Date(`${values.time_range[0]._d} UTC`),
      to_time: new Date(`${values.time_range[1]._d} UTC`),
      location: {
        lat: placemarkGeometry[0],
        long: placemarkGeometry[1],
      },
    }
    //   console.log(data)

    axios_init
      .post('/branch', data)
      .then((data) => {
        message.success(t('saved.successfully'))
        history.push('/branches')
      })
      .catch((err) => {
        console.log(err)
        message.error(t('saving.failed'))
      })
      .finally(() => setLoading(false))
    //   }
  }

  const getMaps = () => {
    axios_init
      .get('/geozone')
      .then(({ data }) => setMapList(data.geozones))
      .catch((err) => console.log(err))
  }

  const onChange = (info) => {
    // setFileList(newFileList);
    if (info.file.status !== 'uploading') {
      //   console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(t('image.uploaded.successfully'))
      //   console.log(info.file)
      setImgUrl(info.file.response.url)
      setUploading(false)
    } else if (info.file.status === 'error') {
      message.error(t('image.uploading.failed'))
      setUploading(false)
    }
  }

  const getMapData = (id) => {
    axios_init
      .get(`/geozones?geozone_ids=${id.length ? id.join() : null}`)
      .then(({ data }) => {
        if (!data.geozones.length) {
          setMultiZone((prev) => ({ ...prev, geometries: [] }))
          return
        }
        const firstData = JSON.parse(data.geozones[0].shape.data)
        setMultiZone({
          geometries: data.geozones.map(
            (geozone) => JSON.parse(geozone.shape.data).geometry[0]
          ),
          options: firstData.options,
          defaultState: firstData.defaultState,
        })
      })
      .catch((err) => console.log(err))
  }
  // console.log(multiZone);

  const setPolygonRef = (ref) => {
    if (ref) {
      polygonRef.current = ref

      ref.events.add('click', (e) => {
        setPlacemarkGeometry(e.get('coords'))
        // console.log(e.get('coords'))
      })
    }
  }

  const routes = [
    {
      name: 'branches',
      route: '/branches',
      link: true,
    },

    {
      name: 'create',
      route: '/branches/create',
      link: false,
    },
  ]

  const configs = {
    name: {
      rules: [
        {
          required: true,
          message: t('please.input.name'),
        },
      ],
    },
    address: {
      rules: [
        {
          required: true,
          message: t('please.input.address'),
        },
      ],
    },
    landmark: {
      rules: [
        {
          required: true,
          message: t('please.input.landmark'),
        },
      ],
    },
    geozone: {
      rules: [
        {
          required: true,
          message: t('please.select.geozone'),
        },
      ],
    },
    phone: {
      rules: [
        {
          required: true,
          message: t('please.input.phone'),
        },
        {
          validator: (_, value) => validatePhone(value),
        },
      ],
    },
    range: {
      rules: [
        {
          type: 'array',
          required: true,
          message: t('please.select.time.range'),
        },
      ],
    },
  }

  return (
    <div className='branches'>
      <BreadCrumbTemplete routes={routes} />

      <div className='create-edit'>
        <Form layout='vertical' onFinish={onFinish}>
          <Card title={t('branches')}>
            <Row style={{ margin: '0 -10px' }}>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='name'
                  label={t('name.branches')}
                  {...configs.name}
                >
                  <Input placeholder={t('enter.name')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='adress'
                  label={t('address')}
                  {...configs.address}
                >
                  <Input placeholder={t('enter.address')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='landmark'
                  label={t('landmark')}
                  {...configs.landmark}
                >
                  <Input placeholder={t('enter.landmark')} />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='geozone_id'
                  label={t('geofence')}
                  {...configs.geozone}
                >
                  <Select
                    loading={!mapList.length}
                    placeholder={t('select.geofence')}
                    mode='multiple'
                    allowClear
                    onChange={(val) => {
                      setSelectedZones(val)
                      getMapData(val)
                    }}
                  >
                    {mapList.length ? (
                      mapList.map(({ name, id }) => (
                        <Option key={id} value={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='phone'
                  label={t('phone.number')}
                  {...configs.phone}
                >
                  <Input
                    placeholder={t('enter.phone.number')}
                    prefix='(+998)'
                  />
                </Form.Item>
              </Col>
              <Col span='8' style={{ padding: '0 10px' }}>
                <Form.Item
                  name='time_range'
                  label={t('time.range')}
                  {...configs.range}
                >
                  <RangePicker
                    allowClear={false}
                    // defaultValue={timeDefVal()}
                    placeholder={[t('from'), t('to')]}
                    format='HH:mm'
                    style={{ width: '100%' }}
                    ranges={{
                      Сегодня: [moment(), moment()],
                      'Эта неделя': [
                        moment().startOf('isoWeek'),
                        moment().endOf('isoWeek'),
                      ],
                      'Этот месяц': [
                        moment().startOf('month'),
                        moment().endOf('month'),
                      ],
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ margin: '0 -10px' }}>
              <Col span='12' style={{ padding: '0 10px' }}>
                <div
                  style={{
                    backgroundColor: 'grey',
                    width: '100%',
                    height: '100%',
                  }}
                >
                  <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
                    <Map
                      // instanceRef={ref => {if(ref) mapRef.current = ref}}
                      width='100%'
                      height='100%'
                      state={multiZone?.defaultState}
                    >
                      {multiZone ? (
                        <Polygon
                          geometry={multiZone.geometries}
                          options={{
                            ...multiZone.options,
                            draggable: false,
                            hasHint: false,
                            fillOpacity: 0.1,
                          }}
                          instanceRef={(ref) => setPolygonRef(ref)}
                        />
                      ) : (
                        <></>
                      )}
                      <Placemark
                        geometry={placemarkGeometry}
                        // instanceRef={ref => ref && setPlacemarkRef(ref)}
                      />
                    </Map>
                  </YMaps>
                </div>
              </Col>
              <Col
                span='12'
                className='img-upload'
                style={{ padding: '0 10px' }}
              >
                <ImgCrop
                  rotate
                  aspect={16 / 9}
                  quality={1}
                  modalOk={t('ok')}
                  modalCancel={t('cancel')}
                  modalTitle={t('edit.image')}
                >
                  <Upload
                    action={`${basic.BASE_URL}/upload/file`}
                    listType='picture-card'
                    onChange={onChange}
                    showUploadList={false}
                    openFileDialogOnClick={openFileDialog}
                    beforeUpload={(file) => {
                      if (
                        file.type === 'image/png' ||
                        file.type === 'image/jpeg' ||
                        file.type === 'image/jpg' ||
                        file.type === 'image/webp'
                      ) {
                        setUploading(true)
                        return true
                      }
                      message.error(t('invalid.image.format'))
                      return false
                    }}
                  >
                    {uploading ? (
                      <LoadingOutlined style={{ fontSize: 30 }} />
                    ) : imgUrl.length ? (
                      <div
                        className='img-content'
                        onMouseEnter={() => setOpenFileDialog(false)}
                        onMouseLeave={() => setOpenFileDialog(true)}
                      >
                        <img alt='uploded img' src={imgUrl} />
                        <div className='img-buttons'>
                          <EyeOutlined
                            style={{ color: '#fff', fontSize: 20 }}
                            onClick={(e) => {
                              window.open(imgUrl, '_blank')
                            }}
                          />
                          <DeleteOutlined
                            style={{
                              color: '#fff',
                              fontSize: 20,
                              marginLeft: 10,
                            }}
                            onClick={() => {
                              setImgUrl('')
                              setOpenFileDialog(true)
                            }}
                          />
                        </div>
                      </div>
                    ) : (
                      <div style={{ color: 'rgba(0, 0, 0, 0.25)' }}>
                        <PlusSquareOutlined style={{ fontSize: '22px' }} />
                        <p style={{ fontSize: '15px' }}>{t('image.upload')}</p>
                      </div>
                    )}
                  </Upload>
                </ImgCrop>
              </Col>
            </Row>
          </Card>
          <Row
            justify='end'
            style={{ paddingTop: '24px', backgroundColor: '#F9F9F9' }}
          >
            <Button
              type='primary'
              htmlType='submit'
              disabled={uploading}
              loading={loading}
            >
              {t('save')}
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  )
}
