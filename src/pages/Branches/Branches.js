import './style.css'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import StyledTag from '@/components/StyledTag/StyledTag'
import axios_init from '../../utils/axios_init'
import StyledButton from '@/components/StyledButton/StyledButton'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../components/breadcrumb/BreadCrumbTemplete'
import { useHistory, Link } from 'react-router-dom'
import { Button, Card, Table, message, Popconfirm, Tag } from 'antd'
import { SearchOutlined, PlusOutlined, EditFilled, DeleteFilled } from '@ant-design/icons'
import usePermissions from '../../utils/usePermission'

export default function Branches() {
  const { t } = useTranslation()
  const history = useHistory()
  const [items, setItems] = useState([])
  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })

  useEffect(() => {
    getData()
  }, [])

  const columns = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
      render: (text) => text,
    },
    {
      title: t('phone.number'),
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: t('status'),
      key: 'status',
      dataIndex: 'status',
      render: (status) => (
        <StyledTag color={status ? 'success' : 'danger'}>{t(status ? 'active' : 'inactive')}</StyledTag>
      ),
    },
    {
      title: t('action'),
      key: 'action',
      align: 'center',
      render: (text, record, index) => (
        <div>
          {
            usePermissions('filial/update') && 
            <StyledButton color='link' icon={EditFilled} onClick={() => editItem(text, record)} tooltip={t('edit')} />
          }
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => deleteItem(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
            {
              usePermissions('filial/delete') &&
              <StyledButton color='danger' icon={DeleteFilled} onClick={() => setVisible(index)} tooltip={t('delete')} />
            }
          </Popconfirm>
        </div>
      ),
    },
  ]

  // ****************EVENTS ****************
  const ExtraButton = function () {
    return (
      <Button
        type='primary'
        icon={<PlusOutlined />}
        onClick={() => {
          history.push('/branches/create')
          // console.log('salom')
        }}
      >
        {t('add')}
      </Button>
    )
  }

  const getData = function (limit = reqQuery.limit, offset = reqQuery.offset) {
    axios_init
      .get(`/branch?limit=${limit}&offset=${offset}`)
      .then((res) => {
        setItems(res.data)
      })
      .finally(() => {})
  }
  const editItem = function (text) {
    history.push({
      pathname: '/branches/edit',
      state: text,
    })
  }
  const deleteItem = function (text) {
    setLoading(true)
    axios_init
      .remove(`/branch/${text.id}`)
      .then((res) => {
        getData(reqQuery.limit, reqQuery.offset)
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
    getData(pageSize, offset)
  }

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const routes = [
    {
      name: 'branches',
      route: '/branches',
      link: false,
    },
  ]

  return (
    <div className='branches'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <Card title={t('branches')} extra={usePermissions('filial/create') ? <ExtraButton /> : <div></div>}>
  
          <Table
            bordered
            pagination={pagination}
            columns={columns}
            dataSource={items.branches}
            rowKey={(record) => record.id}
            scroll={{ x: 100 }}
            locale={{
              filterConfirm: t('ok'),
              filterReset: t('reset'),
              emptyText: t('no.data'),
            }}
          />
        </Card>
      </div>
    </div>
  )
}
