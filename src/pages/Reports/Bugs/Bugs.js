import React, { useEffect, useState } from 'react'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { Table, Input, Image } from 'antd'
import { useTranslation } from 'react-i18next'
import './style.css'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import makeStrFirstLetterUpper from '../../../utils/makeStrFirstLetterUpper'
import CustomDatePicker from '../../../components/DatePicker'

const { Search } = Input

function Bugs() {
  const [loading, setLoading] = useState(false)
  const { t } = useTranslation()

  const [searchedWord, setSearchedWord] = useState('')
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  const [bugReportData, setBugReportData] = useState([])
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/bug-report', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        search: searchedWord,
      })
      .then((res) => {
        setBugReportData(res.data)
      })
      .catch((e) => console.log(e))
      .finally(() => setLoading(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/bug-report-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        search: searchedWord,
        limit: reqQuery.limit,
        offset: reqQuery.offset,
      })
      .then((res) => {
        downloadFile(res.data, t('mobile.representatives'))
      })
      .catch((err) => console.log(err))
      .finally(() => setIsGettingExcel(false))
  }

  useEffect(() => getItems(), [dateRange, searchedWord, dateRange, reqQuery])

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: bugReportData.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const onSearch = (e) => {
    setTimeout(() => {
      setSearchedWord(e.target.value.toLocaleLowerCase())
    }, 500)
  }

  const columns = [
    {
      title: t('created_at'),
      key: 'created_at',
      dataIndex: 'created_at',
      render: (date) => <div>{moment(date).utcOffset(0).format('HH:mm DD-MM-YYYY')}</div>,
    },
    {
      title: t('courier'),
      dataIndex: 'courier_name',
      render: (val) => makeStrFirstLetterUpper(val),
    },
    {
      title: t('brand'),
      dataIndex: 'data',
      render: (val) => makeStrFirstLetterUpper(val.deviceInfo.brand),
    },
    {
      title: t('phone.model'),
      dataIndex: 'data',
      render: (val) => makeStrFirstLetterUpper(val.deviceInfo.model),
    },
    {
      title: t('app.version'),
      dataIndex: 'data',
      render: (val) => (val.deviceInfo.appVersion ? val.deviceInfo.appVersion.split(' ')[0] : ''),
    },
    {
      title: t('screenshot'),
      dataIndex: 'image',
      render: (url) => (
        <Image
          height={90}
          width={90}
          src={url}
          fallback='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
        />
      ),
    },
    {
      title: t('comment'),
      dataIndex: 'message' ?? '',
    },
  ]

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.bugs',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <div className='filters'>
          <div>
            <Search style={{ width: 350, marginRight: 10 }} placeholder={t('type.text')} onChange={onSearch} />
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
          </div>
        </div>
        <Table
          loading={loading}
          columns={columns}
          style={{ marginTop: 16 }}
          dataSource={bugReportData.bug_reports}
          pagination={pagination}
          rowKey={(record) => record.id}
        />
      </div>
    </div>
  )
}

export default Bugs
