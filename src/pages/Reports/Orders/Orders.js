import "./style.css"
import React from "react"
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"

export default function Orders() {
  const { t } = useTranslation()

  
  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false
    },
    {
      name: 'by.orders',
      route: '/catalog/by-orders',
      link: false
    }
  ]
  
  return (
    <div className="reports">
      <div className="by-orders">
        <BreadCrumbTemplete routes={routes}/>

        <div className="content">
          
        </div>
      </div>
    </div>
  )
}