import './style.css'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { Table, Button } from 'antd'
import axios_init from '@/utils/axios_init'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import CustomDatePicker from '../../../components/DatePicker'

export default function Products() {
  const { t } = useTranslation()

  const [items, setItems] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getItems()
  }, [dateRange])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/products', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        status_id:"new"
      })
      .then(({ data }) => {
        setItems(data)
      })
      .finally(() => setLoading(false))
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/products-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        status_id:"new"
      })
      .then((res) => {
        downloadFile(res.data, t('products'))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const columns = [
    {
      title: t('name'),
      key: 'product_name',
      dataIndex: 'product_name',
      sorter: (a, b) => {
        if (a.product_name > b.product_name) return 1
        if (a.product_name < b.product_name) return -1
        return 0
      },
    },
    {
      title: t('total.count'),
      dataIndex: 'total_count',
      key: 'total_count',
      align: 'center',
      sorter: (a, b) => a.total_count - b.total_count,
    },
  ]

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.products',
      route: '/catalog/by-products',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <div className='by-products'>
        <BreadCrumbTemplete routes={routes} />

        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
            <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items}
            pagination={false}
            rowKey={(record) => record.product_id}
          />
        </div>
      </div>
    </div>
  )
}
