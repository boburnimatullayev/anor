import './style.css'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { Table, Button, Input } from 'antd'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import PopoverTitle from '../../../components/MultiSelect/PopoverTitle'
import { useHistory } from 'react-router-dom'
import makeStrFirstLetterUpper from '../../../utils/makeStrFirstLetterUpper'
import CustomDatePicker from '../../../components/DatePicker'

const { Search } = Input

export default function CallsAndNfc() {
  const { t } = useTranslation()
  const history = useHistory()

  const [items, setItems] = useState([])
  // for searching
  const [searchText, setSearchText] = useState('')
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  // for popover title
  const [popoverCouriersFilter, setPopoverCouriersFilter] = useState(false)
  const [choosenCourierFilter, setChoosenCourierFilter] = useState('')
  const [courierList, setCourierList] = useState([])
  const [getValueCourierFilter, setGetValueCourierFilter] = useState(undefined)
  const [searchedWordCourierFilter, setSearchedWordCourierFilter] = useState('')

  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [isFetching, setIsFetching] = useState(true)
  // for date filter
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getItems()
  }, [choosenCourierFilter, searchText, dateRange, reqQuery])

  useEffect(() => {
    getCouriers(searchedWordCourierFilter)
  }, [searchedWordCourierFilter])

  const dataFooter = [
    {
      clientAll: items.courier_checks?.length,
      courierAll: items.courier_checks?.length,
      nfcAll: items.courier_checks?.map((item) => item.nfc).reduce((a, b) => a + b, 0),
      faceIdAll: items.courier_checks?.map((item) => item.face_id).reduce((a, b) => a + b, 0),
      call_beforeAll: items.courier_checks?.map((item) => item.before_delivery).reduce((a, b) => a + b, 0),
      call_afterAll: items.courier_checks?.map((item) => item.after_delivery).reduce((a, b) => a + b, 0),
    },
  ]

  // getting table info
  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/courier-check', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: choosenCourierFilter.length ? choosenCourierFilter.join() : null,
        search: searchText,
      })
      .then(({ data }) => {
        setItems(data)
      })
      .finally(() => setLoading(false))
  }

  // getting couriers list
  const getCouriers = (searchValue = '') => {
    axios_init
      .get('/courier', { search: searchValue })
      .then(({ data }) => {
        let couriers = data.couriers?.map((item, i) => ({
          value: item.id,
          text: item.name,
          key: i,
        }))
        setCourierList(couriers)
      })
      .finally(() => setIsFetching(false))
  }

  // excel download
  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/courier-check-excel', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: moment(dateRange[0]).format('YYYY-MM-DD'),
        to_time: moment(dateRange[1]).format('YYYY-MM-DD'),
        courier_id: choosenCourierFilter.length ? choosenCourierFilter.join() : null,
        search: searchText,
        type: history.location.pathname.includes('nfc') ? 'CHECKS' : 'CALLS',
      })
      .then((res) => {
        downloadFile(res.data, t('mobile.representatives'))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const onSearchChange = (value) => {
    setSearchText(value.toLocaleLowerCase())
  }

  let columns = [
    {
      title: t('No'),
      width: '8%',
      align: 'center',
      render: (record, _, i) => {
        return i + 1
      },
    },
    {
      title: t('clients'),
      dataIndex: 'customer_name',
      width: '25%',
      render: (val) => val && makeStrFirstLetterUpper(val),
    },
    {
      title: t('couriers'),
      width: '25%',
      render: (id) => {
        return courierList.length ? courierList.find((el) => el.value === id.courier_id)?.text ?? t('no.courier') : ''
      },
    },
    history.location.pathname.includes('nfc')
      ? {
          title: t('NFC'),
          dataIndex: 'nfc',
          width: '10%',
          align: 'center',
        }
      : {
          title: t('call.before'),
          dataIndex: 'before_delivery',
          width: '10%',
          align: 'center',
        },
    history.location.pathname.includes('nfc')
      ? {
          title: t('FACE ID'),
          dataIndex: 'face_id',
          width: '10%',
          align: 'center',
        }
      : {
          title: t('call.after'),
          dataIndex: 'after_delivery',
          width: '10%',
          align: 'center',
        },
    {
      title: t('order.comment'),
      key: 'order_comment',
      dataIndex: 'order_comment',
    },
  ]

  const columnFooter = [
    {
      align: 'center',
      width: '8%',
      render: () => {
        return t('general')
      },
    },
    {
      dataIndex: 'clientAll',
      width: '25%',
    },
    {
      dataIndex: 'courierAll',
      width: '25%',
    },
    history.location.pathname.includes('nfc')
      ? {
          dataIndex: 'nfcAll',
          align: 'center',
          width: '10%',
        }
      : {
          dataIndex: 'call_beforeAll',
          width: '10%',
          align: 'center',
        },
    history.location.pathname.includes('nfc')
      ? {
          dataIndex: 'faceIdAll',
          width: '10%',
          align: 'center',
        }
      : {
          dataIndex: 'call_afterAll',
          width: '10%',
          align: 'center',
        },
    {
      dataIndex: '',
      width: '25%',
    },
  ]

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: history.location.pathname.includes('nfc') ? 'by.nfc' : 'by.call',
      route: '/catalog/by-call-and-nfc',
      link: false,
    },
  ]

  function onChange(dates, dateStrings) {
    setDateRange({ from_time: dateStrings[0], to_time: dateStrings[1] })
  }

  return (
    <div className='reports'>
      <div className='by-representatives'>
        <BreadCrumbTemplete routes={routes} />

        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                gap: 4,
              }}
            >
              <Search
                allowClear
                style={{ width: 350, marginRight: 10 }}
                placeholder={`${t('search')}...`}
                onSearch={onSearchChange}
              />
              <div
                style={{
                  minWidth: 200,
                  border: '1px solid #bbb',
                  fontSize: 14,
                  padding: '4px 8px',
                  borderRadius: 5,
                  marginRight: 10,
                }}
              >
                {!isFetching ? (
                  <PopoverTitle
                    title='couriers'
                    popoverVisible={popoverCouriersFilter}
                    setPopoverVisible={setPopoverCouriersFilter}
                    setChosenState={setChoosenCourierFilter}
                    dataList={courierList}
                    getValue={getValueCourierFilter}
                    setGetValue={setGetValueCourierFilter}
                    setSearchedWord={setSearchedWordCourierFilter}
                    topfilter='topfilter'
                  />
                ) : (
                  <></>
                )}
              </div>
              <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
            </div>
            <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            bordered
            tableLayout='fixed'
            dataSource={items.courier_checks}
            pagination={pagination}
            rowKey={(record) => record.order_id + Math.random(10)}
            footer={() => (
              <div>
                <Table
                  rowKey={() => 111111}
                  style={{ backgroundColor: '#ccc' }}
                  columns={columnFooter}
                  dataSource={dataFooter}
                  pagination={false}
                  showHeader={false}
                  bordered
                />
              </div>
            )}
          />
        </div>
      </div>
    </div>
  )
}
