import './style.css';
import React, { useState, useEffect } from 'react';
import moment from 'moment';
import axios_init from '@/utils/axios_init';
import { useTranslation } from 'react-i18next';
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete';
import { Table, Button, Input } from 'antd';
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons';
import PopoverTitle from '../../../components/MultiSelect/PopoverTitle';
import { useHistory } from 'react-router-dom';
import CustomDatePicker from '../../../components/DatePicker';

const { Search } = Input;

export default function CallsAndNfc() {
  const { t } = useTranslation();
  const history = useHistory();

  const [items, setItems] = useState([]);
  // for searching
  const [searchText, setSearchText] = useState('');
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 });
  // for popover title
  //couriers
  const [popoverCouriersFilter, setPopoverCouriersFilter] = useState(false);
  const [choosenCourierFilter, setChoosenCourierFilter] = useState('');
  const [courierList, setCourierList] = useState([]);
  const [searchedWordCourierFilter, setSearchedWordCourierFilter] =
    useState('');
  const [getValueCourierFilter, setGetValueCourierFilter] = useState(undefined);
  //branches
  const [popoverBranchesFilter, setPopoverBranchesFilter] = useState(false);
  const [choosenBrachesFilter, setChoosenBrachesFilter] = useState('');
  const [branchesList, setBranchesList] = useState([]);
  const [searchedWordBranchesFilter, setSearchedWordBranchesFilter] =
    useState('');
  const [getValueBranchesFilter, setValueBrachesFilter] = useState(undefined);

  const [loading, setLoading] = useState(false);
  const [isGettingExcel, setIsGettingExcel] = useState(false);
  const [isFetching, setIsFetching] = useState(true);
  // for date filter
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getCallsReport();
  }, [
    choosenCourierFilter,
    choosenBrachesFilter,
    searchText,
    dateRange,
    reqQuery,
  ]);

  useEffect(() => {
    getCouriers(searchedWordCourierFilter);
  }, [searchedWordCourierFilter]);

  useEffect(() => {
    getBranches(searchedWordBranchesFilter);
  }, [searchedWordBranchesFilter]);

  //GET CALLS REPORTsearchedWordBranchesFilter
  const getCallsReport = () => {
    setLoading(true);
    axios_init
      .get('/report/calls', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: choosenCourierFilter.length
          ? choosenCourierFilter.join()
          : null,
        branch_id: choosenBrachesFilter.length
          ? choosenBrachesFilter.join()
          : '',
        search: searchText,
      })
      .then((res) => {
        setItems(res);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  //GET COURIERS LIST
  const getCouriers = (searchValue = '') => {
    axios_init
      .get('/courier', { search: searchValue })
      .then(({ data }) => {
        let couriers = data.couriers?.map((item, i) => ({
          value: item.id,
          text: item.name,
          key: i,
        }));
        setCourierList(couriers);
      })
      .finally(() => setIsFetching(false));
  };
  //GET BRANCHES LIST
  const getBranches = (searchValue = '') => {
    axios_init
      .get('/branch', { search: searchValue })
      .then(({ data }) => {
        let branches = data.branches?.map((item, i) => ({
          value: item.id,
          text: item.name,
          key: i,
        }));
        setBranchesList(branches);
      })
      .finally(() => setIsFetching(false));
  };
  //DOWNLOAD EXCEL
  const importExcel = () => {
    setIsGettingExcel(true);
    axios_init
      .get('/report/calls/excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: choosenCourierFilter.length
          ? choosenCourierFilter.join()
          : null,
        branch_id: choosenBrachesFilter.length
          ? choosenBrachesFilter.join()
          : null,
        search: searchText,
      })
      .then((res) => {
        window.location.href = res.data.url;
      })
      .finally(() => setIsGettingExcel(false));
  };
  //PAGINATION
  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };
  const pagination = {
    total: items?.data?.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };
  //SEARCH FILTER
  const onSearchChange = (value) => {
    setSearchText(value.toLocaleLowerCase());
  };
  //TABLE COLUMNS
  let columns = [
    {
      title: t('No'),
      width: 70,
      align: 'center',
      children: [
        {
          title: t('count'),
          width: 70,
          align: 'center',
          render: (record, _, i) => {
            return i + 1;
          },
        },
      ],
    },
    {
      title: t('logist'),
      // dataIndex: 'courier_name',
      key: 'courier_name',
      width: 300,
      align: 'center',
      children: [
        {
          title: items?.data?.count ? items?.data?.count : 0,
          align: 'center',
          width: 300,
          dataIndex: 'courier_name',
        },
      ],
    },
    {
      title: `Распределенные заказы на ${moment(dateRange[0]).format(
        'DD-MM-YYYY'
      )} - ${moment(dateRange[1]).format('DD-MM-YYYY')}.`,
      width: 240,
      align: 'center',
      children: [
        {
          width: 240,
          title: items?.data?.orders_count ? items?.data?.orders_count : 0,
          align: 'center',
          dataIndex: 'orders_count',
        },
      ],
    },
    {
      title: `Совершенные звонки 
       ${moment(dateRange[0]).format('DD-MM-YYYY')} - ${moment(
        dateRange[1]
      ).format('DD-MM-YYYY')}
      `,
      width: 200,
      height: 400,
      align: 'center',
      children: [
        {
          width: 200,
          title: items?.data?.calls_count ? items?.data?.calls_count : 0,
          align: 'center',
          dataIndex: 'calls_count',
        },
      ],
    },
    // {
    //   title: `Итог звонка ${moment(dateRange[0]).format(
    //     'DD-MM-YYYY'
    //   )} - ${moment(dateRange[1]).format('DD-MM-YYYY')}`,
    //   align: 'center',
    //   children: [
    {
      title: 'Клиент не поднял телефон',
      align: 'center',
      children: [{ title: items?.data?.dont_pick ? items?.data?.dont_pick : 0, align: 'center', dataIndex: 'dont_pick' }],
    },
    {
      title: 'Клиент сменил дату',
      align: 'center',
      children: [{ title: items?.data?.change_adress ? items?.data?.change_adress : 0, align: 'center', dataIndex: 'change_adress' }],
    },
    {
      title: 'Клиент отказался от продукта',
      align: 'center',
      children: [{ title: items?.data?.cancel_order ? items?.data?.cancel_order : 0, align: 'center', dataIndex: 'cancel_order' }],
    },
    {
      title: 'У клиента нет документов',
      align: 'center',
      children: [{ title: items?.data?.havenot_docs ? items?.data?.havenot_docs : 0, align: 'center', dataIndex: 'havenot_docs' }],
    },
    {
      title: 'Другое',
      align: 'center',
      children: [{ title: items?.data?.other ? items?.data?.other : 0, align: 'center', dataIndex: 'other' }],
    },
    {
      title: 'Общее количество',
      align: 'center',
      children: [{ title: items?.data?.calls_count ? items?.data?.calls_count : 0, align: 'center', dataIndex: 'calls_count' }],
    },
    //   ],
    // },
    {
      title: `Доставлено за ${moment(dateRange[0]).format(
        'DD-MM-YYYY'
      )} - ${moment(dateRange[1]).format('DD-MM-YYYY')}`,
      width: 220,
      align: 'center',
      children: [
        {
          width: 220,
          title: items?.data?.delivered ? items?.data?.delivered : 0,
          align: 'center',
          dataIndex: 'delivered',
        },
      ],
    },
  ];
  //BREADCRMB
  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.call',
      route: '/catalog/by-call-and-nfc',
      link: false,
    },
  ];

  return (
    <div className='reports'>
      <div className='by-representatives'>
        <BreadCrumbTemplete routes={routes} />
        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                gap: 4,
              }}
            >
              <Search
                allowClear
                style={{ width: 350, marginRight: 10 }}
                placeholder={`${t('search')}...`}
                onSearch={onSearchChange}
              />
              <div
                style={{
                  minWidth: 200,
                  border: '1px solid #bbb',
                  fontSize: 14,
                  padding: '4px 8px',
                  borderRadius: 5,
                  marginRight: 10,
                }}
              >
                {!isFetching ? (
                  <>
                    <PopoverTitle
                      title='couriers'
                      popoverVisible={popoverCouriersFilter}
                      setPopoverVisible={setPopoverCouriersFilter}
                      setChosenState={setChoosenCourierFilter}
                      dataList={courierList}
                      getValue={getValueCourierFilter}
                      setGetValue={setGetValueCourierFilter}
                      setSearchedWord={setSearchedWordCourierFilter}
                      topfilter='topfilter'
                    />
                  </>
                ) : (
                  <></>
                )}
              </div>
              <div
                style={{
                  minWidth: 200,
                  border: '1px solid #bbb',
                  fontSize: 14,
                  padding: '4px 8px',
                  borderRadius: 5,
                  marginRight: 10,
                }}
              >
                {!isFetching ? (
                  <PopoverTitle
                    title='branches'
                    popoverVisible={popoverBranchesFilter}
                    setPopoverVisible={setPopoverBranchesFilter}
                    setChosenState={setChoosenBrachesFilter}
                    dataList={branchesList}
                    getValue={getValueBranchesFilter}
                    setGetValue={setValueBrachesFilter}
                    setSearchedWord={setSearchedWordBranchesFilter}
                    topfilter='topfilter'
                  />
                ) : (
                  <></>
                )}
              </div>
              <CustomDatePicker
                finalDate={dateRange}
                setFinalDate={setDateRange}
              />
            </div>
            <Button
              style={{ padding: '0 10px' }}
              type='primary'
              onClick={importExcel}
            >
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            scroll={{ x: 'max-content' }}
            style={{ marginTop: 16 }}
            bordered
            tableLayout='fixed'
            dataSource={items?.data?.calls}
            pagination={pagination}
            rowKey={(record) => record.courier_id + Math.random(10)}
          />
        </div>
      </div>
    </div>
  );
}
