import React, { useEffect, useState } from 'react';
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete';
import { Table, Button } from 'antd';
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import './style.css';
import moment from 'moment';
import axios_init from '@/utils/axios_init';
import makeStrFirstLetterUpper from '../../../utils/makeStrFirstLetterUpper';
import TextFilter from '../../../components/RangeFilter/TextFilter';
import SearchFilter from '../../../components/RangeFilter/SearchFilter';
import CustomDatePicker from '../../../components/DatePicker';
import StyledTag from '@/components/StyledTag/StyledTag';
function Total() {
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation();

  const [isGettingExcel, setIsGettingExcel] = useState(false);
  const [popoverVisibleStatus, setPopoverVisibleStatus] = useState(false);
  const [popoverVisibleBranch, setPopoverVisibleBranch] = useState(false);
  const [popoverVisibleProduct, setPopoverVisibleProduct] = useState(false);
  const [popoverVisibleCourier, setPopoverVisibleCourier] = useState(false);
  const [popoverVisibleCourierComment, setPopoverVisibleCourierComment] =
    useState(false);
  const [popoverVisibleAddress, setPopoverVisibleAddress] = useState(false);
  const [popoverVisibleUid, setPopoverVisibleUid] = useState(false);
  const [popoverVisibleName, setPopoverVisibleName] = useState(false);
  const [popoverVisiblePhone, setPopoverVisiblePhone] = useState(false);

  const [deliveryDayRange, setDeliveryDayRange] = useState('');
  const [chosenStatus, setChosenStatus] = useState('');
  const [uidValue, setUidValue] = useState('');
  const [phoneValue, setPhoneValue] = useState('');
  const [nameValue, setNameValue] = useState('');
  const [chosenProduct, setChosenProduct] = useState('');
  const [chosenBranch, setChosenBranch] = useState('');
  const [chosenCourier, setChosenCourier] = useState('');
  const [chosenCourierComment, setChosenCourierComment] = useState('');
  const [searchedStatusWord, setSearchedStatusWord] = useState('');
  const [searchedCourierWord, setSearchedCourierWord] = useState('');
  const [searchedCourierCommentWord, setSearchedCourierCommentWord] =
    useState('');
  const [searchedBranchWord, setSearchedBranchWord] = useState('');
  const [searchedProductWord, setSearchedProductWord] = useState('');
  const [statusValue, setStatusValue] = useState('');
  const [branchValue, setBranchValue] = useState('');
  const [addressValue, setAddressValue] = useState('');
  const [productValue, setProductValue] = useState('');
  const [courierValue, setCourierValue] = useState('');
  const [courierCommentValue, setCourierCommentValue] = useState('');
  const [desiredDeliveryDayRange, setDesiredDeliveryDayRange] = useState('');
  const [finishedAtRange, setFinishedAtRange] = useState('');

  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 });
  const [totalData, setTotalData] = useState([]);
  const [couriersList, setCouriersList] = useState([]);
  const [statusesList, setStatusesList] = useState([]);
  const [statusesCommentList, setStatusesCommentList] = useState([]);
  const [branchesList, setBranchesList] = useState([]);
  const [productsList, setProductsList] = useState([]);
  const [dateRange, setDateRange] = useState([]);
  const [finishedDate, setFinishedDate] = useState([]);

  const getItems = () => {
    setLoading(true);
    axios_init
      .get('/report/total-report', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        updated_from_time: finishedDate[0],
        updated_to_time: finishedDate[1],
        search_name: nameValue,
        search_id: uidValue,
        search_phone: phoneValue,
        search_address: addressValue,
        comment: chosenCourierComment?.length
          ? chosenCourierComment.includes('Другое')
            ? chosenCourierComment.join().replace('Другое', 'other')
            : chosenCourierComment.join()
          : '',
        status_id: chosenStatus?.length ? chosenStatus.join() : '',
        branch_id: chosenBranch?.length ? chosenBranch.join() : '',
        product_id: chosenProduct?.length ? chosenProduct.join() : '',
        courier_id: chosenCourier?.length ? chosenCourier.join() : '',
        delivered_time_from: deliveryDayRange[0],
        delivered_time_to: deliveryDayRange[1],
        delivery_time_from: desiredDeliveryDayRange[0],
        delivery_time_to: desiredDeliveryDayRange[1],
        finished_time_from: finishedAtRange[0],
        finished_time_to: finishedAtRange[1],
      })
      .then((res) => {
        setTotalData(res.data);
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false));
  };

  // const downloadFile = (url, filename) => {
  //   let element = document.createElement('a')
  //   element.setAttribute('href', url)
  //   element.setAttribute('download', filename)
  //   document.body.appendChild(element)
  //   element.click()
  // }

  let fetchFile = function (url) {
    return fetch(url).then((res) => res.blob());
  };

  let exportFile = function (file) {
    let a = document.createElement('a');
    a.href = URL.createObjectURL(file);
    a.setAttribute('download', '');
    a.click();
  };

  const importExcel = () => {
    setIsGettingExcel(true);
    axios_init
      .get('/report/total-report-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        search_name: nameValue,
        search_id: uidValue,
        search_phone: phoneValue,
        search_address: addressValue,
        offset: reqQuery.offset,
        status_id: chosenStatus?.length ? chosenStatus.join() : '',
        comment: chosenCourierComment?.length
          ? chosenCourierComment.includes('Другое')
            ? chosenCourierComment.join().replace('Другое', 'other')
            : chosenCourierComment.join()
          : '',
        branch_id: chosenBranch?.length ? chosenBranch.join() : '',
        product_id: chosenProduct?.length ? chosenProduct.join() : '',
        courier_id: chosenCourier?.length ? chosenCourier.join() : '',
        delivered_time_from: deliveryDayRange[0],
        delivered_time_to: deliveryDayRange[1],
        delivery_time_from: desiredDeliveryDayRange[0],
        delivery_time_to: desiredDeliveryDayRange[1],
        finished_time_from: finishedAtRange[0],
        finished_time_to: finishedAtRange[1],
      })
      .then((res) => {
        const urls = res?.data;
        for (const url of urls) {
          fetchFile(url).then((file) => exportFile(file));
        }
      })
      .catch((err) => console.log(err))
      .finally(() => setIsGettingExcel(false));
  };

  const getCouriers = () => {
    axios_init
      .get('/courier')
      .then(({ data }) => {
        const allCouriers = data.couriers.map((courier) => {
          return { text: courier.name, value: courier.id };
        });
        setCouriersList(allCouriers);
      })
      .catch((err) => {});
  };

  const getBranches = () => {
    axios_init
      .get('/branch')
      .then(({ data }) => {
        const newBranchList = data.branches.map((elm) => {
          return { text: elm.name, value: elm.id };
        });
        setBranchesList(newBranchList);
      })
      .catch((err) => {});
  };

  const getProducts = () => {
    axios_init
      .get('/product')
      .then(({ data }) => {
        const productsName = data.products.map((product) => {
          return { text: t(product.name), value: product.id };
        });
        setProductsList(productsName);
      })
      .catch((err) => {});
  };

  const getStatuses = () => {
    axios_init
      .get('/order-statuses')
      .then(({ data }) => {
        const allStatuses = data.order_statuses.map((status) => {
          return { text: status.name, value: status.id };
        });
        setStatusesList(allStatuses);
        setStatusesCommentList([
          ...allStatuses.slice(-4),
          {
            text: 'Другое',
            value: 'Другое',
          },
          {
            text: 'Пустое',
            value: 'Пустое',
          },
        ]);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    getCouriers();
    getStatuses();
    getBranches();
    getProducts();
  }, []);

  useEffect(
    () => getItems(),
    [
      dateRange,
      finishedDate,
      reqQuery,
      chosenCourier,
      chosenBranch,
      deliveryDayRange,
      finishedAtRange,
      desiredDeliveryDayRange,
      chosenStatus,
      chosenProduct,
      addressValue,
      nameValue,
      uidValue,
      phoneValue,
      statusesList,
      chosenCourierComment,
    ]
  );

  const onFilterChange = (filters) => {
    if (filters.status_id) {
      setChosenStatus(filters.status_id[0]);
    }
    if (filters.branch_name) {
      setChosenProduct(filters.branch_name[0]);
    }
    if (filters.product_name) {
      setChosenProduct(filters.product_name[0]);
    }
  };

  const onTableChange = (pagination, filters, sorter) => {
    onFilterChange(filters);
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const pagination = {
    total: totalData.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const columns = [
    {
      title: t('status'),
      children: [
        {
          title: TextFilter(
            'status',
            popoverVisibleStatus,
            setPopoverVisibleStatus,
            setChosenStatus,
            statusesList,
            statusValue,
            setStatusValue,
            searchedCourierCommentWord,
            setSearchedCourierCommentWord
          ),
          key: 'status_id',
          dataIndex: 'status',
          width: '5%',
          className: 'column-overflow',
          render: (text, data) => {
            return (
              <StyledTag>
                {!data?.status_id ? t('new') : t(data.status_id)}
              </StyledTag>
            );
          },
        },
        {
          title: (
            <CustomDatePicker
              asFilter={true}
              title='time'
              finalDate={finishedDate}
              setFinalDate={setFinishedDate}
            />
          ),
          dataIndex: 'updated_at',
          width: '5%',
          key: '1',
          render: (date) =>
            date ? moment(date).utcOffset(0).format('HH:mm DD-MM-YYYY') : '-',
        },
      ],
    },
    {
      title: (
        <SearchFilter
          title='UID ID заказа'
          popoverVisible={popoverVisibleUid}
          setPopoverVisible={setPopoverVisibleUid}
          value={uidValue}
          setValue={setUidValue}
        />
      ),
      dataIndex: 'order_id',
      width: '5%',
    },
    {
      title: (
        <SearchFilter
          title='client'
          popoverVisible={popoverVisibleName}
          setPopoverVisible={setPopoverVisibleName}
          value={nameValue}
          setValue={setNameValue}
        />
      ),
      dataIndex: 'customer_name',
      width: '5%',
      render: (val) => makeStrFirstLetterUpper(val),
    },
    {
      title: (
        <SearchFilter
          title='phone.number'
          popoverVisible={popoverVisiblePhone}
          setPopoverVisible={setPopoverVisiblePhone}
          value={phoneValue}
          setValue={setPhoneValue}
        />
      ),
      dataIndex: 'customer_phone',
      render: (val) => (val ? `+${val}` : ''),
      width: '5%',
    },
    {
      title: TextFilter(
        'card.type',
        popoverVisibleProduct,
        setPopoverVisibleProduct,
        setChosenProduct,
        productsList,
        productValue,
        setProductValue,
        searchedProductWord,
        setSearchedProductWord
      ),
      dataIndex: 'product_name',
      width: '5%',
    },
    {
      title: (
        <CustomDatePicker
          title='created_at'
          finalDate={dateRange}
          setFinalDate={setDateRange}
          asFilter={true}
        />
      ),
      key: 'created_at',
      dataIndex: 'created_at',
      render: (date) => <div>{moment(date).format('HH:mm DD-MM-YYYY')}</div>,
      width: '5%',
    },
    {
      title: (
        <CustomDatePicker
          title='desired.delivery.date'
          finalDate={desiredDeliveryDayRange}
          setFinalDate={setDesiredDeliveryDayRange}
          asFilter={true}
        />
      ),
      dataIndex: 'delivery_day',
      width: '5%',
      render: (date) => <div>{moment(date).format('DD-MM-YYYY')}</div>,
    },
    {
      title: TextFilter(
        'branches',
        popoverVisibleBranch,
        setPopoverVisibleBranch,
        setChosenBranch,
        branchesList,
        branchValue,
        setBranchValue,
        searchedBranchWord,
        setSearchedBranchWord
      ),
      dataIndex: 'branch_name',
      width: '5%',
    },
    {
      title: (
        <SearchFilter
          title='delivery.address'
          popoverVisible={popoverVisibleAddress}
          setPopoverVisible={setPopoverVisibleAddress}
          value={addressValue}
          setValue={setAddressValue}
        />
      ),
      dataIndex: 'customer_address',
      width: '5%',
    },
    {
      title: (
        <CustomDatePicker
          title='delivery.date'
          finalDate={deliveryDayRange}
          setFinalDate={setDeliveryDayRange}
          asFilter={true}
        />
      ),
      dataIndex: 'delivered_at',
      width: '5%',
      render: (date) =>
        !date ? '' : <div>{moment(date).format('HH:mm DD-MM-YYYY')}</div>,
    },
    {
      title: (
        <CustomDatePicker
          title='activation.date'
          finalDate={finishedAtRange}
          setFinalDate={setFinishedAtRange}
          asFilter={true}
        />
      ),
      dataIndex: 'finished_at',
      width: '5%',
      render: (val) =>
        val?.at(0) > 0 ? (
          <div>{moment(val).format('HH:mm DD-MM-YYYY')}</div>
        ) : (
          ''
        ),
    },
    // {
    //   title: TextFilter(
    //     'application.status',
    //     popoverVisibleStatus,
    //     setPopoverVisibleStatus,
    //     setChosenStatus,
    //     statusesList,
    //     statusValue,
    //     setStatusValue,
    //     searchedCourierCommentWord,
    //     setSearchedCourierCommentWord
    //   ),
    //   dataIndex: 'status_name',
    //   width: '5%'
    // },
    {
      title: t('supervisor'),
      dataIndex: 'supervisors',
      width: '5%',
      render: (val) =>
        !val
          ? ''
          : val?.map((item) => (
              <p key={item.id}>{makeStrFirstLetterUpper(item.name)}</p>
            )),
    },
    {
      title: TextFilter(
        'courier',
        popoverVisibleCourier,
        setPopoverVisibleCourier,
        setChosenCourier,
        couriersList,
        courierValue,
        setCourierValue,
        searchedCourierWord,
        setSearchedCourierWord
      ),
      dataIndex: 'courier_name',
      render: (val) => makeStrFirstLetterUpper(val),
      width: '5%',
    },
    {
      title: TextFilter(
        'courier.comment',
        popoverVisibleCourierComment,
        setPopoverVisibleCourierComment,
        setChosenCourierComment,
        statusesCommentList,
        courierCommentValue,
        setCourierCommentValue,
        searchedStatusWord,
        setSearchedStatusWord,
        undefined,
        'text'
      ),
      dataIndex: 'courier_comment',
      width: '5%',
    },
    {
      title: t('comment.from.usp.employee'),
      dataIndex: 'supervisor_comment',
      width: '5%',
    },
    {
      title: t('completed.calls.date.time'),
      dataIndex: 'calls_time',
      width: '5%',
      render: (val) =>
        !val ? (
          ''
        ) : (
          <div>
            {val.map((item) => (
              <div>{moment(item).format('HH:mm:ss DD-MM-YYYY')}</div>
            ))}
          </div>
        ),
    },
    {
      title: t('completed.calls.count'),
      dataIndex: 'calls_count',
      width: '5%',
    },
  ];

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'overall',
      link: false,
    },
  ];

  return (
    <div className='reports'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <div className='filters'>
          <Button
            style={{ padding: '0 10px' }}
            type='primary'
            onClick={importExcel}
          >
            {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
          </Button>
        </div>
        <Table
          bordered
          loading={loading}
          columns={columns}
          dataSource={totalData?.total_reports}
          pagination={pagination}
          style={{ marginTop: 16 }}
          onChange={onTableChange}
          rowKey={(record) => record.order_id}
          scroll={{ x: 3000, y: 1300 }}
          locale={{
            filterConfirm: t('ok'),
            filterReset: t('reset'),
            emptyText: t('no.data'),
          }}
        />
      </div>
    </div>
  );
}

export default Total;
