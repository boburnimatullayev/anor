import "./style.css"
import React from "react"
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"

export default function Geofence() {
  const { t } = useTranslation()

  
  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false
    },
    {
      name: 'by.geofence',
      route: '/catalog/by-geofence',
      link: false
    }
  ]
  
  return (
    <div className="reports">
      <div className="by-geofence">
        <BreadCrumbTemplete routes={routes}/>

        <div className="content">
          
        </div>
      </div>
    </div>
  )
}