import React, { useEffect, useState } from 'react'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { Table, DatePicker, Button } from 'antd'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import './style.css'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import CustomDatePicker from '../../../components/DatePicker'
import CustomTimePicker from '../../../components/TimePicker'

const { RangePicker } = DatePicker

function DailyVolume() {
  const [loading, setLoading] = useState(false)
  const { t } = useTranslation()

  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  const [dailyVolumeData, setDailyVolumeData] = useState([])
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])
  const [timeRange, setTimeRange] = useState([moment()])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/daily-volume', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then((res) => {
        setDailyVolumeData(res.data)
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/daily-volume-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        offset: reqQuery.offset,
      })
      .then((res) => {
        downloadFile(res.data, t('mobile.representatives'))
      })
      .catch((err) => console.log(err))
      .finally(() => setIsGettingExcel(false))
  }

  useEffect(() => getItems(), [dateRange, dateRange, reqQuery])

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: dailyVolumeData.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const columns = [
    {
      title: t('date'),
      key: 'created_at',
      dataIndex: 'date',
      render: (date) => <div>{moment(date).utcOffset(0).format('DD-MM-YYYY')}</div>,
      width: '25%',
    },
    {
      title: t('clients.amount.by.delivery.day'),
      dataIndex: 'total_count',
      width: '25%',
    },
    {
      title: t('delivered'),
      dataIndex: 'delivered_count',
      width: '15%',
    },
    {
      title: t('rejected'),
      dataIndex: 'cancelled_count',
      width: '15%',
    },
    {
      title: t('transferred.to.elma'),
      dataIndex: 'transfered_to_elma_count',
      width: '15%',
    },
  ]

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.daily.volume',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <BreadCrumbTemplete routes={routes} />
      <div className='content'>
        <div className='filters'>
          <div>
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
          </div>
          <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
            {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
          </Button>
        </div>
        <Table
          loading={loading}
          columns={columns}
          style={{ marginTop: 16 }}
          dataSource={dailyVolumeData?.daily_volumes}
          pagination={pagination}
          rowKey={(record) => record.date}
        />
      </div>
    </div>
  )
}

export default DailyVolume
