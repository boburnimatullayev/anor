import "./style.css"
import React, { useState, useEffect } from "react"
import moment from "moment"
import axios_init from "@/utils/axios_init"
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"
import { Table, Select, Button, Spin, Tag } from "antd"
import { DownloadOutlined, LoadingOutlined } from "@ant-design/icons"
import CustomDatePicker from "../../../components/DatePicker"

const { Option } = Select

export default function CourierStatuses() {
  const { t } = useTranslation()

  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  const [items, setItems] = useState([])
  const [couriers, setCouriers] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [isFetchingCouriers, setIsFetchingCouriers] = useState(false)
  const [selectedCourier, setSelectedCourier] = useState(undefined)
  const [dateRange, setDateRange] = useState([
    moment().startOf("day"),
    moment().endOf("day"),
  ])

  useEffect(() => {
    getItems()
  }, [dateRange, selectedCourier])

  useEffect(() => {
    getCouriers()
  }, [])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get("/report/courier-statuses", {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: selectedCourier ?? "",
      })
      .then(({ data }) => {
        setItems(data)
      })
      .finally(() => setLoading(false))
  }

  const getCouriers = (searchValue = "") => {
    setIsFetchingCouriers(true)
    axios_init
      .get("/courier", { search: searchValue })
      .then((res) => {
        setCouriers(res.data.couriers)
      })
      .finally(() => setIsFetchingCouriers(false))
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get("/report/courier-statuses-excel", {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0]?.format("LLL"),
        to_time: dateRange[1]?.format("LLL"),
        courier_id: selectedCourier,
      })
      .then((res) => {
        downloadFile(res.data, t("mobile.representatives"))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement("a")
    element.setAttribute("href", url)
    element.setAttribute("download", filename)
    document.body.appendChild(element)
    element.click()
  }

  const onCourierSearch = (value) => {
    getCouriers(value)
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: items?.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const columns = [
    {
      title: t("branch.name"),
      key: "branch_name",
      dataIndex: "branch_name",
    },
    {
      title: t("representative.name"),
      key: "courier_name",
      dataIndex: "courier_name",
    },
    {
      title: t("statuses.count"),
      key: "statuses_count",
      dataIndex: "statuses_count",
      render: (text) =>
        text ? (
          <div style={{ display: "flex", gap: 10, flexWrap: "wrap" }}>
            {text.map((elm, i) =>
              elm.count ? (
                <Tag key={i} style={{ margin: 0 }}>
                  {elm.status_name}{" "}
                  <span style={{ color: "#1890ff" }}>
                    <b>{elm.count}</b>
                  </span>
                </Tag>
              ) : (
                <></>
              )
            )}
          </div>
        ) : (
          ""
        ),
    },
    {
      title: t("total.count"),
      dataIndex: "total_count",
      key: "total_count",
      align: "center",
      sorter: (a, b) => a.total_count - b.total_count,
    },
  ]

  const notFoundContent = (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {isFetchingCouriers ? <Spin size="small" /> : t("no.data")}
    </div>
  )

  const routes = [
    {
      name: "reports",
      route: "/reports",
      link: false,
    },
    {
      name: "by.representative.status",
      link: false,
    },
  ]

  return (
    <div className="reports">
      <div className="by-courier-statuses">
        <BreadCrumbTemplete routes={routes} />

        <div className="content">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div
              style={{
                display: "flex",
              }}
            >
              <CustomDatePicker
                finalDate={dateRange}
                setFinalDate={setDateRange}
              />
              <Select
                allowClear
                showSearch
                style={{ width: 250, marginLeft: "15px" }}
                onSearch={onCourierSearch}
                filterOption={false}
                value={selectedCourier}
                placeholder={t("mob.representatives")}
                onChange={(val) => setSelectedCourier(val)}
                notFoundContent={notFoundContent}
              >
                {couriers && couriers.length ? (
                  couriers.map((val) => (
                    <Option key={val.id} value={val.id}>
                      {val.name}
                    </Option>
                  ))
                ) : (
                  <></>
                )}
              </Select>
            </div>
            <Button
              style={{ padding: "0 10px" }}
              type="primary"
              onClick={importExcel}
            >
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items && items}
            pagination={pagination}
            scroll={{ x: 100 }}
            rowKey={(record) => record.courier_id}
          />
        </div>
      </div>
    </div>
  )
}
