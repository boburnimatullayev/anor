import "./style.css"
import React, { useState, useEffect } from "react"
import moment from "moment"
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"
import { Table, Button, Select } from "antd"
import axios_init from "@/utils/axios_init"
import { DownloadOutlined, LoadingOutlined } from "@ant-design/icons"
import CustomDatePicker from "../../../components/DatePicker"

const statuses = ["new", "assigned", "canc  elled", "finished","delivered"]

export default function Branches() {
  const { t } = useTranslation()
  const { Option } = Select

  const [selectedStatus, setSelectedStatus] = useState("")
  const [items, setItems] = useState([])
  const [columns, setColumns] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [dateRange, setDateRange] = useState([
    moment().startOf("day"),
    moment().endOf("day"),
  ])
  useEffect(() => {
    getItems()
  }, [dateRange, selectedStatus])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get("/report/branches", {
        from_time: dateRange[0],
        to_time: dateRange[1],
        status: selectedStatus,
      })
      .then(({ data }) => {
        // if (data) {
        //   var _data = data.map((item) => {
        //     for (let obj of item.products_count) {
        //       item[obj.product_name] = obj.count
        //     }
        //     return item
        //   })
        //   setColumns(makeColumns(data[0].products_count))
        // }
        setItems(data)
      })
      .finally(() => setLoading(false))
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get("/report/branches-excel", {
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then((res) => {
        downloadFile(res.data, t("branches"))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement("a")
    element.setAttribute("href", url)
    element.setAttribute("download", filename)
    document.body.appendChild(element)
    element.click()
  }

  const makeColumns = (products_count) => {
    let _columns = [
      {
        title: t("first.name"),
        key: "branch_name",
        dataIndex: "branch_name",
      },
      {
        title: t("total.count"),
        dataIndex: "total_count",
        key: "total_count",
        align: "center",
        sorter: (a, b) => a.total_count - b.total_count,
      },
    ]

    if (products_count) {
      return [
        _columns[0],
        ...products_count.map(({ product_name, count }, i) => ({
          title: product_name,
          dataIndex: product_name,
          key: product_name,
          align: "center",
          sorter: (a, b) => a.total_count - b.total_count,
        })),
        _columns[1],
      ]
    }

    return _columns
  }

  const routes = [
    {
      name: "reports",
      route: "/reports",
      link: false,
    },
    {
      name: "by.branches",
      route: "/catalog/by-branches",
      link: false,
    },
  ]

  const columnsStatic = [
    {
      title: t("branch.name"),
      dataIndex: "branch_name",
      key: "branch_name",
    },
    {
      title: t("master_card"),
      dataIndex: "master_card",
      key: "master_card",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("kod_slovo"),
      dataIndex: "kod_slovo",
      key: "kod_slovo",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("humo_mastercard"),
      dataIndex: "humo_mastercard",
      key: "humo_mastercard",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("tria"),
      dataIndex: "tria",
      key: "tria",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("rasrochka"),
      dataIndex: "rasrochka",
      key: "rasrochka",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("perevipusk"),
      dataIndex: "perevipusk",
      key: "perevipusk",
      sorter: (a, b) => a.total_count - b.total_count,
    },
    {
      title: t("total.count"),
      dataIndex: "total_count",
      key: "total_count",
      sorter: (a, b) => a.total_count - b.total_count,
    },
  ]

  return (
    <div className="reports">
      <div className="by-branches">
        <BreadCrumbTemplete routes={routes} />

        <div className="content">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ display: "flex", alignItems: "center" }}>
              <CustomDatePicker
                finalDate={dateRange}
                setFinalDate={setDateRange}
              />
              <Select
                placeholder={t("select.status")}
                allowClear
                showSearch
                style={{ width: 250, marginLeft: "15px" }}
                filterOption={false}
                value={selectedStatus}
                onChange={(val) => setSelectedStatus(val)}
              >
                {statuses?.map((val, i) => (
                  <Option value={val} key={i}>
                    {t(val)}
                  </Option>
                ))}
              </Select>
            </div>
            <Button
              style={{ padding: "0 10px" }}
              type="primary"
              onClick={importExcel}
            >
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columnsStatic}
            style={{ marginTop: 16 }}
            dataSource={items}
            pagination={false}
            scroll={{ x: 100 }}
            rowKey={(record) => record.courier_id}
          />
        </div>
      </div>
    </div>
  )
}
