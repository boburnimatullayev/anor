import React, { useEffect, useState } from 'react'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { Table, Button } from 'antd'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import './style.css'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import CustomDatePicker from '../../../components/DatePicker'

function Distribution() {
  const [loading, setLoading] = useState(false)
  const { t } = useTranslation()

  const [searchedWord, setSearchedWord] = useState('')
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 })
  const [distributionData, setDistributionData] = useState([])
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/daily-assign', {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        search: searchedWord,
      })
      .then((res) => {
        setDistributionData(res.data)
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/daily-assign-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        search: searchedWord,
        offset: reqQuery.offset,
      })
      .then((res) => {
        downloadFile(res.data, t('mobile.representatives'))
      })
      .catch((err) => console.log(err))
      .finally(() => setIsGettingExcel(false))
  }

  useEffect(() => getItems(), [dateRange, searchedWord, dateRange, reqQuery])

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: distributionData.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const onSearch = (e) => {
    setTimeout(() => {
      setSearchedWord(e.target.value.toLocaleLowerCase())
    }, 500)
  }

  const columns = [
    {
      title: t('date'),
      key: 'date',
      dataIndex: 'date',
      render: (date) => <div>{moment(date).utcOffset(0).format('DD-MM-YYYY')}</div>,
      width: '25%',
    },
    {
      title: t('created.applications.count'),
      dataIndex: 'total_count',
    },
    {
      title: t('autodistributed.applications'),
      dataIndex: 'auto_assign_count',
    },
    {
      title: t('undistributed.applications'),
      dataIndex: 'unassigned_count',
    },
    {
      title: t('manual.appointment'),
      dataIndex: 'manual_assign_count',
    },
  ]

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.distribution',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <BreadCrumbTemplete routes={routes} />

      <div className='content'>
        <div className='filters'>
          <div>
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
          </div>
          <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
            {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
          </Button>
        </div>
        <Table
          loading={loading}
          columns={columns}
          style={{ marginTop: 16 }}
          dataSource={distributionData?.daily_assigns?.reverse()}
          pagination={pagination}
          rowKey={(record) => record.date}
        />
      </div>
    </div>
  )
}

export default Distribution
