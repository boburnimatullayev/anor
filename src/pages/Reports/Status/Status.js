import './style.css'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import { Table, Button } from 'antd'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import CustomDatePicker from '../../../components/DatePicker'

export default function Status() {
  const { t } = useTranslation()

  const [items, setItems] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getItems()
  }, [dateRange])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/status-duration', {
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then(({ data }) => {
        setItems(
          data.filter(
            ({ status }) =>
              status !== 'cancelled' || status !== 'cancelled_no_return' || status !== 'cancelled_change_date'
          )
        )
      })
      .finally(() => setLoading(false))
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/status-duration-excel', {
        from_time: dateRange[0]?.format('LLL'),
        to_time: dateRange[1]?.format('LLL'),
      })
      .then((res) => {
        downloadFile(res.data, t('products'))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const columns = [
    {
      title: t('status'),
      key: 'status',
      dataIndex: 'status',
      render: (text) => t(text),
    },
    {
      title: t('duration'),
      dataIndex: 'duration',
      key: 'duration',
      sorter: (a, b) => a.duration - b.duration,
      render: (text) => formatTime(text),
    },
  ]

  function formatTime(sec) {
    const hours = Math.trunc(sec / 3600)
    const minutes = Math.trunc((sec % 3600) / 60)
    const seconds = Math.round((sec % 3600) % 60)
    return `${hours ? hours + ' час. ' : ''}${minutes ? minutes + ' мин. ' : ''}${seconds + ' сек.'}`
  }

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.status',
      route: '/catalog/by-status',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <div className='by-status'>
        <BreadCrumbTemplete routes={routes} />

        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
            <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items}
            pagination={false}
            rowKey={(record) => record.status}
          />
        </div>
      </div>
    </div>
  )
}
