import './style.css'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import axios_init from '@/utils/axios_init'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { DatePicker, Table, Select, Button, Spin, message } from 'antd'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import CustomDatePicker from '../../../components/DatePicker'

const { RangePicker } = DatePicker
const { Option } = Select

export default function Representatives() {
  const { t } = useTranslation()
  const userData = localStorage.getItem('user-data')

  const [items, setItems] = useState([])
  const [columns, setColumns] = useState([])
  const [couriers, setCouriers] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [isFetchingCouriers, setIsFetchingCouriers] = useState(false)
  const [selectedCourier, setSelectedCourier] = useState([])
  const [selectedBranch, setSelectedBranch] = useState([])
  const [branches, setBranches] = useState([])
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getItems()
  }, [dateRange, selectedCourier, selectedBranch])

  useEffect(() => {
    getCouriers()
    getBranches()
  }, [])

  const getItems = () => {
    // console.log(JSON.parse(userData)?.branch_id)
    setLoading(true)
    axios_init
      .get('/report/couriers', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: selectedCourier ?? '',
        branch_id: JSON.parse(userData)?.branch_id || selectedBranch,
        status: 'completed',
      })
      .then(({ data }) => {
        let _data = data?.map((item) => {
          for (let obj of item.products_count) {
            item[obj.product_name] = obj.count
          }
          return item
        })
        setItems(_data)
        setColumns(makeColumns(data && data[0].products_count))
      })
      .finally(() => setLoading(false))
  }

  const getCouriers = (searchValue = '') => {
    setIsFetchingCouriers(true)
    axios_init
      .get('/courier', { search: searchValue })
      .then((res) => {
        setCouriers(res.data.couriers)
      })
      .finally(() => setIsFetchingCouriers(false))
  }

  const getBranches = () => {
    axios_init
      .get('/branch')
      .then((res) => {
        setBranches(res.data.branches.map((elm) => ({ label: elm.name, value: elm.id })))
      })
      .catch((err) => {
        message.error(t('failed.to.fetch'))
      })
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/couriers-excel', {
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: selectedCourier,
        branch: selectedBranch,
        status: 'completed',
      })
      .then((res) => {
        downloadFile(res.data, t('mobile.representatives'))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const onCourierSearch = (value) => {
    getCouriers(value)
  }

  const makeColumns = (products_count) => {
    let _columns = [
      {
        title: t('first.name'),
        key: 'courier_name',
        dataIndex: 'courier_name',
      },
      {
        title: t('branches'),
        key: 'branch_name',
        dataIndex: 'branch_name',
      },
      {
        title: t('total.count'),
        dataIndex: 'total_count',
        key: 'total_count',
        align: 'center',
        sorter: (a, b) => a.total_count - b.total_count,
      },
    ]

    if (products_count) {
      return [
        _columns[0],
        _columns[1],
        ...products_count.map(({ product_name }, i) => ({
          title: product_name,
          dataIndex: product_name,
          key: i,
          align: 'center',
          sorter: (a, b) => a.total_count - b.total_count,
        })),
        _columns[2],
      ]
    }

    return _columns
  }

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.representatives',
      route: '/catalog/by-representatives',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <div className='by-representatives'>
        <BreadCrumbTemplete routes={routes} />

        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div
              style={{
                display: 'flex',
              }}
            >
              <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />
              <Select
                allowClear
                showSearch
                style={{ width: 200 , marginLeft: '15px'}}
                onSearch={onCourierSearch}
                filterOption={false}
                value={selectedCourier}
                placeholder={t('mob.representatives')}
                onChange={(val) => setSelectedCourier(val)}
                notFoundContent={
                  <div
                    style={{
                      width: '100%',
                      height: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    {isFetchingCouriers ? <Spin size='small' /> : t('no.data')}
                  </div>
                }
              >
                {couriers && couriers.length ? (
                  couriers.map((val) => (
                    <Option key={val.id} value={val.id}>
                      {val.name}
                    </Option>
                  ))
                ) : (
                  <></>
                )}
              </Select>
              {!JSON.parse(userData)?.branch_id && (
                <Select
                  allowClear
                  style={{ width: 200 , marginLeft: '15px'}}
                  placeholder={t('branches')}
                  options={branches}
                  value={selectedBranch}
                  onChange={(val) => setSelectedBranch(val)}
                />
              )}
            </div>
            <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items}
            pagination={false}
            scroll={{ x: 100 }}
            rowKey={(record) => record.courier_id}
          />
        </div>
      </div>
    </div>
  )
}
