import "./style.css";
import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import moment from "moment";
import axios_init from "@/utils/axios_init";
import { Table, Select, Button, Spin } from "antd";
import { DownloadOutlined, LoadingOutlined } from "@ant-design/icons";
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete";
import makeStrFirstLetterUpper from "../../../utils/makeStrFirstLetterUpper";
import CustomDatePicker from "../../../components/DatePicker";

const { Option } = Select;

export default function Canceled() {
  const { t } = useTranslation();

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isGettingExcel, setIsGettingExcel] = useState(false);
  const [couriers, setCouriers] = useState([]);
  const [reqQuery, setReqQuery] = useState({ limit: 50, offset: 0 });
  const [isFetchingCouriers, setIsFetchingCouriers] = useState(false);
  const [selectedCourier, setSelectedCourier] = useState("");
  const [selectedOption, setSelectedOption] = useState("");
  const [dateRange, setDateRange] = useState([
    moment().startOf("day"),
    moment().endOf("day"),
  ]);


  useEffect(() => {
    getItems();

  }, [dateRange, selectedCourier, reqQuery,selectedOption ]);

  useEffect(() => {
    getCouriers();
  }, []);

  const getItems = () => {
    setLoading(true);
    axios_init
      .get("/report/cancelled", {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0],
        to_time: dateRange[1],
        courier_id: selectedCourier,
        status:selectedOption,
      })
      .then(({ data }) => {
        setItems(data);
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false));
  };

  const getCouriers = (searchValue = "") => {
    setIsFetchingCouriers(true);
    axios_init
      .get("/courier", { search: searchValue })
      .then((res) => {
        setCouriers(res.data.couriers);
      })
      .finally(() => setIsFetchingCouriers(false));
  };

  const importExcel = () => {
    setIsGettingExcel(true);
    axios_init
      .get("/report/cancelled-excel", {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
        from_time: dateRange[0]?.format("LLL"),
        to_time: dateRange[1]?.format("LLL"),
        courier_id: selectedCourier,
      })
      .then((res) => {
        downloadFile(res.data, t("canceled"));
      })
      .finally(() => setIsGettingExcel(false));
  };

  const downloadFile = (url, filename) => {
    let element = document.createElement("a");
    element.setAttribute("href", url);
    element.setAttribute("download", filename);
    document.body.appendChild(element);
    element.click();
  };

  const onSearch = (value) => {
    getCouriers(value);
  };

  const columns = [
    {
      title: t("name"),
      key: "product_name",
      dataIndex: "product_name",
    },
    {
      title: t("mob.representatives"),
      key: "courier",
      dataIndex: "courier",
    },
    {
      title: t("address"),
      key: "address",
      dataIndex: "address",
    },
    {
      title: t("customer"),
      key: "customer_name",
      dataIndex: "customer_name",
    },
    {
      title: t("phone.number"),
      key: "customer_phone",
      dataIndex: "customer_phone",
    },
    {
      title: t("comment"),
      dataIndex: "comment",
      key: "comment",
    },
  ];

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 50,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const routes = [
    {
      name: "reports",
      route: "/reports",
      link: false,
    },
    {
      name: "by.canceled",
      route: "/catalog/by-canceled",
      link: false,
    },
  ];

  return (
    <div className="reports">
      <div className="by-canceled">
        <BreadCrumbTemplete routes={routes} />

        <div className="content">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ display: "flex" }}>
              <CustomDatePicker
                finalDate={dateRange}
                setFinalDate={setDateRange}
              />
              <Select
                allowClear
                showSearch
                style={{ width: 250, marginLeft: "15px" }}
                onSearch={onSearch}
                filterOption={false}
                value={selectedCourier}
                placeholder={t("mob.representatives")}
                onChange={(val) => setSelectedCourier(val)}
                notFoundContent={
                  <div
                    style={{
                      width: "100%",
                      height: "100%",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    {isFetchingCouriers ? <Spin size="small" /> : t("no.data")}
                  </div>
                }
              >
                {couriers && couriers.length ? (
                  couriers.map((val) => (
                    <Option key={val.id} value={val.id}>
                      {val.name}
                    </Option>
                  ))
                ) : (
                  <></>
                )}
              </Select>

              <Select
                 allowClear
                showSearch
                style={{ width: 250, marginLeft: "15px" }}
                filterOption={false}
                value={selectedOption}
                placeholder={"status"}
                onChange={(val) => setSelectedOption(val)}
                notFoundContent={
                  <div
                    style={{
                      width: "100%",
                      height: "100%",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  ></div>
                }
              >
                <Option value="cancelled">Отменено</Option>
                <Option value="cancel">Отмено</Option>
              </Select>
            </div>
            <Button
              style={{ padding: "0 10px" }}
              type="primary"
              onClick={importExcel}
            >
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items.data}
            pagination={pagination}
            rowKey={(record) => record.order_id}
          />
        </div>
      </div>
    </div>
  );
}
