import './style.css'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { Table, Button } from 'antd'
import axios_init from '@/utils/axios_init'
import { DownloadOutlined, LoadingOutlined } from '@ant-design/icons'
import CustomDatePicker from '../../../components/DatePicker'

export default function CancelledReason() {
  const { t } = useTranslation()

  const [items, setItems] = useState([])
  const [columns, setColumns] = useState([])
  const [loading, setLoading] = useState(false)
  const [isGettingExcel, setIsGettingExcel] = useState(false)
  const [dateRange, setDateRange] = useState([
    moment().startOf('day'),
    moment().endOf('day')
  ])

  useEffect(() => {
    getItems()
  }, [dateRange])

  const getItems = () => {
    setLoading(true)
    axios_init
      .get('/report/cancelled-reason', {
        from_time: dateRange[0],
        to_time: dateRange[1],
      })
      .then(({ data }) => {
        if (data) {
          var _data = data.map((item) => {
            for (let obj of item.reason_count) {
              item[obj.reason_name] = obj.count
            }
            return item
          })
          setColumns(makeColumns(data[0].reason_count))
        }
        setItems(_data)
      })
      .finally(() => setLoading(false))
  }

  const importExcel = () => {
    setIsGettingExcel(true)
    axios_init
      .get('/report/cancelled-reason-excel', {
        from_time: dateRange[0]?.format('LLL'),
        to_time: dateRange[1]?.format('LLL'),
      })
      .then((res) => {
        downloadFile(res.data, t('branches'))
      })
      .finally(() => setIsGettingExcel(false))
  }

  const downloadFile = (url, filename) => {
    let element = document.createElement('a')
    element.setAttribute('href', url)
    element.setAttribute('download', filename)
    document.body.appendChild(element)
    element.click()
  }

  const makeColumns = (products_count) => {
    let _columns = [
      {
        title: t('first.name'),
        key: 'product_name',
        dataIndex: 'product_name',
      },
      {
        title: t('total.count'),
        dataIndex: 'total_count',
        key: 'total_count',
        align: 'center',
        sorter: (a, b) => a.total_count - b.total_count,
      },
    ]

    if (products_count) {
      return [
        _columns[0],
        ...products_count.map(({ reason_name, count }, i) => ({
          title: reason_name,
          dataIndex: reason_name,
          key: reason_name,
          align: 'center',
          sorter: (a, b) => a.total_count - b.total_count,
        })),
        _columns[1],
      ]
    }

    return _columns
  }

  const routes = [
    {
      name: 'reports',
      route: '/reports',
      link: false,
    },
    {
      name: 'by.branches',
      route: '/catalog/by-branches',
      link: false,
    },
  ]

  return (
    <div className='reports'>
      <div className='by-branches'>
        <BreadCrumbTemplete routes={routes} />

        <div className='content'>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <CustomDatePicker finalDate={dateRange} setFinalDate={setDateRange} />

            <Button style={{ padding: '0 10px' }} type='primary' onClick={importExcel}>
              {isGettingExcel ? <LoadingOutlined /> : <DownloadOutlined />}
            </Button>
          </div>
          <Table
            loading={loading}
            columns={columns}
            style={{ marginTop: 16 }}
            dataSource={items}
            pagination={false}
            scroll={{ x: 100 }}
            rowKey={(record) => record.courier_id}
          />
        </div>
      </div>
    </div>
  )
}
