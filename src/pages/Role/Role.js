import "./style.css";
import React, { useEffect, useState } from "react";
import axios_init from "@/utils/axios_init";
import StyledButton from "@/components/StyledButton/StyledButton";
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "../../components/breadcrumb/BreadCrumbTemplete";
import { useHistory, useLocation } from "react-router-dom";
import { EditFilled, DeleteFilled, PlusOutlined } from "@ant-design/icons";
import {
  Card,
  Button,
  Form,
  Table,
  Popconfirm,
  Input,
  message,
  Modal,
} from "antd";
import usePermission from "@/utils/usePermission";
const { Search } = Input;

export default function Role() {
  const { t } = useTranslation();
  const history = useHistory();

  const [inputModal, setInputModal] = useState("");
  const [visible, setVisible] = useState(null);
  const [loading, setLoading] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 });
  const [modalVisible, setModalVisible] = useState(false);
  const [roleData, setRoleData] = useState([]);

  const ExtraButton = function () {
    return (
      <Button
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => modalOnClick()}
      >
        {t("add")}
      </Button>
    );
  };

  const getList = () => {
    setLoading(true);
    axios_init
      .get("/auth/role")
      .then((res) => setRoleData(res.data?.roles?.reverse()))
      .finally(() => setLoading(false));
  };

  const onSearchChange = (text = "") => {
    const searchItem = roleData.filter((item) =>
      item?.name?.toLowerCase().includes(text.toLowerCase())
    );
    setItems(searchItem);
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const modalOnClick = (record, type) => {
    setModalVisible(true);
  };

  const modalHideClick = (record, type) => {
    setModalVisible(false);
  };
  const handleDelete = (val) => {
    setLoading(true);
    axios_init
      .remove(`/auth/role/${val}`)
      .then(() => {
        message.success(t("deleted.successfully"));
      })
      .catch((err) => {
        console.log(err);
        message.error(t("deleting.failed"));
      })
      .finally(() => {
        getList();
        setLoading(false);
        setVisible(null);
      });
  };

  const columns = [
    {
      title: t("name"),
      dataIndex: "name",
      key: "name",
      render: (name, record) => {
        return <a className="employee-link">{name}</a>;
      },
    },
    {
      title: t("actions"),
      key: "actions",
      align: "center",
      render: (text, record, index) => (
        <div>
          {usePermission("superviser/update") && (
            <StyledButton
              color="link"
              icon={EditFilled}
              tooltip={t("edit_role")}
              onClick={() => {
                history.push(`/role/edit/${record.id}`);
              }}
            />
          )}
          <Popconfirm
            title={t("do.you.really.want.to.delete")}
            visible={visible === index}
            onConfirm={() => handleDelete(record?.id)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t("cancel")}
            okText={t("yes")}
          >
            {usePermission("superviser/delete") && (
              <StyledButton
                onClick={() => setVisible(index)}
                color="danger"
                icon={DeleteFilled}
                tooltip={t("delete")}
              />
            )}
          </Popconfirm>
        </div>
      ),
    },
  ];

  const routes = [
    {
      name: "Role",
      link: false,
      route: "/role",
    },
  ];
  const warning = () => {
    message.warning(t("please.input.title"));
  };
  const submitModal = () => {
    if (inputModal.length > 0) {
      axios_init
        .post("/auth/role", {
          client_type_id: "5a3818a9-90f0-44e9-a053-3be0ba1e2c09",
          name: inputModal,
        })
        .finally(() => {
          setModalVisible(false);
          setInputModal("");
          getList();
        });
      // history.push("/role/create");
    } else {
      warning();
    }
  };
  useEffect(() => {
    setItems(roleData);
  }, [roleData]);

  useEffect(() => {
    getList();
  }, []);
  return (
    <div className="branch-user">
      <Modal
        title={t("please.confirm")}
        visible={modalVisible}
        onOk={submitModal}
        onCancel={modalHideClick}
        okText={t("submit")}
        cancelText={t("cancel")}
      >
        <Form>
          <Form.Item
            name="role_name"
            label={t("name")}
            // {...configs.password}
          >
            <Input
              onChange={(e) => setInputModal(e.target.value)}
              placeholder={t("enter.name")}
            />
          </Form.Item>
        </Form>
      </Modal>
      <BreadCrumbTemplete routes={routes} />

      <div className="content">
        <Card
          title={
            <Search
              allowClear
              style={{ width: 350, paddingLeft: "4px" }}
              placeholder={`${t("search")}...`}
              onSearch={onSearchChange}
              onChange={(e) => {
                const value = e.target.value;
                if (value.length === 0) onSearchChange(value);
              }}
            />
          }
          extra={<ExtraButton />}
        >
          <Table
            bordered
            columns={columns}
            loading={tableLoading}
            dataSource={items}
            pagination={pagination}
            rowKey={(record) => record.id}
          />
        </Card>
      </div>
    </div>
  );
}
