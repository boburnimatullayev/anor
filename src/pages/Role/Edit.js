import React, { useEffect, useState } from "react";
import {
  Card,
  Switch,
  Collapse,
  Input,
  Form,
  Row,
  Button,
  message,
  Radio,
} from "antd";
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete";
import { useTranslation } from "react-i18next";
import "./style-create.css";
import axios_init from "@/utils/axios_init";
import setChecked from "./setChecked";
import { useParams } from "react-router-dom";
import { SyncOutlined, CheckCircleTwoTone } from "@ant-design/icons";
let debounce = setTimeout(() => {}, 0);
export default function Edit() {
  const params = useParams();
  const { t } = useTranslation();
  const [data, setData] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [loading, setLoading] = useState(false);
  const [radioBtnValue, setRadioBtnValue] = useState({
    [`dashboard`]: false,
    [`order`]: false,
  });
  const { Panel } = Collapse;
  const routes = [
    {
      name: "permissions",
      link: false,
      route: "/permissions",
    },
  ];

  const isBranchCheck = (elm) => {
    switch (elm.name) {
      case "dashboard":
        setRadioBtnValue((prev) => ({
          ...prev,
          [`dashboard`]: elm.is_my_branch,
        }));
        break;
      case "orders":
        setRadioBtnValue((prev) => ({
          ...prev,
          [`order`]: elm.is_my_branch,
        }));
        break;
      default:
        break;
    }
  };
  //GET ALL PERMISSION
  const getAllPermissions = () => {
    axios_init
      .get("auth/nested-permission")
      .then((res) => {
        axios_init.get(`auth/role/${params.id}`).then(({ data }) => {
          console.log(`perd`, res);
          setInputValue(data.name);
          data?.permissions?.map((item) => {
            isBranchCheck(item);
          });
          setData((els) =>
            res.data?.childs?.map((el, idx) => {
              return setChecked(data, el);
            })
          );
        });
      })
      .catch((err) => console.log(err));
  };
  const roleNameUpdate = (name, id, client_type_id) => {
    axios_init
      .put("auth/role", {
        client_type_id: "5a3818a9-90f0-44e9-a053-3be0ba1e2c01",
        id: id,
        name: name,
      })
      .finally(() => setLoading(false));
  };
  const onChange = (e) => {
    const value = e.target.value;
    setLoading(true);
    clearTimeout(debounce);
    setInputValue(value);
    if (value.length > 0) {
      debounce = setTimeout(() => {
        roleNameUpdate(value, params.id);
      }, 500);
    } else {
      warning();
    }
  };
  //ON SWICTH CHANGE
  const onSwitchChange = (e, id,) => {
      if (e === true) {
        axios_init
          .post("auth/role-permission", {
            permission_id: id,
            role_id: params.id,
            is_all_branch: e,
          })
          .catch((err) => console.log(err));
      } else {
        axios_init
          .remove("auth/role-permission", {
            role_id: params.id,
            permission_id: id,
            is_all_branch: e,
          })
          .catch((err) => console.log(err));
      }
  };

  const warning = () => {
    message.warning(t("please.input.title"));
  };

  const onChangeRadioBtn = (value, name, elm) => {
    const boolValue = value === 1 ? true : false;
    setRadioBtnValue((prev) => ({ ...prev, [`${name}`]: boolValue }));
    onSwitchChange(boolValue, elm.id);
  };

  const showRadioBtn = (elm) => {
    switch (elm.name) {
      case "dashboard":
        return (
          <Radio.Group
            key={elm.id}
            onClick={(event) => event.stopPropagation()}
            style={{ display: "flex", marginLeft: 15 }}
            onChange={(e) => onChangeRadioBtn(e.target.value, "dashboard", elm)}
            value={radioBtnValue["dashboard"] ? 1 : 0}
          >
            <Radio value={1}>свой филиал</Radio>
            <Radio value={0}>все филиал </Radio>
          </Radio.Group>
        );
      case "orders":
        return (
          <Radio.Group
            key={elm.id}
            onClick={(event) => event.stopPropagation()}
            style={{ display: "flex", marginLeft: 15 }}
            onChange={(e) => onChangeRadioBtn(e.target.value, "order", elm)}
            value={radioBtnValue["order"] ? 1 : 0}
          >
            <Radio value={1}>свой филиал</Radio>
            <Radio value={0}>все филиал </Radio>
          </Radio.Group>
        );

      default:
        return "";
    }
  };

  useEffect(() => {
    getAllPermissions();
  }, []);
  return (
    <div>
      <BreadCrumbTemplete routes={routes} />
      <div>
        <Card
          title={
            <Form>
              <Form.Item
                label={t("name")}
                // {...configs.password}
              >
                <Input
                  type="text"
                  value={inputValue}
                  onChange={onChange}
                  placeholder={t("enter.name")}
                  suffix={
                    loading ? (
                      <SyncOutlined spin />
                    ) : (
                      <CheckCircleTwoTone twoToneColor="#52c41a" />
                    )
                  }
                />
              </Form.Item>
            </Form>
          }
        >
          <h2>{t("permissions")}</h2>
          <div className="roles-wrapper">
            <Collapse ghost>
              {data?.map((el, index) => {
                if (el?.name !== "all branch") {
                  return (
                    <>
                      {console.log("elname", el)}
                      <Panel
                        collapsible={
                          el.childs || el.name === "dashboard"
                            ? true
                            : "disabled"
                        }
                        // showArrow={ true }
                        header={
                          <>
                            <div key={index} className="swtich-item">
                              <h1>{t(el.name)}</h1>
                              <Switch
                                onClick={(event) => event.stopPropagation()}
                                onChange={(e) => onSwitchChange(e, el.id)}
                                defaultChecked={el.checked}
                              />
                            </div>

                            {/* {showRadioBtn(el, index)} */}
                          </>
                        }
                        key={index}
                      >
                        <>
                          <Collapse ghost>
                            {/* {el?.name === 'dashboard' || el?.name === 'orders' ? <>
                         
                           <Panel
                             showArrow={ false}
                             collapsible={ "disabled"}
                              header={
                                <div key={index} className="swtich-item">
                                  <h1>Свой филиал / Все филиал</h1>
                                  <Switch
                                    onChange={(e) =>  onSwitchChange(e, el.id,'all')}
                                    defaultChecked={el.checked}
                                  />
                                </div>
                              }
                            ></Panel>
                           </>: <></>} */}
                            {el?.childs?.map((el, index) => {
                              return (
                                <>
                                  <Panel
                                    showArrow={el.childs ? true : false}
                                    collapsible={el.childs ? true : "disabled"}
                                    header={
                                      <div key={index} className="swtich-item">
                                        <h1>
                                          {el?.name ===
                                            "all branch dashboard" ||
                                          el?.name === "all branch order"
                                            ? "Свой филиал / Все филиал"
                                            : t(el.name)}
                                        </h1>
                                        <Switch
                                          onChange={(e) =>
                                            onSwitchChange(e, el.id)
                                          }
                                          defaultChecked={el.checked}
                                        />
                                      </div>
                                    }
                                    key={index}
                                  >
                                    <>
                                      <Collapse ghost>
                                        {el?.childs?.map((el, index) => {
                                          return (
                                            <>
                                              <Panel
                                                showArrow={
                                                  el?.childs ? true : false
                                                }
                                                collapsible={
                                                  el?.childs ? true : "disabled"
                                                }
                                                header={
                                                  <div
                                                    key={index}
                                                    className="swtich-item"
                                                  >
                                                    <h1>{t(el.name)}</h1>
                                                    <Switch
                                                      onChange={(e) =>
                                                        onSwitchChange(e, el.id)
                                                      }
                                                      defaultChecked={
                                                        el.checked
                                                      }
                                                    />
                                                  </div>
                                                }
                                                key={index}
                                              >
                                                {el.childs ? (
                                                  <>
                                                    <Collapse ghost>
                                                      {el?.childs?.map(
                                                        (el, index) => {
                                                          return (
                                                            <>
                                                              <Panel
                                                                showArrow={
                                                                  el.childs
                                                                    ? true
                                                                    : false
                                                                }
                                                                collapsible={
                                                                  el.childs
                                                                    ? true
                                                                    : "disabled"
                                                                }
                                                                header={
                                                                  <div
                                                                    key={index}
                                                                    className="swtich-item"
                                                                  >
                                                                    <h1>
                                                                      {t(
                                                                        el.name
                                                                      )}
                                                                    </h1>
                                                                    <Switch
                                                                      onChange={(
                                                                        e
                                                                      ) =>
                                                                        onSwitchChange(
                                                                          e,
                                                                          el.id
                                                                        )
                                                                      }
                                                                      defaultChecked={
                                                                        el.checked
                                                                      }
                                                                    />
                                                                  </div>
                                                                }
                                                                key={index}
                                                              ></Panel>
                                                            </>
                                                          );
                                                        }
                                                      )}
                                                    </Collapse>
                                                  </>
                                                ) : (
                                                  <>
                                                    <div
                                                      key={index}
                                                      className="swtich-item"
                                                    >
                                                      <h1>{t(el.name)}</h1>
                                                      <Switch
                                                        onChange={(e) =>
                                                          onSwitchChange(
                                                            e,
                                                            el.id
                                                          )
                                                        }
                                                        defaultChecked={
                                                          el.checked
                                                        }
                                                      />
                                                    </div>
                                                  </>
                                                )}
                                              </Panel>
                                            </>
                                          );
                                        })}
                                      </Collapse>
                                    </>
                                  </Panel>
                                </>
                              );
                            })}
                          </Collapse>
                        </>
                      </Panel>
                    </>
                  );
                }
              })}
            </Collapse>
          </div>
        </Card>
        {/* <Row justify="end" style={{ marginTop: 24 }}>
          <Button type="primary">{t("save")}</Button>
        </Row> */}
      </div>
    </div>
  );
}
