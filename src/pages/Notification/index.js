import "./style.css"
import React, { useEffect, useState } from "react"
import axios_init from "@/utils/axios_init"
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete"
import StyledButton from "@/components/StyledButton/StyledButton"

import { useHistory } from "react-router-dom"
import {
  PlusOutlined,
  DeleteFilled,
  EyeOutlined,
  EyeInvisibleOutlined,
} from "@ant-design/icons"
import { Card, Button, Table, Popconfirm } from "antd"
import moment from "moment"
import usePermissions from "../../utils/usePermission"

export default function Notification() {
  const { t } = useTranslation()
  const history = useHistory()
  const [visible, setVisible] = useState(null)
  const [visibleView, setVisibleView] = useState(false)

  const [tableLoading, setTableLoading] = useState(false)
  const [loading, setLoading] = useState(false)

  const [notifications, setNotifications] = useState([])
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })

  useEffect(() => {
    getNotifications()
  }, [])

  const getNotifications = () => {
    setTableLoading(true)
    axios_init
      .get("notification")
      .then((res) => {
        const items = []
        res.data?.notifications?.forEach((n) => {
          items.push({
            id: n.id,
            created_at: n.created_at,
            name: n.title,
            description: n.body,
          })
        })
        setNotifications(items.reverse())
      })
      .catch((err) => console.log(err))
      .finally(() => setTableLoading(false))
  }

  const ExtraButton = function () {
    return (
      <Button
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => {
          history.push("/notification/create")
        }}
      >
        {t("add")}
      </Button>
    )
  }
  const handleView = (text, index) => {
    setLoading(true)
    axios_init
      .put(`/notification`, {
        status: text?.status ? "false" : "true",
        ...text,
      })
      .then((res) => {})
      .catch((err) => {})
      .finally(() => setLoading(false))
  }

  const handleDelete = (val) => {
    setLoading(true)
    axios_init
      .remove(`/notification/${val.id}`)
      .then((res) => {
        getItems()
        message.success(t("deleted.successfully"))
      })
      .catch((err) => {
        console.log(err)
        message.error(t("deleting.failed"))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
  }

  const pagination = {
    total: notifications.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  const columns = [
    {
      title: t("name"),
      dataIndex: "name",
      key: "name",
    },
    {
      title: t("created_at"),
      key: "created_at",
      dataIndex: "created_at",
      render: (date) => (
        <div>{moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
      ),
    },
    {
      title: t("description"),
      dataIndex: "description",
      key: "description",
    },
    {
      title: t("action"),
      key: "action",
      align: "center",
      fixed: "right",
      width: "200px",
      render: (text, record, index) => {
        return (
          <div>
            <StyledButton
              color="view"
              icon={text?.status ? EyeOutlined : EyeInvisibleOutlined}
              onClick={() => handleView(text, index)}
              tooltip={text?.status ? "скрыть" : "показать"}
            />
            <StyledButton
              color="danger"
              icon={DeleteFilled}
              onClick={() => handleDelete(text)}
              tooltip={t("delete.order")}
            />
          </div>
        )
      },
    },
  ]

  const routes = [
    {
      name: "notification",
      link: false,
      route: "/branch-user",
    },
  ]
  return (
    <div className="branch-user">
      <BreadCrumbTemplete routes={routes} />

      <div className="content">
        <Card
          extra={
            usePermissions("notification/create") ? (
              <ExtraButton />
            ) : (
              <div></div>
            )
          }
        >
          <Table
            bordered
            columns={columns}
            loading={tableLoading}
            dataSource={notifications}
            pagination={pagination}
            rowKey={(record) => record.id}
          />
        </Card>
      </div>
    </div>
  )
}
