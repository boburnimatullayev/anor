import './style.css'
import React, { useState } from 'react'
import axios_init from '@/utils/axios_init'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete'
import { Row, Col, Card, Form, Input, Button, message } from 'antd'
const { TextArea } = Input

export default function CreateBranch() {
  const { t } = useTranslation()
  const history = useHistory()

  const [loading, setLoading] = useState(false)
  const [isFetching, setIsFetching] = useState(false)
  const [initialValues, setInitialValues] = useState({
    name: '',
    description: '',
  })

  const onFinish = (values) => {
    axios_init
      .post('/notification', {
        title: values.name,
        message: values.description,
        topic: 'AnorCourier',
        platform: 2,
      })
      .then((res) => {
        message.success(t('saved.successfully'))
        history.replace('/notification')
      })
      .catch((err) => {
        message.error(t('saving.failed'))
      })
  }

  const routes = [
    {
      name: 'notification',
      route: '/notification',
      link: true,
    },

    {
      name: 'create',
      route: '/notification/create',
      link: false,
    },
  ]

  const configs = {
    name: {
      rules: [
        {
          required: true,
          message: t('please.input.title'),
        },
      ],
    },
    description: {
      rules: [
        {
          required: true,
          message: t('please.input.description'),
        },
      ],
    },
  }

  return (
    <div className='branch-user'>
      <BreadCrumbTemplete routes={routes} />

      <div className='create-edit'>
        {!isFetching ? (
          <Form layout='vertical' onFinish={onFinish} initialValues={initialValues}>
            <Card title={t('notification')}>
              <Row style={{ margin: '0 -10px' }}>
                <Col span='8' style={{ padding: '0 10px' }}>
                  <Form.Item name='name' label={t('name')} {...configs.name}>
                    <Input placeholder={t('name')} />
                  </Form.Item>
                </Col>
                <Col span='8' style={{ padding: '0 10px' }}>
                  <Form.Item name='description' label={t('description')} {...configs.description}>
                    <TextArea rows={4} placeholder={t('description')} />
                  </Form.Item>
                </Col>
              </Row>
            </Card>
            <Row justify='end' style={{ paddingTop: '24px', backgroundColor: '#F9F9F9' }}>
              <Button type='primary' htmlType='submit' loading={loading}>
                {t('send')}
              </Button>
            </Row>
          </Form>
        ) : (
          'loading'
        )}
      </div>
    </div>
  )
}
