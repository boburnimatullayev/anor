import './style.css'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { Button, Card, Table, Popconfirm, Tag, message } from 'antd'
import {
  SearchOutlined,
  PlusOutlined,
  EditFilled,
  DeleteFilled,
} from '@ant-design/icons'
import axios_init from '../../../utils/axios_init'
import moment from 'moment'
import StyledButton from '@/components/StyledButton/StyledButton'
import { useHistory } from 'react-router-dom'
import usePermission from '@/utils/usePermission'

export default function Category() {
  const { t } = useTranslation()
  const history = useHistory()
  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [items, setItems] = useState([])
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false,
    },
    {
      name: 'category',
      // route: '/catalog/category',
      link: false,
    },
  ]
  const columns = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: t('created_at'),
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => (
        <div>{moment(text).format('HH:mm DD-MM-YYYY')}</div>
      ),
    },
    {
      title: t('action'),
      key: 'action',
      align: 'center',
      render: (text, record, index) => (
        <div>
          {
            usePermission('category/update') && 
            <StyledButton
              onClick={() => {
                editItem(text, record)
              }}
              color='link'
              icon={EditFilled}
              tooltip={t('edit')}
            />
          }
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => deleteItem(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
            {
              usePermission('category/delete') &&
              <StyledButton
                onClick={() => setVisible(index)}
                color='danger'
                icon={DeleteFilled}
                tooltip={t('delete')}
              />
            }
          </Popconfirm>
        </div>
      ),
    },
  ]

  const pagination = {
    // showSizeChanger: true,
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    // pageSizeOptions: [10, 20, 50, 100],
    // defaultPageSize: 2,
    // pageSizeOptions: [2, 4, 8, 16],
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  // ****************EVENTS
  const ExtraButton = function () {
    return (
      <Button
        type='primary'
        icon={<PlusOutlined />}
        onClick={() => {
          history.push('/catalog/category/create')
        }}
      >
        {t('add')}
      </Button>
    )
  }
  const getData = function (limit = reqQuery.limit, offset = reqQuery.offset) {
    // dispatch(isLoadingOverlay(true))
    axios_init
      .get(`/category?limit=${limit}&offset=${offset}`)
      .then((res) => {
        setItems(res.data)
      })
      .finally(() => {
        // dispatch(isLoadingOverlay(false))
      })
  }
  const editItem = function (text, record) {
    history.push(`/catalog/category/edit?id=${text.id}`)
  }
  const deleteItem = function (text) {
    setLoading(true)
    axios_init
      .remove(`/category/${text.id}`)
      .then((res) => {
        getData(reqQuery.limit, reqQuery.offset)
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
    getData(pageSize, offset)
  }

  React.useEffect(() => {
    getData()
    // getCelebrity()
  }, [])

  return (
    <div className='category'>
      <BreadCrumbTemplete routes={routes} />
      <div className='content'>
        <Card title={t('category')} extra={usePermission('category/create') ? <ExtraButton /> : <div></div>}>
          <Table
            columns={columns}
            dataSource={items.categories}
            rowKey={(record) => record.id}
            scroll={{ x: 100 }}
            pagination={pagination}
            locale={{
              filterConfirm: t('ok'),
              filterReset: t('reset'),
              emptyText: t('no.data'),
            }}
          />
        </Card>
      </div>
    </div>
  )
}
