import "./style.css"
import React, { useRef } from 'react'
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"
import { Button, Card, Col, Form, Input, Row, Skeleton } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
import axios_init from '../../../utils/axios_init'

// import { SearchOutlined, PlusOutlined, EditFilled, DeleteFilled } from '@ant-design/icons'
// import axios_init from '../../../utils/axios_init'
// import moment from 'moment'
const queryString = require('query-string')
const token = localStorage.getItem("token")

export default function Create(props) {
  const { t } = useTranslation()
  const history = useHistory()
  const updateId = queryString.parse(props.location.search).id
  const [loading, setLoading] = React.useState(false)
  const [initialValues, setInitialValues] = React.useState(null)
  // const form = useRef()

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false
    },
    {
      name: 'category',
      route: '/catalog/category',
      link: true
    },
    {
      name: updateId ? 'edit' : 'add',
      route: '/catalog/category/create',
      link: false
    }
  ]

  // ****************EVENTS
  const onFinish = function (value) {
    console.log(value)
    setLoading(true)
    if (updateId) {
      axios_init.put(`/category/${updateId}`, value)
        .then(res => {
          history.push('/catalog/category')
        })
        .finally(() => {
          setLoading(false)
        })
    } else {
      axios_init.post('/category', value)
        .then(res => {
          history.push('/catalog/category')
        })
        .finally(() => {
          setLoading(false)
        })
    }
  }
  // const refs = function (value) {
  //   value.current.validateFields().then(res => {
  //     console.log(res)
  //   })
  //   console.log(value)
  //   console.log(value.current.validateFields())
  // }
  // const getData = function () {
  //   // dispatch(isLoadingOverlay(true))
  //   axios_init.get('/catalog').then(res => {
  //     console.log(res)
  //     setItems(res.data.catalogs)
  //   }).finally(() => {
  //     // dispatch(isLoadingOverlay(false))
  //   })
  // }
  React.useEffect(() => {
    if (updateId) {
      axios_init.get(`/category/${updateId}`).then(res => {
        setInitialValues({
          name: res.data.name
        })
      })
    } else setInitialValues({})
    // getCelebrity()
  }, [])




  // ****************EVENTS





  return (
    <div className="category">
      <BreadCrumbTemplete routes={routes}/>
      <Card title={t('category')}>
        {
          initialValues ?
            (
              <Form
                layout={'vertical'}
                name="basic"
                initialValues={initialValues}
                onFinish={onFinish}
              >
                <Row>
                  <Col span={12}>
                    <Form.Item
                      label="Название категории"
                      name="name"
                      rules={[{ required: true, message: t("please.input.name") }]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <div/>
                  <Form.Item>
                    <Button htmlType="submit" loading={loading} type="primary">
                      { !updateId ? t('create') : t('edit')  }
                    </Button>
                  </Form.Item>
                </div>
              </Form>
            ) :
            (
              <div>
                <Skeleton active />
                <Skeleton active />
                <Skeleton active />
              </div>
            )
        }
      </Card>
    </div>
  )
}