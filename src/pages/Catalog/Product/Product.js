import './style.css'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import BreadCrumbTemplete from '../../../components/breadcrumb/BreadCrumbTemplete'
import { Button, Card, Table, Tag, Popconfirm, message } from 'antd'
import { PlusOutlined, EditFilled, DeleteFilled } from '@ant-design/icons'
import axios_init from '../../../utils/axios_init'
import StyledButton from '@/components/StyledButton/StyledButton'
import moment from 'moment'
import { useHistory } from 'react-router-dom'
import usePermission from '@/utils/usePermission'

export default function Category() {
  const { t } = useTranslation()
  const history = useHistory()
  const [visible, setVisible] = useState(null)
  const [loading, setLoading] = useState(false)
  const [items, setItems] = useState([])
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 })

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false,
    },
    {
      name: 'product',
      route: '/catalog/product',
      link: false,
    },
  ]
  const columns = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: t('created_at'),
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => <div>{moment(text).format('HH:mm DD-MM-YYYY')}</div>,
    },
    {
      title: t('action'),
      key: 'action',
      align: 'center',
      render: (text, record, index) => (
        <div>
          {
            usePermission('product/update') &&
            <StyledButton
              onClick={() => {
                editItem(text, record)
              }}
              color='link'
              icon={EditFilled}
              tooltip={t('edit')}
            />
          }
          <Popconfirm
            title={t('do.you.really.want.to.delete')}
            visible={visible === index}
            onConfirm={() => deleteItem(text)}
            okButtonProps={{ loading: loading }}
            onCancel={() => setVisible(null)}
            cancelText={t('cancel')}
            okText={t('yes')}
          >
            {
              usePermission('product/delete') &&
              <StyledButton onClick={() => setVisible(index)} color='danger' icon={DeleteFilled} tooltip={t('delete')} />
            }
          </Popconfirm>
        </div>
      ),
    },
  ]

  // ****************EVENTS
  const ExtraButton = function () {
    return (
      <Button
        onClick={() => {
          history.push('/catalog/product/create')
        }}
        type='primary'
        icon={<PlusOutlined />}
      >
        {t('add')}
      </Button>
    )
  }
  const getData = function (limit = reqQuery.limit, offset = reqQuery.offset) {
    axios_init
      .get(`/product?limit=${limit}&offset=${offset}`)
      .then((res) => {
        setItems(res.data)
      })
      .finally(() => {})
  }
  const editItem = function (text, record) {
    history.push(`/catalog/product/edit?id=${text.id}`)
  }
  const deleteItem = function (text) {
    setLoading(true)
    axios_init
      .remove(`/product/${text.id}`)
      .then((res) => {
        getData(reqQuery.limit, reqQuery.offset)
        message.success(t('deleted.successfully'))
      })
      .catch((err) => {
        console.log(err)
        message.error(t('deleting.failed'))
      })
      .finally(() => {
        setLoading(false)
        setVisible(null)
      })
  }

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize
    setReqQuery({ limit: pageSize, offset })
    getData(pageSize, offset)
  }

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  }

  React.useEffect(() => {
    getData()
  }, [])

  return (
    <div className='category'>
      <BreadCrumbTemplete routes={routes} />
      <Card title={t('product')} extra={usePermission('product/create') ? <ExtraButton /> : <div></div>}>
        <Table
          columns={columns}
          dataSource={items.products}
          rowKey={(record) => record.id}
          scroll={{ x: 100 }}
          pagination={pagination}
          locale={{
            filterConfirm: t('ok'),
            filterReset: t('reset'),
            emptyText: t('no.data'),
          }}
        />
      </Card>
    </div>
  )
}
