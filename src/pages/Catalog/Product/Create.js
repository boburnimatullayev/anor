import "./style.css"
import React, { useEffect, useState } from 'react'
import { useTranslation } from "react-i18next"
import BreadCrumbTemplete from "../../../components/breadcrumb/BreadCrumbTemplete"
import { Button, Card, Col, Form, Input, Select, Row, Skeleton, message } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
import axios_init from '../../../utils/axios_init'
// import { SearchOutlined, PlusOutlined, EditFilled, DeleteFilled } from '@ant-design/icons'
// import axios_init from '../../../utils/axios_init'
// import moment from 'moment'

const token = localStorage.getItem("token")
const queryString = require('query-string')
const { Option } = Select;

export default function Create(props) {
  const { t } = useTranslation()
  const history = useHistory()
  const updateId = queryString.parse(props.location.search).id
  const [loading, setLoading] = useState(false)
  const [categories, setCategories] = useState([])
  const [documents, setDocuments] = useState([])
  const [initialValues, setInitialValues] = useState(null)
  // const form = useRef()

  const routes = [
    {
      name: 'catalog',
      route: '/catalog',
      link: false
    },
    {
      name: 'product',
      route: '/catalog/product',
      link: true
    },
    {
      name: updateId ? 'edit' : 'add',
      route: '/catalog/product/create',
      link: false
    }
  ]

  // ****************EVENTS
  const onFinish = function (value) {
    console.log(value)
    setLoading(true)
    
    if (updateId) {
      axios_init.put(`/product/${updateId}`, {
        ...value,
        docs: documents.filter(({id}) => 
          value.docs.includes(id)
        )
      }).then(res => {
        history.push('/catalog/product')
        console.log(res)
      }).finally(() => {
        setLoading(false)
      })
    } else {
      axios_init.post('/product', {
        ...value,
        docs: documents.filter(({id}) => 
          value.docs.includes(id)
        )
      }).then(res => {
        history.push('/catalog/product')
        console.log(res)
      }).finally(() => {
        setLoading(false)
      })
    }
  }
  // const refs = function (value) {
  //   value.current.validateFields().then(res => {
  //     console.log(res)
  //   })
  //   console.log(value)
  //   console.log(value.current.validateFields())
  // }
  // const getData = function () {
  //   // dispatch(isLoadingOverlay(true))
  //   axios_init.get('/catalog').then(res => {
  //     console.log(res)
  //     setItems(res.data.catalogs)
  //   }).finally(() => {
  //     // dispatch(isLoadingOverlay(false))
  //   })
  // }
  
  const getProducts = () => {
    if (updateId) {
      axios_init.get(`/product/${updateId}`).then(({data}) => {
        console.log(data)
        setInitialValues({
          ...data,
          docs: data.docs ? data.docs.map(({id}) => id) : []
          // documents: data.docs?.map(({name}) => name)
        })
      })
    } else setInitialValues({})
  }
  
  const getCategories = () => {
    axios_init.get("/category")
      .then(({data}) => setCategories(data.categories))
      .catch(err => console.log(err))
  }

  const getDocuments = () => {
    axios_init.get("/document")
      .then(({data}) => setDocuments(data.documents))
      .catch(err => console.log(err))
  }
  
  useEffect(() => {
    getProducts()
    getCategories()
    getDocuments()
    // getCelebrity()
  }, [])




  // ****************EVENTS





  return (
    <div className="category">
      <BreadCrumbTemplete routes={routes}/>
      <Card title={t('product')}>
        {
          initialValues ?
            (
              <Form
                layout={'vertical'}
                name="basic"
                initialValues={initialValues}
                onFinish={onFinish}
              >
                <Row style={{ margin: '0 -10px' }}>
                  <Col span={8} style={{ padding: '0 10px' }}>
                    <Form.Item
                      label={t("id")}
                      name="id"
                      // rules={[{ required: true, message: t("please.input.name") }]}
                    >
                      <Input placeholder={t('enter.id')} />
                    </Form.Item>
                  </Col>
                  <Col span={8} style={{ padding: '0 10px' }}>
                    <Form.Item
                      label={t("product.name")}
                      name="name"
                      rules={[{ required: true, message: t("please.input.name") }]}
                    >
                      <Input placeholder={t('enter.name')} />
                    </Form.Item>
                  </Col>
                  <Col span={8} style={{ padding: '0 10px' }}>
                    <Form.Item
                      label={t("category")}
                      name="category_id"
                      rules={[{ required: true, message: t("please.select.category") }]}
                    >
                      <Select placeholder={t("select.category")} loading={!categories.length}>
                        {categories.length ? categories.map(({id, name}) => (
                          <Option key={id} value={id}>{name}</Option>
                        )) : <></>}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={8} style={{ padding: '0 10px' }}>
                    <Form.Item
                      label={t("documents")}
                      name="docs"
                      rules={[{ required: true, message: t("please.select.documents") }]}
                    >
                      <Select
                        mode="multiple"
                        placeholder={t("select.documents")}
                        loading={!documents.length}
                      >
                        {documents.length ? documents.map((val, i) => (
                          <Option key={val.id} value={val.id}>{val.name}</Option>
                        )) : <></>}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <div/>
                  <Form.Item>
                    <Button htmlType="submit" loading={loading} type="primary">
                      { !updateId ? t('create') : t('edit')  }
                    </Button>
                  </Form.Item>
                </div>
              </Form>
            ) :
            (
              <div>
                <Skeleton active />
                <Skeleton active />
                <Skeleton active />
              </div>
            )
        }
      </Card>
    </div>
  )
}