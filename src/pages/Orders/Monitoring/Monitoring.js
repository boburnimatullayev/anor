import StyledTag from "@/components/StyledTag/StyledTag";
import { DownloadOutlined } from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { Card, Table, Button, Tag } from "antd";
import { useTranslation } from "react-i18next";
import axios_init from "@/utils/axios_init";
import moment from "moment";
import "./style.css";

//MONITORING

export default function Monitoring({ tabKey }) {
  const { t } = useTranslation();
  const [tableLoading, setTableLoading] = useState(false);
  const [reqQuery, setReqQuery] = useState({ limit: 10, offset: 0 });
  const [data, setTableData] = useState([]);
  const [dataCount, setDataCount] = useState(0);

  //GET ORDER DATA
  const getMonitoringData = () => {
    setTableLoading(true);
    axios_init
      .get(`/order-by-status/overdue`, {
        limit: reqQuery.limit,
        offset: reqQuery.offset,
      })
      .then((res) => {
        axios_init
          .get("/order-statuses")
          .then(({ data }) => {
            const allStatuses = data.order_statuses.map((status) => {
              return { name: status.name, value: status.id };
            });
            axios_init
              .get("/courier")
              .then(({ data }) => {
                const allCouriers = data.couriers.map((courier) => {
                  return { text: courier.name, value: courier.id };
                });
                setTableData((el) =>
                  res.data.orders.map((el) => ({
                    ...el,
                    status: getStat(allStatuses, el.status_id),
                    courier_name: getCurName(allCouriers, el.courier_id),
                  }))
                );
                setDataCount(res.data.count);
              })
              .catch((err) => {});
          })
          .catch((err) => {});
      })
      .catch((err) => console.log(err))
      .finally(() => setTableLoading(false));
  };

  // GET STATUSES BY GIVEN STATUS_ID
  const getStat = (arr, value) => {
    return arr.filter((el) => el.value === value)[0]?.name;
  };
  // GET COURIER NAME BY ID
  const getCurName = (arr, value) => {
    if (value) {
      return arr.filter((el) => el.value === value)[0]?.text;
    }
    return t("no.courier");
  };

  const ExtraButton = function () {
    return (
      <Button
        type="primary"
        icon={<DownloadOutlined />}
        onClick={() => {
          // history.push('/notification/create');
        }}
      ></Button>
    );
  };

  useEffect(() => {
    if (tabKey === "monitoring") {
      getMonitoringData();
    }
  }, [reqQuery, tabKey]);

  const pagination = {
    total: dataCount,
    defaultCurrent: 1,
    defaultPageSize: 10,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const routes = [
    {
      name: "orders",
      route: "/orders",
      link: false,
    },
    {
      name: "monitoring",
      link: false,
      route: "/orders/monitoring",
    },
  ];

  const columns = [
    {
      title: t("status"),
      key: "status_id",
      width: 140,
      render: (status) => <StyledTag>{status.status}</StyledTag>,
    },
    {
      title: t("time"),
      key: "created_at",
      dataIndex: "created_at",
      render: (date) => (
        <>{moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")}</>
      ),
      width: 150,
    },
    {
      title: t("client"),
      dataIndex: "customer_name",
      key: "customer_name",
      width: 300,
    },
    {
      title: t("customer_phone"),
      dataIndex: "customer_phone",
      key: "customer_phone",
      width: 150,
    },
    {
      title: t("address"),
      dataIndex: "customer_address",
      key: "customer_address",
      width: 340,
    },
    {
      title: t("product"),
      dataIndex: "product",
      key: "product",
      render: (product) => <Tag color="blue">{product.name}</Tag>,
      width: 200,
    },
    {
      title: t("mob.representatives"),
      dataIndex: "courier_name",
      key: "courier_name",
      width: 300,
    },
    {
      title: t("created_at"),
      key: "created_at",
      dataIndex: "created_at",
      render: (date) => (
        <div>{moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
      ),
      width: 150,
    },
    {
      title: t("time.chosen.by.the.client.for.delivery"),
      key: "delivery_day",
      dataIndex: "delivery_time",
      render: (date) => (
        <div>{moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
      ),
      width: 150,
    },
    {
      title: t("application.distribution.time"),
      key: "assigned_at",
      dataIndex: "assigned_at",
      render: (date) => (
        <div>
          {date !== "0001-01-01T00:00:00Z"
            ? moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")
            : ""}
        </div>
      ),
      width: 150,
    },
    {
      title: t("comment"),
      dataIndex: "note",
      key: "note",
      width: 300,
    },
  ];

  return (
    <div className="content">
      {/* MONITORING TAB START */}
      <Card extra={<ExtraButton />}>
        <Table
          rowClassName={(record) => {
            if (record.status_id === "overdue") {
              return "overdue-row";
            }
          }}
          scroll={{ x: 1700 }}
          bordered
          columns={columns}
          loading={tableLoading}
          dataSource={data}
          pagination={pagination}
          rowKey={(record) => record.id}
        />
      </Card>
      {/* MONITORING TAB END */}
    </div>
  );
}
