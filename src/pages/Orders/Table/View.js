import React, { useState, useEffect } from 'react';
import moment from 'moment';
import StyledTag from '@/components/StyledTag/StyledTag';
import axios_init from '@/utils/axios_init';
import { useTranslation } from 'react-i18next';
import { Table, Modal, Checkbox, message, Tag, Descriptions } from 'antd';
import { YMaps, Map, Polygon, Placemark } from 'react-yandex-maps';
import { defaultMapState } from '@/constants/basic';

export function View({ orderData = {}, getItems, onCancel, status, ...props }) {
  const { t } = useTranslation();

  useEffect(() => {
    if (props.visible) {
      makeDocuments().then((val) => {
        setDocs(val);
      });
    }
  }, [props.visible]);

  const [img, setImg] = useState({});
  const [loading, setLoading] = useState(false);
  const [docs, setDocs] = useState([]);
  const [rejected, setRejected] = useState([]);

  useEffect(() => {}, [rejected]);

  const handleSave = () => {
    if (status === 'finished') {
      onModalCancel();
      return;
    }

    setLoading(true);
    if (rejected.length) {
      rejected.map((data, i) => {
        axios_init
          .put('/order-file', data)
          .then((res) => {
            if (i + 1 === rejected.length) {
              axios_init
                .put('/order-processing/change-status', {
                  comment: data.document_name + ' has been rejected',
                  status_id: 'rejected',
                  order_id: data.order_id,
                })
                .then((res) => {
                  getItems();
                })
                .finally(() => {
                  onModalCancel();
                  setLoading(false);
                });
            }
          })
          .catch((err) => console.log(err));
      });
    } else {
      axios_init
        .put('/order-processing/change-status', {
          comment: orderData.customer_name + ' has been finished',
          status_id: 'finished',
          order_id: orderData.id,
        })
        .then((res) => {
          getItems();
        })
        .finally(() => {
          onModalCancel();
          setLoading(false);
        });
    }
  };

  const makeDocuments = () => {
    return new Promise((resolve, reject) => {
      try {
        let res = orderData.product.docs.map((val) => ({
          ...val,
          files: orderData?.doc_files?.filter(({ document_id }) => document_id === val.id),
        }));
        resolve(res);
      } catch (e) {
        reject(e);
      }
    });
  };

  const onModalCancel = () => {
    setDocs([]);
    setRejected([]);
    onCancel();
  };

  const onCheckboxChange = (e, file) => {
    if (e.target.checked) {
      setRejected((old) => [...old, { ...file, approved: false }]);
    } else {
      setRejected((old) => old.filter(({ file_url }) => file.file_url !== file_url));
    }
  };

  const columnsOrder = [
    {
      title: t('name'),
      dataIndex: ['product', 'name'],
      key: ['product', 'name'],
      render: (data) => data,
    },
    {
      title: t('card.number'),
      dataIndex: 'card_number',
      key: 'card_number',
    },
    {
      title: t('status'),
      dataIndex: ['status_id'],
      key:  ['status_id'],
      render: (status) => (
        <StyledTag color={status === 'finished' ? 'success' : 'danger'}>{t(status === 'finished' ? 'active' : 'inactive')}</StyledTag>
      ),
    },
    {
      title: t('documents'),
      dataIndex: ['product', 'docs'],
      key: ['product', 'docs'],
      render: (data) =>
        data
          ? data.map((val, i) => (
              <Tag key={i} color='blue'>
                {val.name}
              </Tag>
            ))
          : t('no.document'),
    },
  ];

  const columnsFiles = [
    {
      title: t('name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: t('files'),
      dataIndex: 'files',
      key: 'files',
      render: (text, record) =>
        text?.map((file) => (
          <div key={file.file_url} style={{ padding: 5 }}>
            {status !== 'finished' && (
              <Checkbox defaultChecked={false} onChange={(e) => onCheckboxChange(e, file)}>
                {file.file_name}
              </Checkbox>
            )}
            <div
              style={{ width: 30, marginLeft: 5, cursor: 'pointer', display: 'inline-block' }}
              onClick={() => setImg({ file_url: file.file_url, file_name: file.file_name })}
            >
              <img src={file.file_url} width='100%' />
            </div>
          </div>
        )),
    },
  ];

  const columnsHistory = [
    {
      title: t('status'),
      dataIndex: 'status_id',
      key: 'status',
      render: (val) => t(val),
    },
    {
      title: t('updated_at'),
      dataIndex: 'created_at',
      key: 'created_at',
      render: (date) => moment(date).format('YYYY-MM-DD HH:mm'),
    },
    {
      title: t('comment'),
      dataIndex: 'comment',
      key: 'comment',
      render: (val) => t(val),
    },
  ];
  
  const columnsCalls = [
    {
      title: t('completed.calls.date.time'),
      dataIndex: 'calls_time',
      key: 'calls_time',
      render: (arr) => arr ? arr?.map((el) => {
        return <>
        {<p>{moment(el).format('YYYY-MM-DD HH:mm')}</p> || '-'}
        </>
      } ) : '-'
    },
    {
      title: t('completed.calls.count'),
      dataIndex: 'calls_count',
      key: 'calls_count',
      render: (count) => count
    },
  ];

  const columnsNFC = [
    {
      title: t('nfc_match'),
      dataIndex: 'nfc_match',
      key: 'nfc_match',
      render: (nfc_match) => nfc_match
    },
  ];

  return (
    <Modal
      {...props}
      onOk={handleSave}
      okText={rejected.length ? t('reject') : t('finish')}
      onCancel={onModalCancel}
      confirmLoading={loading}
    >
      <Descriptions title={t('user.info')} layout='vertical' bordered>
        <Descriptions.Item label={t('customer.name')}>{orderData.customer_name}</Descriptions.Item>
        <Descriptions.Item label={t('customer.phone')}>{orderData.customer_phone}</Descriptions.Item>
        <Descriptions.Item label={t('customer.address')}>{orderData.customer_address}</Descriptions.Item>
        <Descriptions.Item label={t('note')}>{orderData.note}</Descriptions.Item>
      </Descriptions>

      <div style={{ display: 'flex', alignItems: 'baseline',  margin: '20px 0', justifyContent: 'left'}}>
      <h3 >{t('doc.type')}</h3>
      <p style={{ marginLeft: '25px'}}>{t(orderData.is_foreign_docs ? "doc.foreign" : "doc.local")}</p>
      </div>
      {[orderData.product] ? (
        <div>
          <h3 style={{ margin: '20px 0' }}>{t('products')}</h3>
          <Table
            bordered
            pagination={false}
            columns={columnsOrder}
            dataSource={[orderData]}
            rowKey={(record) => record.id}
          />
        </div>
      ) : undefined}
      {docs ? (
        <div>
          <h3 style={{ margin: '20px 0' }}>{t('files')}</h3>
          <Table
            bordered
            scroll={{ y: 500 }}
            pagination={false}
            columns={columnsFiles}
            dataSource={docs}
            rowKey={(val) => val.id}
          />
        </div>
      ) : undefined}
      {orderData.status_logs ? (
        <div>
          <h3 style={{ margin: '20px 0' }}>{t('history')}</h3>
          <Table
            bordered
            scroll={{ y: 500 }}
            pagination={false}
            columns={columnsHistory}
            dataSource={orderData.status_logs}
            rowKey={(val) => val.status_id}
          />
        </div>
      ) : undefined}
      <div>
          <h3 style={{ margin: '20px 0' }}>{t('calls')}</h3>
          <Table
            bordered
            scroll={{ y: 500 }}
            pagination={false}
            columns={columnsCalls}
            dataSource={[{calls_count: orderData.calls_count, calls_time: orderData.calls_time}]}
            rowKey={(val) => val.id}
          />
      </div>
      <div>
          <h3 style={{ margin: '20px 0' }}>{t('NFC')}</h3>
          <Table
            bordered
            scroll={{ y: 500 }}
            pagination={false}
            columns={columnsNFC}
            dataSource={[{nfc_match: orderData.nfc_match}]}
            rowKey={(val) => val.id}
          />
      </div>
      {orderData.customer_location && (
        <>
          <h3 style={{ margin: '20px 0' }}>{t('order.address')}</h3>
          <div
            style={{
              backgroundColor: 'grey',
              width: '100%',
              height: 250,
            }}
          >
            <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
              <Map
                width='100%'
                height='100%'
                state={{
                  ...defaultMapState,
                  center: [orderData.customer_location.lat, orderData.customer_location.long],
                }}
              >
                <Placemark geometry={[orderData.customer_location.lat, orderData.customer_location.long]} />
              </Map>
            </YMaps>
          </div>
        </>
      )}

      <Modal visible={img.file_url} title={img.file_name} footer={null} onCancel={() => setImg({})}>
        <img src={img.file_url} width='100%' />
      </Modal>
    </Modal>
  );
}
