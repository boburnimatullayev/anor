import './style.css';
import React, { useState, useEffect, useRef } from 'react';
import moment from 'moment';
import axios_init from '@/utils/axios_init';
import { validatePhone } from '@/utils/validatePhone';
import { useTranslation } from 'react-i18next';
import BreadCrumbTemplete from '@/components/breadcrumb/BreadCrumbTemplete';
import { defaultMapState } from '@/constants/basic';
import { useHistory, useLocation } from 'react-router-dom';
import { YMaps, Map, Polygon, Placemark } from 'react-yandex-maps';
import { Row, Col, Card, Form, Input, Button, Select, DatePicker, TimePicker, message } from 'antd';

const { Option } = Select;

export default function Add() {
  const { t } = useTranslation();
  const history = useHistory();
  const state = useLocation().state;
  const polygonRef = useRef(null);

  const [branches, setBranches] = useState([]);
  const [products, setProducts] = useState([]);
  const [placemarkGeometry, setPlacemarkGeometry] = useState([]);
  const [geozone_id, setGeozone_id] = useState(null);
  const [loading, setLoading] = useState(false);
  const [multiZone, setMultiZone] = useState({
    defaultState: defaultMapState,
    geometries: [],
    options: {},
  });

  const routes = [
    {
      name: 'orders',
      route: '/orders/table',
      link: true,
    },
    {
      name: 'edit',
      link: false,
      route: '/orders/table/edit',
    },
  ];

  useEffect(() => {
    if (state) {
      getBranches();
      getProducts();
      getMapData(state.geozone_id);
      setPlacemarkGeometry([state.customer_location.lat, state.customer_location.long]);
      setGeozone_id(state.geozone_id.split(','));
    } else {
      history.push('/orders/table');
    }
  }, []);

  function disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  const onFinish = (values) => {
    if (placemarkGeometry.length) {
      setLoading(true);
      const delivery_time = values.delivery_time.format('YYYY-MM-DD HH:mm');

      let data = {
        ...values,
        customer_phone: '+998' + values.customer_phone,
        geozone_id: geozone_id.length ? geozone_id.join() : null,
        delivery_time: new Date(`${delivery_time} UTC`),
        customer_location: {
          lat: placemarkGeometry[0],
          long: placemarkGeometry[1],
        },
      };

      axios_init
        .put(`/order/${state.id}`, data)
        .then((res) => {
          message.success(t('updated.successfully'));
          history.push('/orders');
        })
        .catch((err) => {
          console.log(err);
          message.error(t('updating.failed'));
        })
        .finally(() => setLoading(false));
    } else {
      message.error(t('please.mark.place.on.geozone'));
    }
  };

  const getBranches = () => {
    axios_init
      .get('/branch')
      .then(({ data }) => {
        setBranches(data.branches);
      })
      .catch((err) => console.log(err));
  };

  const getProducts = () => {
    axios_init
      .get('/product')
      .then(({ data }) => {
        setProducts(data.products);
      })
      .catch((err) => console.log(err));
  };

  const getMapData = (id) => {
    let ids;
    if (typeof id === 'string') ids = id.split(',');
    else ids = id;

    axios_init
      .get(`/geozones`, {
        geozone_ids: ids.length ? ids.join() : null,
      })
      .then(({ data }) => {
        if (!data.geozones.length) {
          setMultiZone((prev) => ({ ...prev, geometries: [] }));
          return;
        }
        const firstData = JSON.parse(data.geozones[0].shape.data);
        setMultiZone({
          geometries: data.geozones.map((geozone) => JSON.parse(geozone.shape.data).geometry[0]),
          options: firstData.options,
          defaultState: firstData.defaultState,
        });
      })
      .catch((err) => console.log(err));
  };

  const handleChangeBranch = (val) => {
    let { geozone_id } = branches.filter((item) => item.id === val)[0];
    setGeozone_id(geozone_id);
    getMapData(geozone_id);
  };

  const setPolygonRef = (ref) => {
    if (ref) {
      polygonRef.current = ref;

      ref.events.add('click', (e) => {
        setPlacemarkGeometry(e.get('coords'));
      });
    }
  };

  const configs = {
    branch_id: {
      rules: [
        {
          required: true,
          message: t('please.select.branch'),
        },
      ],
    },
    product: {
      rules: [
        {
          required: true,
          message: t('please.select.product'),
        },
      ],
    },
    date: {
      rules: [
        {
          required: true,
          message: t('please.select.date'),
        },
      ],
    },
    time_range: {
      rules: [
        {
          type: 'array',
          required: true,
          message: t('please.select.time.range'),
        },
      ],
    },
    customer_name: {
      rules: [
        {
          required: true,
          message: t('please.input.name'),
        },
      ],
    },
    customer_phone: {
      rules: [
        {
          required: true,
          message: t('please.input.phone'),
        },
        {
          validator: (_, value) => validatePhone(value),
        },
      ],
    },
    customer_address: {
      rules: [
        {
          required: true,
          message: t('please.input.address'),
        },
      ],
    },
    doc_type: {
      rules: [
        {
          required: true,
          message: t("please.select.doc"),
        },
      ],
    }
  };

  return (
    <div className='orders'>
      <BreadCrumbTemplete routes={routes} />

      <div className='add'>
        <Form
          layout='vertical'
          onFinish={onFinish}
          initialValues={
            state
              ? {
                  ...state,
                  customer_phone: state.customer_phone.split('+998')[1],
                  delivery_time: moment(new Date(new Date(state.from_time).getTime() + 1800000)).utc(), //moment(state.from_time, 'YYYY-MM-DD HH:mm'),
                  product: state.product.id,
                }
              : {}
          }
        >
          <Card title={t('orders')}>
            <Row style={{ margin: '0 -10px' }}>
              <Col span='12' style={{ padding: '0 10px' }}>
                <Form.Item name='branch_id' label={t('branches')} {...configs.branch_id}>
                  <Select
                    placeholder={t('select.branch')}
                    loading={!branches.length}
                    onChange={(val) => handleChangeBranch(val)}
                  >
                    {branches.length ? (
                      branches.map(({ name, id }) => (
                        <Option value={id} key={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col span='12' style={{ padding: '0 10px' }}>
                <Form.Item name='product' label={t('product')} {...configs.product}>
                  <Select placeholder={t('select.product')} loading={!products.length}>
                    {products.length ? (
                      products.map(({ name, id }) => (
                        <Option value={id} key={id}>
                          {name}
                        </Option>
                      ))
                    ) : (
                      <></>
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col span='12' style={{ padding: '0 10px' }}>
                <Form.Item name='delivery_time' label={t('delivery.day')} {...configs.date}>
                  <DatePicker
                    allowClear={false}
                    showTime
                    format='YYYY-MM-DD HH:mm'
                    placeholder={t('select.date')}
                    style={{ width: '100%' }}
                    disabledDate={disabledDate}
                  />
                </Form.Item>
              </Col>
              <Col span="12" style={{ padding: "0 10px" }}>
                <Form.Item
                  name="is_foreign_docs"
                  label={t("doc.type")}
                  {...configs.doc_type}
                >
                   <Select
                   placeholder={t('doc.select')}
                  >
                    <Option value={true}> {t('doc.foreign')}</Option>
                    <Option value={false}> {t('doc.local')}</Option>                   
                  </Select>
                  
                </Form.Item>
              </Col>
            </Row>

            <Row style={{ margin: '0 -10px' }}>
              <Col span='12' style={{ padding: '0 10px', paddingBottom: 24 }}>
                <div
                  style={{
                    backgroundColor: 'grey',
                    width: '100%',
                    height: '100%',
                  }}
                >
                  <YMaps query={{ lang: 'ru_RU', load: 'package.full' }}>
                    <Map width='100%' height='100%' state={multiZone?.defaultState}>
                      {multiZone ? (
                        <Polygon
                          geometry={multiZone.geometries}
                          options={{
                            ...multiZone.options,
                            draggable: false,
                            hasHint: false,
                            fillOpacity: 0.1,
                          }}
                          instanceRef={(ref) => setPolygonRef(ref)}
                        />
                      ) : (
                        <></>
                      )}
                      <Placemark geometry={placemarkGeometry} />
                    </Map>
                  </YMaps>
                </div>
              </Col>
              <Col span='12' style={{ padding: '0 10px' }}>
                <Form.Item name='customer_name' label={t('client') + ' ' + t('fio')} {...configs.customer_name}>
                  <Input placeholder={t('enter.name')} />
                </Form.Item>
                <Form.Item name='customer_phone' label={t('phone.number')} {...configs.customer_phone}>
                  <Input placeholder={t('enter.phone.number')} prefix='(+998)' />
                </Form.Item>
                <Form.Item name='customer_address' label={t('address')} {...configs.customer_address}>
                  <Input placeholder={t('enter.address')} />
                </Form.Item>
              </Col>

              <Col span='24' style={{ padding: '0 10px' }}>
                <Form.Item name='note' label={t('note')}>
                  <Input.TextArea rows={4} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <Row justify='end' style={{ marginTop: 24 }}>
            <Button type='primary' htmlType='submit' loading={loading}>
              {t('save')}
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}
