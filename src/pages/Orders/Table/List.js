import React, { useState, useEffect } from "react";
import moment from "moment";
import { View } from "./View";
import StyledTag from "@/components/StyledTag/StyledTag";
import axios_init from "@/utils/axios_init";
import StyledButton from "@/components/StyledButton/StyledButton";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import {
  Card,
  Button,
  Table,
  Modal,
  Form,
  Input,
  Row,
  Col,
  message,
  Select,
  Tag,
  Popconfirm,
} from "antd";
import {
  AppstoreAddOutlined,
  PlusOutlined,
  EditFilled,
  DeleteFilled,
  EyeOutlined,
  DownloadOutlined,
} from "@ant-design/icons";
import makeStrFirstLetterUpper from "../../../utils/makeStrFirstLetterUpper";
import TextFilter from "../../../components/RangeFilter/TextFilter";
import CustomDatePicker from "../../../components/DatePicker";
import usePermissions from "../../../utils/usePermission";
import SearchFilter from "../../../components/RangeFilter/SearchFilter";
import { useSelector } from "react-redux";
import axios from "axios";

let isFirstRender = true;
const { Search } = Input;




export function List({ tabKey }) {
  // React.useState(nprogress.done());
  const { t } = useTranslation();
  const history = useHistory();
  const isExcelPage = history.location.pathname.includes("excel");


  const [form] = Form.useForm();

  const [items, setItems] = useState([]);
  const [uidValue, setUidValue] = useState("");
  const [popoverVisibleUid, setPopoverVisibleUid] = useState(false);
  const [couriersList, setCouriersList] = useState([]);
  const [courierListByBranchId, setCourierListByBranchId] = useState([]);
  const [selectedItem, setSelectedItem] = useState({});
  const [assignModal, setAssignModal] = useState(false);
  const [visibleView, setVisibleView] = useState(false);
  const [popVisible, setPopVisible] = useState(null);
  const [forwardModal, setForwardModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [listloading, setListLoading] = useState(false);
  const [orderData, setOrderData] = useState({});
  const [reqQuery, setReqQuery] = useState({ limit: 60, offset: 0 });
  const [loadBtn, setLoadBtn] = useState(false);
  const [sort, setSort] = useState({ order: "", arrangement: "" });
  const [searchText, setSearchText] = useState("");
  const [products, setProducts] = useState([]);
  const [statusList, setStatusList] = useState([]);
  const [commentValue, setCommentValue] = useState("");
  const [geozoneList, setGeozoneList] = useState([]);
  const [branchList, setBranchList] = useState([]);
  const [chosenStatus, setChosenStatus] = useState("");
  const [chosenGeozone, setChosenGeozone] = useState("");
  const [chosenBranch, setChosenBranch] = useState("");
  const [chosenProduct, setChosenProduct] = useState("");
  const [chosenRepresentive, setChosenRepresentive] = useState("");

  const [timeRange, setTimeRange] = useState("");
  const [customerOrderRange, setCustomerOrderRange] = useState("");
  const [deliveryDayRange, setDeliveryDayRange] = useState("");
  const [courierAssignedRange, setCourierAssignedRange] = useState("");
  const [popoverVisible, setPopoverVisible] = useState(false);
  const [popoverVisibleComment, setPopoverVisibleComment] = useState(false);
  const [popoverVisibleGeozone, setPopoverVisibleGeozone] = useState(false);
  const [popoverVisibleBranch, setPopoverVisibleBranch] = useState(false);
  const [popoverVisibleProducts, setPopoverVisibleProducts] = useState(false);
  const [popoverVisibleCourier, setPopoverVisibleCourier] = useState(false);

  const [getValueStatus, setGetValueStatus] = useState("");
  const [searchedWordStatus, setSearchedWordStatus] = useState("");
  const [searchedWordComment, setSearchedWordComment] = useState("");
  const [getValueGeozone, setGetValueGeozone] = useState("");
  const [searchedWordGeozone, setSearchedWordGeozone] = useState("");
  const [getValueBranch, setGetValueBranch] = useState("");
  const [searchedWordBranch, setSearchedWordBranch] = useState("");
  const [getValueStatusProduct, setGetValueStatusProduct] = useState("");
  const [searchedWordProduct, setSearchedWordProduct] = useState("");
  const [getValueStatusCourier, setGetValueStatusCourier] = useState("");
  const [searchedWordProductCourier, setSearchedWordProductCourier] =
    useState("");
  const [commentState, setCommentState] = useState("");
  const permissions = useSelector((state) => state.auth.permissions);
  const permissionOrder = permissions?.find((item) => item?.name === "orders");
  const permissionAllBranch = permissions?.find((item) => item?.name === "all branch");
  useEffect(() => {
    if (tabKey === "table") {
      getCouriers();
      getProducts();
      getStatuses();
      getGeozones();
      getBranches();

      !isFirstRender && setReqQuery({ limit: 10, offset: 0 });
      isFirstRender = false;
    }
  }, [tabKey]);

  useEffect(() => {
   
    if (tabKey === "table" || isExcelPage) {
      getItems();
    } 
  }, [
    reqQuery,
    sort,
    searchText,
    timeRange,
    customerOrderRange,
    courierAssignedRange,
    deliveryDayRange,
    chosenStatus,
    commentValue,
    chosenProduct,
    chosenRepresentive,
    chosenGeozone,
    chosenBranch,
    uidValue,
    tabKey,
  ]);

  let userData = JSON.parse(window.localStorage.getItem("user-data"));

  const getItems = () => {
    setListLoading(true);

    const params = {
      limit: reqQuery.limit,
      offset: reqQuery.offset,
      geozone_id: chosenGeozone?.length ? chosenGeozone.join() : null,
      status: chosenStatus?.length ? chosenStatus.join() : null,
      comment: commentValue,
      product: chosenProduct?.length ? chosenProduct.join() : null,
      courier_id: chosenRepresentive?.length ? chosenRepresentive.join() : null,
      arrangement: arrng[sort.arrangement],
      order: sort.order,
      search: searchText,
      status_from_time: timeRange?.[0],
      status_to_time: timeRange?.[1],
      from_time: customerOrderRange?.[0],
      to_time: customerOrderRange?.[1],
      delivery_time_from: deliveryDayRange?.[0],
      delivery_time_to: deliveryDayRange?.[1],
      assigned_from_time: courierAssignedRange?.[0],
      assigned_to_time: courierAssignedRange?.[1],
      search_id: uidValue,
    };
    if (permissionOrder?.all_branch && !isExcelPage) {
      params.branch_id = JSON.parse(localStorage.getItem("branch_id"));
    } else if (userData.type === "SUPER-ADMIN" || permissionAllBranch?.id === "dd41320a-4155-4333-a3ea-f4608c6d6877") {
      params.branch_id = chosenBranch?.length ? chosenBranch.join() : null;
    }


    axios_init
      .get(isExcelPage ? "report/orders" : "order", params)
      .then((res) => {
      
          
        setItems(res.data);
        
      })
      .catch(() => {
        setItems([]);
      })
      .finally(() => setListLoading(false));
  };

  const getGeozones = () => {
    axios_init
      .get("/geozone")
      .then(({ data }) => {
        const newZoneList = data.geozones.map((elm) => {
          return { text: elm.name, value: elm.id };
        });
        setGeozoneList(newZoneList);
      })
      .catch((err) => {});
  };

  const getBranches = () => {
    axios_init
      .get("/branch")
      .then(({ data }) => {
        const newBranchList = data.branches.map((elm) => {
          return { text: elm.name, value: elm.id };
        });
        setBranchList(newBranchList);
      })
      .catch((err) => {});
  };

  const getProducts = () => {
    axios_init
      .get("/product")
      .then(({ data }) => {
        const productsName = data.products.map((product) => {
          return { text: t(product.name), value: product.id };
        });
        setProducts(productsName);
      })
      .catch((err) => {});
  };

  const getStatuses = () => {
    axios_init
      .get("/order-statuses")
      .then(({ data }) => {
 
        let newArray = [];
        data &&
          data.order_statuses
            .map((status) => {
              return { text: status.name, value: status.id };
            })
            .forEach((e) => {
              if (
                ![
                  "not_enough_docs",
                  "changed_address_and_time",
                  "cannot_call",
                ].includes(e.value)
              ) {
                newArray.push(e);
              }
            });
        setStatusList(newArray);
      })
      .catch((err) => {});
  };

  const getCouriers = () => {
    if (userData.type === "SUPER-ADMIN") {
      axios_init
        .getC("/courier")
        .then(({ data }) => {
          const allCouriers = data.couriers.map((courier) => {
            return { text: courier.name, value: courier.id };
          });
          setCouriersList(allCouriers);
          setCourierListByBranchId(allCouriers);
        })
        .catch((err) => {});
    } else {
      axios_init
        .get("/courier")
        .then(({ data }) => {
          const allCouriers = data.couriers.map((courier) => {
            return { text: courier.name, value: courier.id };
          });
          setCouriersList(allCouriers);
          setCourierListByBranchId(allCouriers);
        })
        .catch((err) => {});
    }
  };

  const getCouriersByBranchId = (val) => {
    console.log("rrr");

    axios_init
      .get("/courier", {
        branch_id: val,
      })
      .then(({ data }) => {
        const allCouriers = data.couriers.map((courier) => {
          return { text: courier.name, value: courier.id };
        });
        setCourierListByBranchId(allCouriers);
      })
      .catch((err) => {});
  };

  const handleDelete = (id) => {
    setDeleteLoading(true);
    axios_init
      .remove(`order/${id}`)
      .then((data) => {
        message.success(t("deleted.successfully"));
        getItems();
      })
      .catch((err) => {
        {
        }
        message.error(t("deleting.failed"));
      })
      .finally(() => {
        setDeleteLoading(false);
        setPopVisible(null);
      });
  };

  const handleEdit = (item) => {
    history.push({
      pathname: "/orders/table/edit",
      state: item,
    });
  };

  const handleView = (text, index) => {
    setLoading(true);
    axios_init
      .get(`/order/${text?.id}`)
      .then((res) => {
        setOrderData(res.data);
        setVisibleView(true);
      })
      .catch((err) => {})
      .finally(() => setLoading(false));
  };

  const closeView = () => {
    setVisibleView(false);
  };

  const onForwardFinish = ({ comment, status_id, ...args }) => {
    setLoading(true);
    axios_init
      .put("/order-processing/change-status", {
        comment,
        status_id,
        order_id: selectedItem.id,
      })
      .then(() => {
        getItems();
        message.success(t("status.changed.successfully"));
        form.resetFields();
        setForwardModal(false);
      })
      .catch(() => {
        message.error(t("changing.failed"));
      })
      .finally(() => setLoading(false));
  };

  const handleAssign = (text) => {
    setAssignModal(true);
    setSelectedItem(text);
  };

  const branchSelectAssigning = (value) => {
    getCouriersByBranchId(value);
  };

  const onAssignFinish = ({ comment, courier_id }) => {
    setLoading(true);
    axios_init
      .put("/order-processing/assign", {
        comment: comment,
        courier_id,
        order_id: selectedItem.id,
      })
      .then(() => {
        getItems();
        message.success(t("assigned.successfully"));
        form.resetFields();
        setAssignModal(false);
      })
      .catch(() => {
        message.error(t("changing.failed"));
      })
      .finally(() => setLoading(false));
  };

  const onTableChange = (pagination, filters, sorter) => {
    onSortChange(sorter);
    onFilterChange(filters);
  };

  const onFilterChange = (filters) => {
    if (filters.status_id) {
      setChosenStatus(filters.status_id[0]);
    }
    if (filters.product) {
      setChosenProduct(filters.product[0]);
    }
    if (filters.courier_id) {
      setChosenRepresentive(filters.courier_id[0]);
    }
  };

  const onSortChange = (sorter) => {
    setSort({ order: sorter.columnKey, arrangement: sorter.order });
  };

  const onSearchChange = (text) => {
    setSearchText(text);
    setReqQuery((old) => ({ ...old, offset: 0 }));
  };

  const onPaginationChange = (page, pageSize) => {
    let offset = (page - 1) * pageSize;
    setReqQuery({ limit: pageSize, offset });
  };

  const downloadApi = () => {
    setLoadBtn(true);
    axios_init
      .get("/report/orders-excel", {
        limit: reqQuery.limit,
        offset: 0,
        geozone_id: chosenGeozone?.length ? chosenGeozone.join() : null,
        branch_id: chosenBranch?.length ? chosenBranch.join() : null,
        search_id: uidValue,
        status: chosenStatus?.length ? chosenStatus.join() : null,
        comment: commentValue,
        product: chosenProduct?.length ? chosenProduct.join() : null,
        courier_id: chosenRepresentive?.length
          ? chosenRepresentive.join()
          : null,
        arrangement: arrng[sort.arrangement],
        order: sort.order,
        search: searchText,
        status_from_time: timeRange?.[0],
        status_to_time: timeRange?.[1],
        from_time: timeRange?.[0] ?? "",
        to_time: timeRange?.[1] ?? "",
        delivery_time_from: deliveryDayRange?.[0],
        delivery_time_to: deliveryDayRange?.[1],
        assigned_from_time: courierAssignedRange?.[0],
        assigned_to_time: courierAssignedRange?.[1],
        type: isExcelPage ? "report" : "orders",
      })
      .then((res) => {
        window.location.href = res.data;
      })
      .catch(err => {
        console.log(err)
      })
      .finally(() => setLoadBtn(false));
  };

  const pagination = {
    total: items.count,
    defaultCurrent: 1,
    current: reqQuery.offset / reqQuery.limit + 1,
    defaultPageSize: 60,
    onShowSizeChange: (page, pageSize) => onPaginationChange(page, pageSize),
    onChange: (page, pageSize) => onPaginationChange(page, pageSize),
    showSizeChanger: true,
    pageSizeOptions: [10, 20, 60, 100],
  };
  const ExtraButton = function () {
    return (
      <div>
        <Row gutter={[10, 10]}>
          <Col span="auto"></Col>
          <Col span="auto">
            <Button
              onClick={downloadApi}
              type="primary"
              loading={loadBtn}
              icon={<DownloadOutlined />}
              size={"middle"}
            />
          </Col>
          {!isExcelPage && (
            <Col span="auto">
              {usePermissions("orders/create") && (
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() => {
                    history.push("/orders/table/add");
                  }}
                >
                  {t("add")}
                </Button>
              )}
            </Col>
          )}
        </Row>
      </div>
    );
  };

  const extraTitle = (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        gap: 10,
        paddingLeft: "4px",
      }}
    >
      <Search
        allowClear
        style={{ width: 350 }}
        placeholder={`${t("search")}...`}
        onSearch={onSearchChange}
      />
      <div
        style={{
          minWidth: 250,
          border: "1px solid #bbb",
          fontSize: 14,
          padding: "4px 8px",
          borderRadius: 5,
        }}
      >
        {TextFilter(
          "geozones",
          popoverVisibleGeozone,
          setPopoverVisibleGeozone,
          setChosenGeozone,
          geozoneList,
          getValueGeozone,
          setGetValueGeozone,
          searchedWordGeozone,
          setSearchedWordGeozone,
          "topfilter"
        )}
      </div>
      {!permissionOrder?.is_my_branch && (
        <div
          style={{
            minWidth: 250,
            border: "1px solid #bbb",
            fontSize: 14,
            padding: "4px 8px",
            borderRadius: 5,
          }}
        >
          {TextFilter(
            "branches",
            popoverVisibleBranch,
            setPopoverVisibleBranch,
            setChosenBranch,
            branchList,
            getValueBranch,
            setGetValueBranch,
            searchedWordBranch,
            setSearchedWordBranch,
            "topfilter"
          )}
        </div>
      )}
    </div>
  );

  const phoneColumn = {
    title: t("phone.number"),
    dataIndex: "customer_phone",
    key: "customer_phone",
  };

  const addressColumn = {
    title: t("address"),
    dataIndex: "customer_address",
    key: "customer_address",
  };

  const deliveryTimeColumn = {
    title: (
      <CustomDatePicker
        asFilter={true}
        title="time.chosen.by.the.client.for.delivery"
        finalDate={deliveryDayRange}
        setFinalDate={setDeliveryDayRange}
      />
    ),
    dataIndex: "delivery_time",
    key: "delivery_day",
    render: (date, val) => {
      return `${moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")} `;
    },
  };

  const statusColumn = {
    title: t("status"),
    children: [
      {
        title: TextFilter(
          "status",
          popoverVisible,
          setPopoverVisible,
          setChosenStatus,
          statusList,
          getValueStatus,
          setGetValueStatus,
          searchedWordStatus,
          setSearchedWordStatus
        ),
        key: "status_id",
        dataIndex: "status",
        className: "column-overflow",
        width: 200,
        render: (text, data) => {
          return (
            <StyledTag>
              {!data?.status_id ? t("new") : t(data.status_id)}
            </StyledTag>
          );
        },
      },
      {
        title: (
          <CustomDatePicker
            asFilter={true}
            title="time"
            finalDate={timeRange}
            setFinalDate={setTimeRange}
          />
        ),
        dataIndex: isExcelPage ? "updated_at" : "status_created_time",
        key: "1",
        render: (date) =>
          isExcelPage ? (
            date.Valid ? (
              <div>
                {moment(date.Time).utcOffset(0).format("HH:mm DD-MM-YYYY")}
              </div>
            ) : (
              ""
            )
          ) : (
            moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")
          ),
      },
    ],
  };

  const actionColumn = {
    title: t("action"),
    key: "action",
    align: "center",
    fixed: "right",
    width: "200px",
    render: (text, record, index) => {
      return (
        <div>
          <StyledButton
            color="view"
            icon={EyeOutlined}
            onClick={() => handleView(text, index)}
            tooltip={t("view.order")}
          />
          {(usePermissions("orders/update", true) &&
            record.status_id === "new") ||
          record.status_id === "not_enough_docs" ||
          record.status_id === "pending" ||
          record.status_id === "assigned" ||
          record.status_id === "not_assigned" ||
          record.status_id === "overdue" ||
          record.status_id === "in_progres" ||
          record.status_id === "changed_address_and_time" ? (
            <StyledButton
              icon={AppstoreAddOutlined}
              onClick={() => handleAssign(text)}
              tooltip={t("assign.courier")}
            />
          ) : (
            <></>
          )}
          {usePermissions("orders/update") && (
            <StyledButton
              color="link"
              icon={EditFilled}
              onClick={() => handleEdit(text)}
              tooltip={t("edit.order")}
            />
          )}
          <Popconfirm
            title={t("do.you.really.want.to.delete")}
            visible={popVisible === index}
            onConfirm={() => handleDelete(text?.id)}
            okButtonProps={{ loading: deleteLoading }}
            onCancel={() => setPopVisible(null)}
            cancelText={t("cancel")}
            okText={t("yes")}
          >
            {usePermissions("orders/delete") && (
              <StyledButton
                color="danger"
                icon={DeleteFilled}
                onClick={() => setPopVisible(index)}
                tooltip={t("delete.order")}
              />
            )}
          </Popconfirm>
        </div>
      );
    },
  };

  const createdAtColumn = {
    title: (
      <CustomDatePicker
        asFilter={true}
        title="date.of.creation.of.the.application"
        finalDate={customerOrderRange}
        setFinalDate={setCustomerOrderRange}
      />
    ),
    dataIndex: "created_at",
    key: "created_at",
    render: (date) => (
      <div>{moment(date).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
    ),
  };

  const assignedAtColumn = {
    title: (
      <CustomDatePicker
        asFilter={true}
        title="application.distribution.time"
        finalDate={courierAssignedRange}
        setFinalDate={setCourierAssignedRange}
      />
    ),
    dataIndex: "assigned_at",
    key: "created_at",
    render: (date) => {
      return date.Valid ? (
        <div>{moment(date.Time).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
      ) : (
        ""
      );
    },
  };

  const finishedAtColumn = {
    title: t("finished.application.date"),
    dataIndex: "completed_at",
    key: "completed_at",
    render: (date) => {
      return date.Valid ? (
        <div>{moment(date.Time).utcOffset(0).format("HH:mm DD-MM-YYYY")}</div>
      ) : (
        ""
      );
    },
  };

  const clientColumn = {
    title: t("client"),
    dataIndex: "customer_name",
    key: "customer_name",
    render: (name) => makeStrFirstLetterUpper(name),
  };

  const productColumn = {
    title: TextFilter(
      "products",
      popoverVisibleProducts,
      setPopoverVisibleProducts,
      setChosenProduct,
      products,
      getValueStatusProduct,
      setGetValueStatusProduct,
      searchedWordProduct,
      setSearchedWordProduct
    ),
    dataIndex: isExcelPage ? "product_name" : "product",
    key: "product",
    width: 225,
    render: (product) =>
      isExcelPage ? (
        <Tag color="blue">{product}</Tag>
      ) : product.name ? (
        <Tag color="blue">{product.name}</Tag>
      ) : (
        "Нет продукта"
      ),
  };

  const courierColumn = {
    title: TextFilter(
      "mob.representatives",
      popoverVisibleCourier,
      setPopoverVisibleCourier,
      setChosenRepresentive,
      couriersList,
      getValueStatusCourier,
      setGetValueStatusCourier,
      searchedWordProductCourier,
      setSearchedWordProductCourier
    ),
    dataIndex: isExcelPage ? "courier_name" : "courier_id",
    key: "courier_id",
    render: (val) => {
      return isExcelPage
        ? !val.String
          ? t("no.courier")
          : makeStrFirstLetterUpper(val.String)
        : couriersList.length
        ? couriersList.filter((courier) => courier.value === val)[0]?.text ??
          t("no.courier")
        : "";
    },
  };
  

  const branchColumn = {
    title: t("branch"),
    dataIndex: "branch_name",
    key: "branch_id",
    render: (branch_name) => {
      return branch_name.length ? branch_name : "";
    },
  };

  const commentColumn = {
    title: (
      <SearchFilter
        title="comment"
        popoverVisible={popoverVisibleComment}
        setPopoverVisible={setPopoverVisibleComment}
        value={commentValue}
        setValue={setCommentValue}
      />
    ),
    dataIndex: "note",
    key: "delivery_day",
    render: (note) => {
      return note.length ? note : "";
    },
  };

  const cardsAmound = {
    title: ("Количество карт"),
    dataIndex: "note",
    key: "cards_amound",
    render: (cards_amound) => {
      return cards_amound.length ? cards_amound : "";
    },
  };

  const orderIdColumn = {
    title: (
      <SearchFilter
        title="UID ID заказа"
        popoverVisible={popoverVisibleUid}
        setPopoverVisible={setPopoverVisibleUid}
        value={uidValue}
        setValue={setUidValue}
      />
    ),
    dataIndex: "id",
    key: "id",
    render: (id) => {
      return id;
    },
  };

  const ordersColumns = [
    statusColumn,
    clientColumn,
    phoneColumn,
    addressColumn,
    productColumn,
    cardsAmound,
    orderIdColumn,
    courierColumn,
    createdAtColumn,
    deliveryTimeColumn,
    assignedAtColumn,
    commentColumn,
    actionColumn,
  ];

  const excelReportsColumns = [
    createdAtColumn,
    assignedAtColumn,
    finishedAtColumn,
    clientColumn,
    productColumn,
    statusColumn,
    courierColumn,
    branchColumn,
    commentColumn,
  ];
  return (
    <div className="all-orders">
      <Card title={extraTitle} extra={<ExtraButton />}>
    
        <Table
          bordered
          columns={isExcelPage ? excelReportsColumns : ordersColumns}
          loading={listloading}
          dataSource={items.orders}
          pagination={pagination}
          rowKey={(record) => record.id}
          onChange={onTableChange}
          rowClassName={(record) => {
            if (record?.status_id === "overdue") return "selected-row";
          }}
          scroll={{ x: 2100, y: 1300 }}
          locale={{
            filterConfirm: t("ok"),
            filterReset: t("reset"),
            emptyText: t("no.data"),
          }}
        />
      </Card>

      <View
        title={t("view.order")}
        cancelText={t("cancel")}
        width={1000}
        visible={visibleView}
        onCancel={closeView}
        orderData={orderData}
        setOrderData={setOrderData}
        getItems={getItems}
        status={"all_orders"}
      />

      <Modal
        title={t("change.status")}
        visible={forwardModal}
        onCancel={() => setForwardModal(false)}
        footer={null}
      >
        <Form form={form} layout="vertical" onFinish={onForwardFinish}>
          <Form.Item name="comment" label={t("comment")}>
            <Input />
          </Form.Item>
          <Form.Item required name="status_id" label={t("status")}>
            <Select placeholder={t("select.status")}>
              {statuses
                .filter((val) => val !== "all_orders")
                .map((val, i) => (
                  <Select.Option value={val} key={i}>
                    {t(val)}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
          <Row justify="end">
            <Button key="cancel" onClick={() => setForwardModal(false)}>
              {t("cancel")}
            </Button>
            <Button
              key="submit"
              type="primary"
              htmlType="submit"
              loading={loading}
              style={{ marginLeft: 10 }}
            >
              {t("submit")}
            </Button>
          </Row>
        </Form>
      </Modal>

      <Modal
        title={t("assign.courier")}
        visible={assignModal}
        onCancel={() => setAssignModal(false)}
        footer={null}
      >
        <Form form={form} layout="vertical" onFinish={onAssignFinish}>
          <Form.Item name="comment" label={t("comment")}>
            <Input value={commentState} />
          </Form.Item>
          <Form.Item required name="branch_id" label={t("branches")}>
            <Select
              allowClear
              showSearch
              placeholder={t("select.branch")}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              filterSort={(optionA, optionB) =>
                optionA.children
                  .toLowerCase()
                  .localeCompare(optionB.children.toLowerCase())
              }
              onChange={branchSelectAssigning}
            >
              {branchList.length ? (
                branchList.map((val, i) => (
                  <Select.Option value={val.value} key={i}>
                    {t(val?.text)}
                  </Select.Option>
                ))
              ) : (
                <></>
              )}
            </Select>
          </Form.Item>

          <Form.Item
            required
            name="courier_id"
            label={t("mobile.representatives")}
          >
            <Select
              onSelect={(_, e) => {
                setCommentState(e?.children);
              }}
              allowClear
              placeholder={t("select.mob.representative")}
              showSearch="true"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              filterSort={(optionA, optionB) =>
                optionA.children
                  .toLowerCase()
                  .localeCompare(optionB.children.toLowerCase())
              }
            >
              {courierListByBranchId.length ? (
                courierListByBranchId.map((val, i) => (
                  <Select.Option value={val.value} key={i}>
                    {t(val?.text)}
                  </Select.Option>
                ))
              ) : (
                <></>
              )}
            </Select>
          </Form.Item>
          <Row justify="end">
            <Button key="cancel" onClick={() => setAssignModal(false)}>
              {t("cancel")}
            </Button>
            <Button
              key="submit"
              type="primary"
              htmlType="submit"
              loading={loading}
              style={{ marginLeft: 10 }}
            >
              {t("submit")}
            </Button>
          </Row>
        </Form>
      </Modal>
    </div>
  );
}

const statuses = [
  "all_orders",
  "new",
  "pending",
  "assigned",
  "in_progres",
  "cancelled",
  "on_the_way",
  "arrived",
  "delivered",
  "finished",
  "completed",
  "overdue",
];

const arrng = {
  descend: "desc",
  ascend: "asc",
};
