import "./style.css";
import React from "react";
import { List } from "./List";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

export default function   Table({tabKey}) {
  const { t } = useTranslation();
  const history = useHistory();
  return (
    <>
      {history.location.pathname.includes("excel") ? (
        <div className="table">
          <List tabKey={tabKey} />
        </div>
      ) : (
        <div className="table">
        
          <h1
            style={{
              margin: "35px 0 10px 24px",
              fontSize: "17px",
              lineHeight: "22px",
            }}
          >
            {t("all_orders")}
          </h1>
          <List tabKey={tabKey} />
        </div>
      )}
    </>
  );
}
