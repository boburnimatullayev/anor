import React, { useState } from "react";
import {  Card, Button, Form, Input, notification } from "antd";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import axios_init from "@/utils/axios_init";

export default function FinishStatus() {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [defaultValue, setDefaultValue] = useState({
    ids: "",
  });
  const history = useHistory();
  const routes = [
    {
      name: "orders",
      route: "/orders",
      link: false,
    },
    {
      name: "status",
      link: false,
      route: "/orders/finish-status",
    },
  ];

  //ON FINISH
  const onFinish = (e) => {
    setLoading(true);
    axios_init
      .get("order-processing/change-status/finished", e)
      .then((res) => {
        if (res.code == 200) {
          notification.success({ message: "Успешно" });
        }
        if (res.code != 200) {
          notification.warning({ message: "Ошибка" });
        }
      })

      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        form.resetFields();
        setLoading(false);
      });
  };
  return (
    <div className="content">
      <Card>
        <Form form={form} onFinish={onFinish} defaultValue={defaultValue}>
          <Form.Item name="ids" label="">
            <Input
              style={{ width: "350px" }}
              placeholder={t("enter.id")}
            ></Input>
          </Form.Item>
          <Button type="primary" htmlType="submit" loading={loading}>
            {t("save")}
          </Button>
        </Form>
      </Card>
    </div>
  );
}
