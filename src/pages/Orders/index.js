import "./style.css";
import React, { useState, useEffect, useCallback } from "react";
import { Tabs } from "antd";
import OrderMap from "./OrderMap/OrderMap";
import Calendar from "./Calendar/Calendar";
import Table from "./Table/Table";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import BreadCrumbTemplete from "@/components/breadcrumb/BreadCrumbTemplete";
import CourierTracking from "./CourierTracking/CourierTracking";
import Monitoring from "./Monitoring/Monitoring";
import FinishStatus from "./FinishStatus/FinishStatus";

const { TabPane } = Tabs;
const Orders = () => {
  const [tabKey, setTabKey] = useState("table");

  const { t } = useTranslation();
  const history = useHistory();
  const routes = [
    {
      name: "orders",
      route: "/orders",
      link: false,
    },
  ];

  const excelPageRoutes = [
    {
      name: "reports",
      route: "/reports",
      link: false,
    },
    {
      name: "by.excel",
      link: false,
    },
  ];
  return (
    <div className="orders" >
      <BreadCrumbTemplete
        routes={
          history.location.pathname.includes("excel") ? excelPageRoutes : routes
        }
      />

      <div className="content"  >
        <Tabs onChange={(e) => {
            console.log('tabkey', e)
          return setTabKey(e)
        }} size="large" key="table">
          <TabPane tab={t("table")} key="table">
            <Table tabKey={tabKey}  />
          </TabPane>
          <TabPane tab={t("calendar")} key="calendar">
            <Calendar tabKey={tabKey} />
          </TabPane>
          <TabPane tab={t("map")} key="map">
            <OrderMap tabKey={tabKey} />
          </TabPane>
          <TabPane tab={t("couriers.tracking")} key="couriers.tracking">
            <CourierTracking tabKey={tabKey} />
          </TabPane>
          <TabPane tab={t("monitoring")} key="monitoring">
            <Monitoring tabKey={tabKey} />
          </TabPane>
          <TabPane tab={t("finish.status")} key="6">
            <FinishStatus tabKey={tabKey} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};
export default Orders;
