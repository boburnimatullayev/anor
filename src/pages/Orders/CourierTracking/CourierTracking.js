import "./style.css";
import React, { useState, useEffect } from "react";
import courier_img from "../../../assets/icons/courier_logo.png";
import axios_init from "@/utils/axios_init";
import { useTranslation } from "react-i18next";
import { defaultMapState } from "@/constants/basic";
import moment from "moment";
import { Button, Card, Select, DatePicker } from "antd";
import { YMaps, Map, Placemark } from "react-yandex-maps";
import makeStrFirstLetterUpper from "../../../utils/makeStrFirstLetterUpper";

let interval = setInterval(() => {}, 0);

export default function CourierTracking({ tabKey }) {
  const { t } = useTranslation();

  const [couriers, setCouriers] = useState([]);
  const [courierIds, setCourierIds] = useState([]);
  const [curCouriers, setCurCouriers] = useState([]);
  const [date, setDate] = useState(moment());
  const [isDrawing, setIsDrawing] = useState(false);
  const [isOnline, setIsOnline] = useState(false);
  const [isActive, setIsActive] = useState(1);
  const [filterCourierIds, setFilterCourierIds] = useState([]);
  const [filterGeozoneIds, setFilterGeozoneIds] = useState(null);
  const [geozones, setGeozones] = useState([]);

  useEffect(() => {
    if (tabKey === "couriers.tracking") {
      getCouriers();
      getItems();
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive, tabKey]);

  useEffect(() => {
    if (couriers.length && tabKey === "couriers.tracking") {
      getCurCouriers();
      clearInterval(interval);
      interval = setInterval(() => {
        getCurCouriers();
      }, 8000);
    }
  }, [couriers, filterCourierIds, filterGeozoneIds, isOnline, tabKey, date]);

  function disabledDate(current) {
    const tomorrow = moment().add(1, "days").startOf("day");
    const yesterday = moment().subtract(1, "days").startOf("day");
    return yesterday > current || current > tomorrow;
  }

  const getCouriers = () => {
    setIsDrawing(true);
    axios_init
      .get(`/courier`, {
        active: isActive,
      })
      .then(({ data }) => {
        const allCouriersId = data?.couriers.map((item) => item.id);
        const allCouriers = data?.couriers.map((item) => ({
          id: item.id,
          name: item.name,
          phone: item.phone,
          location: item.location,
          address: item.address,
        }));

        setCourierIds(allCouriersId);
        setCouriers(allCouriers);
      })
      .catch((err) => console.log(err))
      .finally(() => setIsDrawing(false));
  };
  const getItems = () => {
    axios_init
      .get(`/branch`)
      .then((res) => {
        setGeozones(res?.data?.branches);

      })
      .catch((err) => console.log(err));
  };
  const getCurCouriers = () => {
    axios_init
      .get(`/courier-locations`, {
        courier_id:
          filterCourierIds.length > 0
            ? filterCourierIds.toString()
            : filterGeozoneIds
            ? ""
            : "all",
        is_online: isOnline,
        from_time: date.format("YYYY-MM-DD HH:mm:ss"),
        branch_id: filterGeozoneIds,
      })
      .then((res) => {
        setCurCouriers(res?.data || []);
      })
      .catch((err) => console.log(err));
  };

  const filterOneCourier = (filterGeozoneIds, field) => {
    const filtered = couriers?.find((item) => item.id === filterGeozoneIds);
    return makeStrFirstLetterUpper(filtered?.[field]);
  };
  const DrawCouriers = () =>
    curCouriers.map(({ location, name, courier_id, phone, address }, index) => {
      if (courierIds.includes(courier_id)) {
        return (
          <Placemark
            key={index}
            options={{
              iconLayout: "default#imageWithContent",
              iconImageHref: courier_img,
              iconImageSize: [40, 40],
              iconImageOffset: [-20, -20],
            }}
            field
            geometry={[location.lat, location.long]}
            properties={{
              hintContent: name,
              balloonContent: `
                    <span>Имя: ${filterOneCourier(
                      courier_id,
                      "name"
                    )}</span><br>
                    <span>Телефон: ${filterOneCourier(
                      courier_id,
                      "phone"
                    )}</span><br>
                    <span>Адрес: ${filterOneCourier(
                      courier_id,
                      "address"
                    )}</span>
                `,
            }}
          />
        );
      }
    });

  const { Option } = Select;
  const ExtraOptions = (
    <>
      <Button
        type="primary"
        loading={isDrawing}
        onClick={() => getCurCouriers()}
      >
        {t("search")}
      </Button>
      <Select
        style={{ width: 160, marginLeft: 10 }}
        allowClear
        showSearch
        placeholder={t("filial_filter")}
        optionLabelProp="label"
        maxTagCount={"responsive"}
        onChange={(val) => {
          setFilterGeozoneIds(val);
        }}
      >
        {geozones?.map((geozona, i) => (
          <Option key={geozona.id} label={geozona.name} value={geozona.id}>
            {geozona?.name}
          </Option>
        ))}
      </Select>
      <Select
        showSearch
        mode="multiple"
        placeholder={t("filter")}
        optionFilterProp="children"
        style={{ width: 250, marginLeft: 10 }}
        maxTagPlaceholder={(e) =>
          t("selected") + " " + e.length + " " + t("couriers")
        }
        filterOption={(input, option) =>
          option.children.toLowerCase().includes(input.toLowerCase())
        }
        defaultValue={[]}
        optionLabelProp="label"
        maxTagCount={"responsive"}
        onChange={(val) => setFilterCourierIds(val)}
      >
        {couriers.length &&
          couriers.map((courier, i) => (
            <Option key={courier.id} label={courier?.name} value={courier.id}>
              {courier?.name}
            </Option>
          ))}
      </Select>
      <Select
        placeholder={t("select.status")}
        style={{ width: 160, marginLeft: 10 }}
        onChange={(val) => setIsActive(val)}
        allowClear
      >
        <Option value={1}>{t("active")}</Option>
        <Option value={0}>{t("inactive")}</Option>
      </Select>
      <DatePicker
        allowClear={false}
        showTime
        format="YYYY-MM-DD HH:mm"
        placeholder={t("select.date")}
        disabledDate={disabledDate}
        onChange={(val) => setDate(val)}
      />
      <Select
        allowClear
        placeholder={t("on.the.web")}
        style={{ width: 160, marginLeft: 10 }}
        onChange={(val) => setIsOnline(val)}
      >
        <Option value="false">{t("offline")}</Option>
        <Option value="true">{t("online")}</Option>
      </Select>
    </>
  );

  return (
    <div className="couriers-tracking">
      <Card title={t("map")} extra={ExtraOptions}>
        <YMaps query={{ lang: "ru_RU", load: "package.full" }}>
          <Map
            width="100%"
            height="100%"
            defaultState={{
              ...defaultMapState,
              controls: defaultMapState.controls.filter(
                (val) => val !== "fullscreenControl"
              ),
            }}
          >
            {curCouriers.length ? <DrawCouriers /> : <></>}
          </Map>
        </YMaps>
      </Card>
    </div>
  );
}
