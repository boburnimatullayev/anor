import "./style.css"
import React, { useState, useEffect, useRef, useMemo } from "react"
import { Modal } from "antd"
import { defaultMapState } from "@/constants/basic"
import { YMaps, Map, Polyline, Placemark } from 'react-yandex-maps';


export function ViewCourierMap({placeMarkData = [], polylineData = [], userDailyRoute = [], lastLocationOfCourier = {}, ...props }) {
    const ymaps = useRef(null)



    const sortedUserDailyRoute = userDailyRoute?.map(item => [item.location.lat, item.location.long]) ?? []
    const sortedLastLocationOfCourier = lastLocationOfCourier ? [lastLocationOfCourier?.location?.lat, lastLocationOfCourier?.location?.long] : []

    const addEventToButton = () => {
        if(placeMarkData.length) {
            placeMarkData.map(({order}) => {
                if(order) {
                   
                    var element = document.getElementById(order.id);
                    element.addEventListener("click", () => console.log("clicked"))
                }
            })
        }
    }

    const DrawRoutes = () => polylineData.map(({geometry}, index) => {
        return(
        <div key={'polyline' + index}>
            <Polyline
                geometry={geometry}
                options={options}
            />
        </div>
    )})

    const DrawPlaceMarks = () => placeMarkData.map(({type, location, order}, index) => {
        

        return (
            <Placemark
                key={'steps' + index}
                options={markOptions[type]}
                geometry={location}
                properties={placeMarkProperty({type, order})}
            />
        )
    })

    return (
        <Modal {...props} className="view-courier-map">
            <YMaps ref={ymaps} query={{ lang: "ru_RU", load: "package.full" }}>
                <Map
                    // instanceRef={addViewButton}
                    width="100%"
                    height="100%"
                    defaultState={defaultMapState}
                >
                    {polylineData.length ? <DrawRoutes /> : <></>}
                    {placeMarkData.length ? <DrawPlaceMarks /> : <></>}
                    {/* <ZoomControl options={{ float: 'right' }} /> */}
                    <Polyline 
                        geometry={sortedUserDailyRoute}
                        options={{
                            balloonCloseButton: false,
                            strokeColor: '#FF0000',
                            strokeWidth: 4,
                            strokeOpacity: 0.5,
                          }}
                    />
                    <Placemark
                        geometry={sortedLastLocationOfCourier}
                        properties={ {iconContent: `${lastLocationOfCourier?.name}`} }
                        options={{
                            preset: 'islands#darkGreenStretchyIcon'
                        }}
                    />
                </Map>
            </YMaps>
        </Modal>
    )
}


// function randomColor(){
//     var allowed = "ABCDEF0123456789", S = "#";

//     while(S.length < 7){
//         S += allowed.charAt(Math.floor((Math.random()*16)+1));
//     }
//     return S;
// }

function placeMarkProperty({type, order = {}}) {

    if(type === "job") {
        return {
            hintContent: order.customer_name,
            balloonContent: `
                <span>Имя: ${order.customer_name}</span><br>
                <span>Телефон: ${order.customer_phone}</span><br>
                <span>Адрес: ${order.customer_address}</span><br>
                `
                // <button id="${order.id}">view</button>
        }
    } else {
        return {}
    }
}

const options = {
    balloonCloseButton: false,
    strokeColor: "#466CA9",
    strokeWidth: 4,
    strokeOpacity: 0.6,
}

const markOptions = {
    start: {
        // preset: 'islands#circleIcon',
        iconColor: "red"
    },
    end: {
        // preset: 'islands#circleIcon',
        iconColor: "red"
    },
    job: {
        preset: 'islands#circleDotIcon',
        iconColor: "#466CA9",
    }
}