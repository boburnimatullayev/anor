import "./style.css";
import React, { useEffect, useState, useMemo, useCallback } from "react";
import basic from "@/constants/basic";
import moment from "moment";
import axios_init from "@/utils/axios_init";
import { decode } from "@/utils/decodeGeometry";
import StyledCard from "@/components/StyledCard/StyledCard";
import { ViewCourierMap } from "./View";
import { useTranslation } from "react-i18next";
import { Card, DatePicker, Spin, Select } from "antd";

const { COURIER_MAP_URL } = basic;
let courierLocationInterval = setInterval(() => {}, 0);

export default function Calendar({ tabKey }) {
  const { t } = useTranslation();

  const [couriers, setCouriers] = useState([]);
  const [placeMarkData, setPlaceMarkData] = useState([]);
  const [polylineData, setPolylineData] = useState([]);
  const [userDailyRoute, setUserDailyRoute] = useState([]);
  const [courierLastLocation, setCourierLastLocation] = useState([]);
  const [date, setDate] = useState(moment());
  const [visible, setVisible] = useState(false);
  const [cardLoading, setCardLoading] = useState(null);
  // for select
  const [couriersOption, setCouriersOption] = useState([]);
  const [isFetchingCouriers, setIsFetchingCouriers] = useState(false);
  const [selectedCourier, setSelectedCourier] = useState([]);

  useEffect(() => {
    if (tabKey === "calendar") {
      getCouriers();
    }
    return () => {
      clearInterval(courierLocationInterval);
    };
  }, [tabKey]);

  useEffect(() => {
    if (tabKey === "calendar") {
      setCalendar();
    }
  }, [date, tabKey]);

  const getCouriers = (searchValue = "") => {
    setIsFetchingCouriers(true);
    axios_init
      .get("/courier", { search: searchValue })
      .then((res) => {
        setCouriersOption(res.data.couriers);
      })
      .catch((err) => console.log(err))
      .finally(() => setIsFetchingCouriers(false));
  };

  const onCourierSearch = (value) => {
    getCouriers(value);
  };

  const makeTimetable = async () => {
    let min = 0;
    let max = 23;
    const delivery_time_from = moment(date)
      .set({ hour: 0, minute: 0 })
      .add(5, "h");
    const delivery_time_to = moment(date)
      .set({ hour: 23, minute: 59 })
      .add(5, "h");

    let { data: couriersData } = await axios_init.get(
      "/courier-timetable-datefilter",
      { date }
    );
    let { data: ordersData } = await axios_init.get("/order", {
      delivery_time_from,
      delivery_time_to,
      status: "assigned",
      limit: 1000,
    });

    return new Promise((resolve, reject) => {
      let result = couriersData.couriers.map((courier) => ({
        ...courier,
        orders: ordersData.orders.filter(
          ({ courier_id }) => courier_id === courier.courier_id
        ),
      }));
      resolve(result);
    });
  };

  const items = useMemo(() => {
    const res = couriers.length
      ? couriers.filter(
          (elm) =>
            !selectedCourier.length || selectedCourier.includes(elm.courier_id)
        )
      : [];

    return res;
  }, [couriers, selectedCourier]);

  const setCalendar = () => {
    makeTimetable().then((data) => {
      setCouriers(data);
    });
  };

  const onChangeDate = (value) => {
    setDate(value);
  };

  const getCourierRouteAndLocations = useCallback(async (courier) => {
    const courierDailyRoute = await axios_init.get("/courier-trackings", {
      courier_id: courier.courier_id,
      from_time: moment(date)
        .set({ hour: 0, minute: 0 })
        .format("YYYY-MM-DD HH:mm:ss"),
      to_time: moment(date)
        .set({ hour: 23, minute: 59 })
        .format("YYYY-MM-DD HH:mm:ss"),
    });

    const lastLocationOfCourier = await axios_init.get("/courier-locations", {
      courier_id: courier.courier_id,
    });

    const singleCourierInfo = await axios_init.get(
      `/courier/${courier.courier_id}`
    );
    let courierLocation = {
      ...(lastLocationOfCourier.data?.[0] ?? {}),
      name: singleCourierInfo.data.name,
    };

    setUserDailyRoute(courierDailyRoute.data);
    setCourierLastLocation(courierLocation);
  }, []);

  const onViewClick = async (branch_id, index, courier) => {
    setCardLoading(index);

    try {
      getCourierRouteAndLocations(courier);
      clearInterval(courierLocationInterval);
      courierLocationInterval = setInterval(() => {
        getCourierRouteAndLocations(courier);
      }, 8000);

      const { placeMarkData, polylineData } = await makeMapData(
        branch_id,
        index
      );

      setPlaceMarkData(placeMarkData);
      setPolylineData(polylineData);

      setCardLoading(false);
      setVisible(true);
    } catch (error) {
      console.log(error);
    }
  };

  const makeMapData = (branch_id, index) => {
    return new Promise(async (resolve, reject) => {
      let placeMarkData = [],
        polylineData = [];
      try {
        var { data: branchData } = await axios_init.get(`/branch/${branch_id}`);
      } catch (e) {
        reject(e);
      }

      let urlOSRM =
        COURIER_MAP_URL +
        `/driving/${branchData.location.long},${branchData.location.lat}`;
      placeMarkData.push({
        type: "start",
        location: [branchData.location.lat, branchData.location.long],
      });
      couriers[index].orders.map(({ customer_location: c_l, ...args }) => {
        if (c_l) {
          urlOSRM += `;${c_l.long},${c_l.lat}`;
          placeMarkData.push({
            type: "job",
            location: [c_l.lat, c_l.long],
            order: { ...args },
          });
        }
      });
      urlOSRM += `;${branchData.location.long},${branchData.location.lat}?source=first&destination=last`;
      placeMarkData.push({
        type: "end",
        location: [branchData.location.lat, branchData.location.long],
      });

      try {
        var { trips } = await axios_init.get(urlOSRM);
      } catch (e) {
        reject(e);
      }
      polylineData = trips.map(({ geometry }) => ({
        geometry: decode(geometry),
      }));

      resolve({ placeMarkData, polylineData });
    });
  };

  const ExtraOption = (
    <div style={{ display: "flex", gap: 10 }}>
      <Select
        allowClear
        showSearch
        style={{ width: 300 }}
        onSearch={onCourierSearch}
        filterOption={false}
        value={selectedCourier}
        mode="tags"
        showArrow={false}
        placeholder={t("mob.representatives")}
        onChange={(val) => setSelectedCourier(val)}
        notFoundContent={
          <div
            style={{
              width: "100%",
              height: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {isFetchingCouriers ? <Spin size="small" /> : t("no.data")}
          </div>
        }
      >
        {couriersOption && couriersOption?.length ? (
          couriersOption.map((val) => (
            <Select.Option key={val.id} value={val.id}>
              {val.name}
            </Select.Option>
          ))
        ) : (
          <></>
        )}
      </Select>

      <DatePicker
        allowClear={false}
        style={{ width: 150 }}
        placeholder={t("select.date")}
        value={date}
        onChange={onChangeDate}
      />
    </div>
  );

  return (
    <>
      <div className="table-content">
        <Card title={t("calendar")} extra={ExtraOption}>
          {couriers.length ? (
            <div
              style={{
                width: 70,
                flex: "none",
                borderRight: "1px solid #0000001a",
              }}
            >
              <div
                style={{
                  height: 28,
                  width: "100%",
                  borderBottom: "1px solid #0000001a",
                }}
              />
              {allDates.map((val, i) => (
                <div
                  key={i}
                  style={{
                    padding: 5,
                    height: 28,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                    borderBottom: "1px solid #0000001a",
                  }}
                >
                  <span style={{ lineHeight: "14px" }}>{val}</span>
                </div>
              ))}
            </div>
          ) : undefined}
          <div
            style={{
              display: "flex",
              width: "calc(100% - 70px)",
              overflowX: "scroll",
            }}
          >
            {items.map((courier, i) => (
              <div
                key={i}
                style={{
                  width: 250,
                  flex: "none",
                  borderLeft: i !== 0 && "1px solid #0000001a",
                }}
              >
                <div
                  style={{
                    padding: 5,
                    height: 28,
                    gap: 5,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                    borderBottom: "1px solid #0000001a",
                  }}
                >
                  <span style={{ lineHeight: "14px" }}>{courier.name}</span>
                  <div className="count-badge">{courier?.orders?.length}</div>
                </div>
                <div
                  style={{
                    height: "calc(100% - 28px)",
                    display: "flex",
                    justifyContent: "center",
                    width: "100%",
                    borderBottom: "1px solid #0000001a",
                  }}
                >
                  {courier.from_time && (
                    <StyledCard
                      type="active"
                      orders={courier.orders}
                      onViewClick={() =>
                        onViewClick(courier.branch_id, i, courier)
                      }
                      loading={cardLoading === i}
                      style={{
                        marginTop: (+courier.from_time.slice(11, 13) - 8) * 28,
                        height:
                          (+courier.to_time.slice(11, 13) -
                            +courier.from_time.slice(11, 13)) *
                          28,
                        width: "90%",
                      }}
                    />
                  )}
                </div>
              </div>
            ))}
          </div>
        </Card>
      </div>

      <ViewCourierMap
        title={t("orders.map")}
        visible={visible}
        onCancel={() => setVisible(false)}
        userDailyRoute={userDailyRoute}
        footer={null}
        placeMarkData={placeMarkData}
        lastLocationOfCourier={courierLastLocation}
        polylineData={polylineData}
      ></ViewCourierMap>
    </>
  );
}

const allDates = [
  "08:00",
  "09:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
];
