import "./style.css";
import React, { useState, useEffect } from "react";
import moment from "moment";
import courier_img from "../../../assets/icons/courier_logo.png";
import axios_init from "@/utils/axios_init";
import { decode } from "@/utils/decodeGeometry";
import { useTranslation } from "react-i18next";
import { defaultMapState } from "@/constants/basic";
import { Card, Button, DatePicker, Select, Popconfirm } from "antd";
import { YMaps, Map, Polyline, Placemark } from "react-yandex-maps";

const { Option } = Select;
export default function OrderMap({ tabKey }) {
  const { t } = useTranslation();

  const [isDrawing, setIsDrawing] = useState(false);
  const [couriers, setCouriers] = useState([]);
  const [courierShifts, setCourierShifts] = useState([]);
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rangeIndex, setRangeIndex] = useState(null);
  const [date, setdate] = useState(moment(new Date(), "YYYY-MM-DD"));
  const [routingData, setRoutingData] = useState([]);

  useEffect(() => {
    if (tabKey === "map") {
      getCourierShifts();
    }
  }, [tabKey]);

  useEffect(() => {
    getData();
    setVisible(false);
  }, [rangeIndex, date]);

  const getData = (assign = 0) => {
    if (rangeIndex !== null) {
      axios_init
        .get(`order-processing/test/${assign}`, {
          from: new Date(
            `${moment(date).format("YYYY-MM-DD")} ${courierShifts[
              rangeIndex
            ].from_time.slice(11, 16)} UTC`
          ),
          to: new Date(
            `${moment(date).format("YYYY-MM-DD")} ${courierShifts[
              rangeIndex
            ].to_time.slice(11, 16)} UTC`
          ),
        })
        .then(({ data }) => {
          setCouriers([]);

          let result = [];
          for (let key in data) {
            if (data[key] && assign === 0) {
              result.push(data[key].result.routes);
            }
          }
          setRoutingData(result);
        })
        .catch((err) => console.log(err))
        .finally(() => {
          setLoading(false);
          setVisible(false);
        });
    }
  };

  const getCourierShifts = () => {
    axios_init
      .get("/courier-shift")
      .then(({ data }) => {
        setCourierShifts(data.shifts);
      })
      .catch((err) => console.log(err));
  };

  const getCouriers = () => {
    setIsDrawing(true);
    axios_init
      .get("/courier")
      .then(({ data }) => {
        setCouriers(data.couriers);
      })
      .catch((err) => console.log(err))
      .finally(() => setIsDrawing(false));
  };

  const handleSave = () => {
    setLoading(true);
    getData(2);
  };

  const DrawBranchRoute = ({ data }) =>
    data.map((route, index) => {
      let color = randomColor();

      const options = {
        balloonCloseButton: false,
        strokeColor: color,
        strokeWidth: 4,
        strokeOpacity: 0.6,
      };

      const markOptions = {
        start: {
          iconColor: "red",
        },
        end: {
          iconColor: "red",
        },
        job: {
          preset: "islands#circleDotIcon",
          iconColor: color,
        },
      };

      const PlaceMarks = ({ routes }) =>
        routes.steps.map((step, i) => (
          <Placemark
            key={"steps" + i + Math.random()}
            options={markOptions[step.type]}
            geometry={step.location.reverse()}
            properties={placeMarkProperty(step)}
          />
        ));

      return (
        <div key={"polyline" + index + Math.random()}>
          <Polyline geometry={decode(route.geometry)} options={options} />
          <PlaceMarks routes={route} />
        </div>
      );
    });

  const DrawRoutes = () =>
    routingData ? (
      routingData.map((elm, i) => <DrawBranchRoute data={elm} key={i} />)
    ) : (
      <></>
    );

  const DrawCouriers = () =>
    couriers.map(({ location, name, phone, address, id }) => (
      <Placemark
        key={id}
        options={{
          iconLayout: "default#imageWithContent",
          iconImageHref: courier_img,
          iconImageSize: [40, 40],
          iconImageOffset: [-20, -20],
        }}
        geometry={[location.lat, location.long]}
        properties={{
          hintContent: name,
          balloonContent: `
                    <span>Имя: ${name}</span><br>
                    <span>Телефон: ${phone}</span><br>
                    <span>Адрес: ${address}</span>
                `,
        }}
      />
    ));

  const ExtraOptions = () => {
    return (
      <>
        <Button type="primary" loading={isDrawing} onClick={getCouriers}>
          {t("view.couriers")}
        </Button>
        <DatePicker
          allowClear={false}
          style={{ marginLeft: 10 }}
          onChange={(date) => setdate(date)}
          defaultValue={date}
        />
        <Select
          loading={!courierShifts.length}
          placeholder={t("select.shift")}
          style={{ width: 140, marginLeft: 10 }}
          onChange={(val) => setRangeIndex(val)}
          value={rangeIndex}
        >
          {courierShifts.length ? (
            courierShifts.map((shift, i) => (
              <Option key={shift.id} value={i}>
                {shift.from_time.slice(11, 16)} - {shift.to_time.slice(11, 16)}
              </Option>
            ))
          ) : (
            <></>
          )}
        </Select>
        <Popconfirm
          title={t("do.you.really.want.to.save")}
          visible={visible}
          onConfirm={handleSave}
          okButtonProps={{ loading: loading }}
          onCancel={() => setVisible(false)}
          cancelText={t("cancel")}
          okText={t("yes")}
        >
          <Button
            type="primary"
            style={{ marginLeft: 10 }}
            onClick={() => setVisible(true)}
          >
            {t("save")}
          </Button>
        </Popconfirm>
      </>
    );
  };

  return (
    <div className="order-map">
      <Card title={t("map")} extra={<ExtraOptions />}>
        <YMaps query={{ lang: "ru_RU", load: "package.full" }}>
          <Map
            width="100%"
            height="100%"
            defaultState={{
              ...defaultMapState,
              controls: defaultMapState.controls.filter(
                (val) => val !== "fullscreenControl"
              ),
            }}
          >
            {courierShifts.length && routingData.length ? (
              <DrawRoutes />
            ) : couriers.length ? (
              <DrawCouriers />
            ) : (
              <></>
            )}
          </Map>
        </YMaps>
      </Card>
    </div>
  );
}

function randomColor() {
  var allowed = "ABCDEF0123456789",
    S = "#";

  while (S.length < 7) {
    S += allowed.charAt(Math.floor(Math.random() * 16 + 1));
  }
  return S;
}

function placeMarkProperty({ type, order }) {
  if (type === "job") {
    return {
      hintContent: order.customer_name,
      balloonContent: `
                <span>Имя: ${order.customer_name}</span><br>
                <span>Телефон: ${order.customer_phone}</span><br>
                <span>Адрес: ${order.customer_address}</span>
            `,
    };
  } else {
    return {};
  }
}

// const img_coutier = "https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.creativemarket.com%2F0.1.0%2Fps%2F6600834%2F1820%2F1214%2Fm1%2Ffpnw%2Fwm0%2Flogo-source-20-.jpg%3F1561626659%26s%3Da9fe72001dd02abb20522fda4c8dba0f&imgrefurl=https%3A%2F%2Fcreativemarket.com%2FZHR%2F3895012-simple-delivery-courier-logo&tbnid=bgXIGI4JxyruPM&vet=12ahUKEwiA5bmPxtfwAhWplosKHTRXCyMQMygwegUIARDKAg..i&docid=POS6XQ6DOXSwTM&w=1820&h=1214&q=courier&hl=en&ved=2ahUKEwiA5bmPxtfwAhWplosKHTRXCyMQMygwegUIARDKAg"
